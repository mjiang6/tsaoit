// Logger.java
// Michael Jiang, 09/02/2009

package jlib.util;

import org.apache.log4j.xml.DOMConfigurator;


/**
 * @author Michael Jiang
 * @date 09-02-2009
 */

public class Logger extends Thread
{
    /**
     * initialize with default log configuration file
     */
    public static void init()
    {
        Logger.init("jlib.util.Logger");
    }

    /**
     * initialize with given log file name
     */
    public static void init(String logName)
    {
        Logger.init(null, logName);
    }

    /**
     * initialize with given log file name
     */
    public static void init(String logCfgFile, String logName)
    {
        if (_me == null)
            _me = new Logger(logCfgFile, logName);
    }

    public static void log(String msg) {
        Logger.log(msg, null);
    }

    public static void all(String msg) {
        Logger.all(msg, null);
    }

    public static void debug(String msg) {
        Logger.debug(msg, null);
    }

    public static void info(String msg) {
        Logger.info(msg, null);
    }

    public static void warn(String msg) {
        Logger.warn(msg, null);
    }

    public static void error(String msg) {
        Logger.error(msg, null);
    }

    public static void fatal(String msg) {
        Logger.fatal(msg, null);
    }

    public static void console(String msg) {
        Logger.console(msg, null);
    }

    public static void log(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 3); else _me.logger.warn(message(msg, obj));
    }

    public static void all(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 6); else _me.logger.debug(message(msg, obj));
    }

    public static void debug(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 5); else _me.logger.debug(message(msg, obj));
    }

    public static void info(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 4); else _me.logger.info(message(msg, obj));
    }

    public static void warn(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 3); else _me.logger.warn(message(msg, obj));
    }

    public static void error(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 2); else _me.logger.error(message(msg, obj));
    }

    public static void fatal(String msg, Object obj) {
        if (_me == null) Logger.console(msg, obj, 1); else _me.logger.fatal(message(msg, obj));
    }

    public static void console(String msg, Object obj) {
        Logger.console(msg, obj, 3);
    }

    public static void console(String msg, Object obj, int logLevel) {
        if (_logLevel==0 || _logLevel<logLevel)
            return;
        String lvl = null;
        if (logLevel == 1)
            lvl = " FATL ";
        else if (logLevel == 2)
            lvl = " ERRO ";
        else if (logLevel == 3)
            lvl = " WARN ";
        else if (logLevel == 4)
            lvl = " INFO ";
        else if (logLevel == 5)
            lvl = " DBUG ";
        else
            lvl = " DUMP ";
        System.out.println(lvl+message(msg, obj));
        System.out.flush();
    }
    
    public static void setLogLevel(int logLevel) {
        _logLevel = logLevel;
    }

    public static Logger create(String logCfgFile, String logName) {
        return new Logger(logCfgFile, logName);
    }
    
    public org.apache.log4j.Logger getLogger() {
        return logger;
    }

    static String message(String msg, Object obj) {
        if (obj == null)
            return msg;
        String s = "["+obj.getClass().getSimpleName()+"] ";
        return (s+msg);
    }

    Logger(String logCfgFile, String logName)
    {
        try {
            if (!Helper.empty(logCfgFile))
                DOMConfigurator.configure(logCfgFile);
            logger = Helper.empty(logName)? org.apache.log4j.Logger.getRootLogger(): org.apache.log4j.Logger.getLogger(logName);
        } catch (Exception e) { }
    }
    
    org.apache.log4j.Logger logger;
    static Logger _me = null;
    static int _logLevel = 3;
}

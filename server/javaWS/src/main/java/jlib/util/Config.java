// Config.java
// Michael Jiang, 2009-09-01
package jlib.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Enumeration;
import java.util.Properties;

public class Config
{

    public static Config me()
    {
        return _me;
    }

    public static void load(String path) throws IOException
    {
        Config.load(new File(path));
    }

    public static void load(File f) throws IOException
    {
        Config.load(new FileInputStream(f));
        _me.file = f;
    }

    public static void load(InputStream in) throws IOException
    {
        Properties p = new Properties();
        p.load(in);
        _me = new Config(p, false);
    }

    public static void loadFromXML(String path) throws IOException
    {
        Config.loadFromXML(new File(path));
    }

    public static void loadFromXML(File f) throws IOException
    {
        Config.loadFromXML(new FileInputStream(f));
        _me.file = f;
    }

    public static void loadFromXML(InputStream in) throws IOException
    {
        Properties p = new Properties();
        p.loadFromXML(in);
        _me = new Config(p, true);
    }

    public static void addFromXML(InputStream in) throws IOException
    {
        Properties newProperties = new Properties();
        newProperties.loadFromXML(in);
        
        // Check if we have existing properties
        if (_me != null && _me.properties != null)
        {
            // Get existing properties
            Properties oldProperties = _me.properties;
            Enumeration<?> propertyEnum = oldProperties.propertyNames();
            
            for (; propertyEnum.hasMoreElements(); ) 
            {
                // Get existing property name
                String propName = (String)propertyEnum.nextElement();

                // Get New property value
                String propValue = (String)oldProperties.get(propName);
                
                // Set the existing property within the new properties variable
                newProperties.put(propName, propValue);
            }
        } // End if we have existing properties
        
        // Update the config object to add the new properties
        _me = new Config(newProperties, true);
        
    } // End addFromXML()
    
    public static void addNewProperties(Properties propertiesIn) throws IOException
    {

        // Create set of new properties
        Properties newProperties = new Properties(propertiesIn);
        
        // Check if we have existing properties
        if (_me != null && _me.properties != null)
        {
            // Get existing properties
            Properties oldProperties = _me.properties;
            Enumeration<?> propertyEnum = oldProperties.propertyNames();
            
            for (; propertyEnum.hasMoreElements(); ) 
            {
                // Get existing property name
                String propName = (String)propertyEnum.nextElement();

                // Get New property value
                String propValue = (String)oldProperties.get(propName);
                
                // Set the existing property within the new properties variable
                newProperties.put(propName, propValue);
            }
        } // End if we have existing properties
        
        // Update the config object to add the new properties
        _me = new Config(newProperties, true);
        
    } // End addFromXML()
    
    public static Properties getProperties()
    {
        return _me.properties;
    }

    public int getInt(String name)
    {
        return this.get(name, (int)0);
    }

    public int get(String name, int def)
    {
        String v = this.get(name, Helper.str(def));
        if (v.length() <= 0)
            return def;
        return Helper.num(v);
    }

    public boolean getBool(String name)
    {
        return this.get(name, false);
    }

    public boolean get(String name, boolean def)
    {
        String v = this.get(name, Helper.str(def));
        if (Helper.empty(v))
            return def;
        return Helper.bool(v);
    }

    public String getStr(String name)
    {
        return this.get(name, "");
    }

    public String get(String name)
    {
        return this.get(name, "");
    }

    public String get(String name, String def)
    {
        return Helper.parseSysEnv(getRaw(name, def));
    }

    public void set(String name, int val)
    {
        this.set(name, String.valueOf(val));
    }

    public void set(String name, boolean val)
    {
        this.set(name, (val? "YES": "NO"));
    }

    public void set(String name, String val)
    {
        if (Helper.empty(name) || file==null)
            return;
        synchronized (this)
        {
            properties.put(name, val);
            try
            {
                if (isXml)
                    properties.storeToXML(new FileOutputStream(file, false), "");
                else
                    properties.store(new FileOutputStream(file, false), "");
            } catch (IOException e) {}
        }
    }

    private String getRaw(String name, String def)
    {
        synchronized (this)
        {
            String v = properties.getProperty(name);
            if (v != null)
                return v;
            v = def;
            properties.put(name, v);
            if (autoSave && file!=null)
            {
                try
                {
                    if (isXml)
                        properties.storeToXML(new FileOutputStream(file, false), "Saved default");
                    else
                        properties.store(new FileOutputStream(file, false), "Saved default");
                } catch (IOException e) {}
            }
            return v;
         }
    }

    Config(Properties p, boolean x)
    {
        this.properties = p;
        this.isXml = x;
        this.file = null;
    }

    Properties properties;
    File file;
    boolean isXml;
    boolean autoSave;
    
    static Config _me = null;
}

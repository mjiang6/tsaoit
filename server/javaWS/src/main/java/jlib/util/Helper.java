package jlib.util;
/**
 *  Created by Michael Jiang on 12/4/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

import java.util.Collection;
import java.util.Enumeration;
import java.util.Iterator;
import java.util.Map;

public class Helper {
    /**
     * test for empty (null is empty)
     */
    public static boolean empty(String s) {
        return (s==null || s.length() <= 0);
    }

    public static boolean empty(Object[] b) {
        return (b==null || b.length<=0);
    }

    public static boolean empty(byte[] b) {
        return (b==null || b.length<=0);
    }

    public static <T> boolean empty(Collection<T> obj) {
        return (obj==null || obj.size()<=0);
    }

    public static <K, V> boolean empty(Map<K, V> obj) {
        return (obj==null || obj.size()<=0);
    }

    public static <T> boolean empty(Iterator<T> i) {
        return (i==null || !i.hasNext());
    }

    public static <T> boolean empty(Enumeration<T> e) {
        return (e==null || !e.hasMoreElements());
    }

    /**
     * convert String to boolean
     */
    public static boolean bool(String param) {
        String ss = Helper.trim(param);
        if (Helper.empty(ss))
            return false;
        if (ss.equalsIgnoreCase("YES"))
            return true;
        if (ss.equalsIgnoreCase("TRUE"))
            return true;
        if (ss.equalsIgnoreCase("Y"))
            return true;
        if (ss.equalsIgnoreCase("T"))
            return true;
        if (Helper.num(ss)  != 0)
            return true;
        return false;
    }

    public static boolean bool(Boolean param) {
        return (param != null && param == Boolean.TRUE);
    }

    public static boolean bool(Number param) {
        return (param != null && param.intValue() != 0);
    }

    /**
     * convert String to number as int
     */
    public static int num(String param) {
        return Helper.num(param, 10);
    }
    public static int num(String param, int radix) {
        try
        {
            String ss = Helper.trim(param);
            if (Helper.empty(ss))
                return 0;
            return Integer.parseInt(ss, radix);
        } catch (Exception e) {}
        return 0;
    }
    public static int num(Object param) {
        try {
            return Helper.num((String)param);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * convert null String to ""
     */
    public static String str(String param) {
        return (param==null? "": param);
    }

    /**
     * convert int to String
     */
    public static String str(int param) {
        return String.valueOf(param);
    }

    /**
     * convert boolean to "YES" or "NO"
     */
    public static String str(boolean param) {
        return (param? "YES": "NO");
    }

    /**
     * convert double to String
     */
    public static String str(double param) {
        return String.valueOf(param);
    }

    /**
     * convert byte[] to String
     */
    public static String str(byte[] param) {
        return (param==null || param.length<=0? "": new String(param));
    }

    public static String trim(String param) {
        return Helper.empty(param)? "": param.trim();
    }


    /**
     * Replace macro ${XXX} in the input string "ss" with system environment variable
     */
    public static String parseSysEnv(String ss) {
        if (Helper.empty(ss))
            return "";
        String oo = "";
        int beg = 0;
        int end = 0;
        for (;;) {
            beg = ss.indexOf("${", end);
            if (beg < 0)
                break;

            oo += ss.substring(end, beg);
            beg += 2;
            end = ss.indexOf("}", beg);
            if (end < 0)
            {
                oo += "${";
                end = beg;
                continue;
            }
            String envName = ss.substring(beg, end);
            String envValue = Helper.str(System.getProperty(envName));
            oo += envValue;
            end ++;
            beg = end;
        }
        oo += ss.substring(end, ss.length());
        return oo;
    }

    /**
     * Replace macro $(name) in the input string "ss" with val
     */
    public static String parseVaraible(String ss, String name, String val) {
        if (Helper.empty(ss))
            return "";
        String oo = "";
        int beg = 0;
        int end = 0;
        for (;;) {
            beg = ss.indexOf("$(", end);
            if (beg < 0)
                break;

            oo += ss.substring(end, beg);
            beg += 2;
            end = ss.indexOf(")", beg);
            if (end < 0) {
                oo += "$(";
                end = beg;
                continue;
            }
            String envName = ss.substring(beg, end);
            if (name.equals(envName))
                oo += Helper.str(val);
            else
                oo += ("$("+envName+")");
            end ++;
            beg = end;
        }
        oo += ss.substring(end, ss.length());
        return oo;
    }

}

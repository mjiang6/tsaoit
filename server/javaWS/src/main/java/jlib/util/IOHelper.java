package jlib.util;
/**
 *  Created by Michael Jiang on 12/4/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.Console;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

public class IOHelper {
    public static final int UNKNOWN_LENGTH = -1;
    public static final int CHUNKED_LENGTH = -1000;

    /**
	 * Constructors for IOHelper.
	 */
	public IOHelper(String in) {
        this(Helper.str(in).getBytes());
	}

    public IOHelper(byte[] bytes) {
        this(new ByteArrayInputStream(bytes));
    }

	public IOHelper(File inFile) throws IOException {
        this(new FileInputStream(inFile));
	}
	
	public IOHelper(Console con) {
	    this(System.in);
	    m_con = con;
	}

    public IOHelper(InputStream in) {
        m_in = in;
        m_con = null;
    }

    /**
     * This is for transfer unknown amount of data from input stream.
     */
    public int writeTo(OutputStream out) throws IOException {
        return writeTo(out, true);
    }

    /**
     * This is for transfer unknown amount of data from input stream.
     */
    public int writeTo(OutputStream out, boolean autoFlush) throws IOException {
        if (m_in == null || out==null)
            throw new IOException("IOHelper.writeTo(): Null in or out stream!");
        byte[] buf = new byte[BUF_SIZE];
        int n = 0, count = 0;
        while ((n = m_in.read(buf, 0, BUF_SIZE)) != -1) {
            out.write(buf, 0, n);
            count += n;
        }
        if (autoFlush)
            try {out.flush();} catch (Exception af) {}
        return count;
    }

    /**
     * This is for transfer unknwon amount of data from input stream.
     */
    public int writeTo(File outFile) throws IOException {
        return this.writeTo(outFile, UNKNOWN_LENGTH);
    }

    /**
     * This is for transfer knwon amount of data from input stream.
     */
    public int writeTo(File outFile, int len) throws IOException {
        FileOutputStream fo = null;
        try {
            fo = new FileOutputStream(outFile, false);
            return this.writeTo(fo, len);
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        } finally {
            if (fo != null) try {fo.close();} catch (Exception e) {}
        }
    }

    /**
     * This is for transfer unknwon amount of data from input stream.
     */
    public int writeTo(StringBuffer str) throws IOException {
        return this.writeTo(str, -1);
    }

    /**
     * This is for transfer knwon amount of data from input stream.
     */
    public int writeTo(StringBuffer str, int len) throws IOException {
        try {
            if (str == null)
                return 0;
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            int n = this.writeTo(bo, len);
            str.append(bo.toString());
            return n;
        } catch (Exception e) {
            throw new IOException(e.getMessage());
        }
    }

    /**
     * This is for transfer knwon amount of data from input stream.
     */
    public int writeTo(OutputStream out,  int len) throws IOException {
        return this.writeTo(out, len, true);
    }

    /**
     * This is for transfer knwon amount of data from input stream.
     */
    public int writeTo(OutputStream out, int len, boolean autoFlush) throws IOException {
        if (len < 0)
            return writeTo(out, autoFlush);
        if (len == 0) // No data to load
            return 0;
        if (m_in == null || out==null)
            throw new IOException("IOHelper.writeTo(): Null in or out stream!");
        byte[] buf = new byte[BUF_SIZE];
        int n = 0, count = 0;
        while (n >= 0 && count <len) {
            int k = len - count;
            n = m_in.read(buf, 0, k<BUF_SIZE? k: BUF_SIZE);
            if (n <= 0)
                continue;
            out.write(buf, 0, n);
            count += n;
        }
        if (autoFlush)
            try {out.flush();} catch (Exception af) {}
        return count;
    }

    /**
     * If len is fairly small (less than 10 MB), this method may more effecient.
     */
    public byte[] readBytes(int len) throws IOException {
        ByteArrayOutputStream bo = new ByteArrayOutputStream();
        writeTo(bo, len);
        return bo.toByteArray();
    }

    public static  void println(OutputStream out) {
        IOHelper.println(out, "");
    }

    public static  void println(OutputStream out, String line) {
        IOHelper.println(out, line, "\r\n");
    }

    public static  void println(OutputStream out, String line, boolean flush) {
        IOHelper.println(out, line, "\r\n", false);
    }
    
    public static  void println(OutputStream out, String line, String eol) {
        IOHelper.println(out, line, eol, false);
    }
    
    public static  void println(OutputStream out, String line, String eol, boolean flush) {
        try {
            String msg = Helper.str(line) + (Helper.empty(eol)? "\r\n": eol);
            out.write(msg.getBytes(), 0, msg.length());
            if (flush)
                out.flush();
        } catch (Exception e) {}
    }

    public static  void println(File out) {
        IOHelper.println(out, true);
    }

    public static  void println(File out, boolean append) {
        IOHelper.println(out, "", append);
    }

    public static  void println(File out, String line) {
        IOHelper.println(out, line, true);
    }

    public static  void println(File out, String line, boolean append) {
        IOHelper.println(out, line, "\r\n", append);
    }
   
    public static  void println(File out, String line, String eol) {
        IOHelper.println(out, line, eol, true);
    }
    
    public static  void println(File out, String line, String eol, boolean append) {
        FileOutputStream fo = null;
        try {
            fo = new FileOutputStream(out, append);
            IOHelper.println(fo, line, eol, true);
        } catch (Exception e) {
        } finally {
            if (fo != null)
                try {fo.close();} catch (Exception e) {}
        }
    }

    public void close() {
        try {
            m_in.close();
            m_in = null;
        } catch (Exception e) {}
    }

    public InputStream getInputStream() {
        return m_in;
    }

    int num(String ln) {
        if (Helper.empty(ln))
            return 0;
        int i = ln.indexOf(";"); // ignore comment...
        if (i >= 0)
            ln = ln.substring(0, i);
        return Helper.num(Helper.trim(ln));
    }

    InputStream m_in;
    Console m_con;
    static final int BUF_SIZE = 0x1000;
}

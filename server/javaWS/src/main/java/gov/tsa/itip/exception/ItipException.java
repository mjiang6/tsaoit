package gov.tsa.itip.exception;
/**
 *  Created by Michael Jiang on 12/4/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

public class ItipException extends Exception {

    public ItipException() {
        this("ItipException");
    }

    public ItipException(String arg0) {
        super(arg0);
    }

    public ItipException(Throwable arg0) {
        super(arg0);
    }

    public ItipException(String arg0, Throwable arg1) {
        super(arg0, arg1);
    }
}

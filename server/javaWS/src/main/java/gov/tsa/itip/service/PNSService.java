package gov.tsa.itip.service;
/**
 *  Created by Michael Jiang on 12/3/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

import com.mongodb.MongoClient;
import com.mongodb.client.FindIterable;
import com.mongodb.client.MongoDatabase;
import gov.tsa.itip.exception.ItipException;
import gov.tsa.itip.models.Device;
import gov.tsa.itip.models.NetResponse;
import javapns.Push;
import jlib.util.Config;
import jlib.util.Helper;
import jlib.util.IOHelper;
import jlib.util.Logger;
import org.bson.Document;
import org.json.JSONException;
import org.json.JSONObject;

import javax.management.MBeanServer;
import javax.management.ObjectName;
import javax.management.Query;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.UnsupportedEncodingException;
import java.lang.management.ManagementFactory;
import java.net.HttpURLConnection;
import java.net.InetAddress;
import java.net.URL;
import java.net.URLEncoder;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.Executors;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.TimeUnit;

public class PNSService {

    public enum MessageType {
        PNSAlert,
        PNSBadge,
        PNSSound,
    };

    public static PNSService me() {
        if (_me == null)
            _me = new PNSService();
        return _me;
    }

    public void init() throws ItipException {
        InputStream in = null;
        try {
            String cfgFile = "/etc/tsaoit/tsaoit.xml";
            File cfgPath = new File(cfgFile);
            in = cfgPath.exists()? new FileInputStream(cfgPath): Thread.currentThread().getContextClassLoader().getResourceAsStream("tsaoit.xml");
            Config.loadFromXML(in);
            try {in.close();} catch (Exception e) {}
            Config cfg = Config.me();
            String log4jConfig = cfg.get("log4j-cfg", "/etc/tsaoit/log4j.xml");
            String logName = cfg.get("log-name", "jlib.util.Logger");
            Logger.init(log4jConfig, logName);
            this.timeoutInMS = 1000 * cfg.get("net-timeout-in-seconds", 300);
            this.dump = cfg.get("dump-output", false);
            this.dbHost = cfg.get("db-host", "localhost");
            this.dbPort = cfg.get("db-port", 27017);
            this.dbName = cfg.get("db-name", "webapp");
            this.passcode = cfg.get("pns-passcode");
            this.keyStoreDev = cfg.get("key-store-dev", "/etc/tsaoit/pns-keys/apns-sendbox-4-tsaoit.p12");
            this.keyStoreProd = cfg.get("key-store-prod", "/etc/tsaoit/pns-keys/apns-production-4-tsaoit.p12");
            this.keyGcmHttp = cfg.get("key-store-gcm", "/etc/tsaoit/pns-keys/gcm-pns-4-tsaoit.p12");
            this.scheduler = Executors.newScheduledThreadPool(cfg.get("thread-pool-count", 8));
            this.baseUrl = "http://";
            String pnsHost = cfg.get("pns-host", "localhost");
            String pnsPort = cfg.get("pns-port", "8080");
            try {
                MBeanServer mbs = ManagementFactory.getPlatformMBeanServer();
                Set<ObjectName> objs = mbs.queryNames(new ObjectName("*:type=Connector,*"), Query.match(Query.attr("protocol"), Query.value("HTTP/1.1")));
                pnsHost = InetAddress.getLocalHost().getHostAddress();
                ObjectName obj = objs.toArray(new ObjectName[0])[0];
                pnsPort = obj.getKeyProperty("port");
            } catch (Exception e) {}
            this.baseUrl += pnsHost + ":";
            this.baseUrl += pnsPort + cfg.get("pns-root-context", "/javaWS");
            String pnsBaseUrl = this.baseUrl + "/api/pns";
            Logger.debug("PNS Base URL: "+pnsBaseUrl, this);
            String tsaoitBaseUrl = cfg.get("tsaoit-api-internal-url", "http://localhost:9000/api");
            NetResponse res = get(tsaoitBaseUrl+"/pns/register/"+URLEncoder.encode(pnsBaseUrl, "UTF-8"));
            JSONObject json = new JSONObject(new String(res.content, "UTF-8"));
            Logger.debug("MONGO DB Info in JSON: \n"+json, this);
            if (dump)
                System.out.println("\nMONGO DB Info in JSON: \n"+json);
            try {
                dbHost = (String)json.get("dbHost");
            } catch (JSONException e) {}
            try {
                dbPort = Helper.num(json.get("dbPort"));
            } catch (JSONException e) {}
            try {
                dbName = (String)json.get("dbName");
            } catch (JSONException e) {}
        } catch (UnsupportedEncodingException e) {
            throw new ItipException(e.getMessage());
        } catch (JSONException e) {
            throw new ItipException(e.getMessage());
        } catch (FileNotFoundException e) {
            throw new ItipException(e.getMessage());
        } catch (IOException e) {
            throw new ItipException(e.getMessage());
        } finally {
            try {in.close();} catch (Exception e) {}
        }
    }

    public int push(String sender, String message, MessageType msgType, boolean badge, String[] grougs, String[] users) throws ItipException {
        MongoClient mongoClient = new MongoClient(dbHost, dbPort);
        MongoDatabase db = mongoClient.getDatabase(dbName);
        List<Device> devices = new ArrayList<Device>();
        FindIterable<Document> iterable = db.getCollection("devices").find();
        for (Document doc: iterable) {
            String ch = doc.getString("channel");
            Device device = new Device(doc.getString("token"));
            Device.PNSChannel channel = Device.PNSChannel.APNSProduction;
            if ("sandbox".equalsIgnoreCase(ch))
                channel = Device.PNSChannel.APNSDevelopment;
            else if ("gcm".equalsIgnoreCase(ch))
                channel = Device.PNSChannel.PNSGcmHttp;
            else
                channel = Device.PNSChannel.APNSProduction;
            device.setChannel(channel);
            device.setName(doc.getString("name"));
            device.setOwner(doc.getString("owner"));
            devices.add(device);
        }
        for (Device dev: devices) {
            scheduler.schedule(new PNSTask(dev, msgType, message, badge), 0, TimeUnit.SECONDS);
        }
        Logger.log("PNSService.push(): Message has been pushed to "+devices.size()+" devices.", this);
        return devices.size();
    }

    class PNSTask implements Runnable {

        PNSTask(Device dev, MessageType mtp, String msg, boolean bdg) {
            device = dev;
            msgType = mtp;
            notice = msg;
            badge = bdg;
        }

        public void run() {
            try {
                Logger.log("Sending essage: "+notice+" to "+device.getName()+" with token="+device.getToken()+" via "+device.getChannel().toString()+" channel started ...", this);
                if (device.getChannel() == Device.PNSChannel.APNSProduction) {
                    if (badge)
                        Push.badge(1, keyStoreProd, passcode, true, device.getToken());
                    if (msgType == MessageType.PNSSound)
                        Push.sound("ona-alert", keyStoreProd, passcode, true, device.getToken());
                    else
                        Push.alert(notice, keyStoreProd, passcode, true, device.getToken());
                } else if (device.getChannel() == Device.PNSChannel.APNSDevelopment) {
                    if (badge)
                        Push.badge(1, keyStoreDev, passcode, false, device.getToken());
                    if (msgType == MessageType.PNSSound)
                        Push.sound("ona-alert", keyStoreDev, passcode, false, device.getToken());
                    else
                        Push.alert(notice, keyStoreDev, passcode, false, device.getToken());
/*
                } else {
                    Message.Builder msgBuilder = new Message.Builder();
                    msgBuilder.addData("alert", notice);
                    Sender gcmSender = new Sender(keyGcmHttp);
                    gcmSender.send(msgBuilder.build(), device.deviceToken, 1);
*/
                }
                Logger.debug("Message: "+notice+" pushed to "+device.getName()+" with token="+device.getToken()+" via "+device.getChannel().toString()+" channel.", this);
            } catch (Exception e) {
                Logger.error("Sending Message: "+notice+" to "+device.getName()+" with token="+device.getToken()+" via "+device.getChannel().toString()+" channel got ERROR=>"+e.getMessage(), this);
            }
        }

        Device device;
        MessageType msgType;
        String notice;
        boolean badge;
    }



    NetResponse get(String urlString) throws ItipException {
        return send(urlString, null);
    }

    NetResponse sendJSON(String urlString, String json) throws ItipException {
        try {
            return send(urlString, null, json.getBytes("UTF-8"), "application/json");
        } catch (UnsupportedEncodingException e) {
            throw new ItipException(e.getMessage());
        }
    }

    NetResponse send(String urlString, byte[] content) throws ItipException {
        return send(urlString, null, content, "application/x-www-form-urlencoded;charset=UTF-8");
    }

    NetResponse send(String urlString, String refer, byte[] content, String contentType) throws ItipException {
        InputStream is = null;
        try {
            if (dump) {
                System.out.println();
                System.out.println("================================================= NEW REQUEST ===============================================");
                System.out.println(urlString);
            }
            URL url = new URL(urlString);
            HttpURLConnection conn = (HttpURLConnection)url.openConnection();
            conn.setUseCaches(false);
            conn.setDoInput(true);
            conn.setAllowUserInteraction(false);
            conn.setInstanceFollowRedirects(true);
            if (!Helper.empty(refer))
                conn.setRequestProperty("Referer", refer);
            if (content == null) {
                conn.setDoOutput(false);
                conn.setRequestMethod("GET");
                conn.connect();
            } else {
                conn.setDoOutput(true);
                conn.setRequestMethod("POST");
                conn.setRequestProperty("Content-Type", contentType);
                conn.setRequestProperty("Content-Length", Helper.str(content.length));
                conn.connect();
                OutputStream os = conn.getOutputStream();
                os.write(content);
                os.flush();
                os.close();
            }
            NetResponse res = new NetResponse(conn.getResponseCode());
            if (dump) {
                System.out.println("==================== RESPONSE["+res.code+"] =====================");
                System.out.println(res.code+" - RES_MSG: "+conn.getResponseMessage());
                System.out.println("RES HEADERS:");
            }
            Map<String, List<String>> hdrMap = conn.getHeaderFields();
            for (String hdrName: hdrMap.keySet()) {
                if (Helper.empty(hdrName))
                    continue;
                List<String> val = hdrMap.get(hdrName);
                for (String v: val) {
                    if (dump)
                        System.out.println(hdrName+": "+Helper.str(v));
                    res.headers.put(hdrName, v);
                    if ("Location".equalsIgnoreCase(hdrName)) {
                        res.loc = Helper.str(v);
                        break;
                    }
                }
            }
            is = conn.getInputStream();
            ByteArrayOutputStream bo = new ByteArrayOutputStream();
            IOHelper io = new IOHelper(is);
            io.writeTo(bo, conn.getContentLength());
            res.content = bo.toByteArray();
            io.close();
            res.refer = (res.code==302)? urlString: null;
            return res;
        } catch (Exception e) {
            if (dump)
                e.printStackTrace();
            throw new ItipException(e.getMessage());
        } finally {
            try {is.close();} catch (Exception e) {}
        }
    }

    public PNSService () {

    }


    long timeoutInMS;
    boolean dump;
    String dbHost;
    int dbPort;
    String dbName;
    String keyStoreDev;
    String keyStoreProd;
    String keyGcmHttp;
    String passcode;
    String baseUrl;
    ScheduledExecutorService scheduler;
    static PNSService _me = null;
}

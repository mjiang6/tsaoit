package gov.tsa.itip.models;
/**
 *  Created by Michael Jiang on 12/3/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

public class Device {
    public enum PNSChannel {
        APNSProduction,
        APNSDevelopment,
        PNSGcmHttp,
    };

    public Device() {
        this(null);
    }

    public Device(String token) {
        this.token = token;
        this.channel = PNSChannel.APNSProduction;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public String getUuid() {
        return uuid;
    }

    public void setUuid(String uuid) {
        this.uuid = uuid;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public PNSChannel getChannel() {
        return channel;
    }

    public void setChannel(PNSChannel channel) {
        this.channel = channel;
    }

    String name;
    String token;
    String uuid;
    String owner;
    PNSChannel channel;
}

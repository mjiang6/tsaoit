package gov.tsa.itip.rest;
/**
 *  Created by Michael Jiang on 12/16/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

import gov.tsa.itip.exception.ItipException;
import gov.tsa.itip.service.PNSService;

import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

public class MyConfig implements ServletContextListener {

    @Override
    public void contextInitialized(ServletContextEvent servletContextEvent) {
        try {
            System.out.println("PNSService started ...");
            PNSService.me().init();
        } catch (Exception e) {
            throw new RuntimeException(e.getMessage());
        }
    }

    @Override
    public void contextDestroyed(ServletContextEvent servletContextEvent) {
        System.out.println("PNSService stopped ...");
    }
}

package gov.tsa.itip.rest;
/**
 *  Created by Michael Jiang on 12/3/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */


import jlib.util.Helper;
import gov.tsa.itip.service.PNSService;
import org.json.JSONObject;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("pns")
public class PNS {

    @GET
    @Path("/test/{name}")
    @Produces(MediaType.APPLICATION_JSON)
    public String hello(@PathParam("name") String name) {
        PNSService.me();
        return "Hello: " + name + "!";
    }

    @POST
    @Produces(MediaType.APPLICATION_JSON)
    @Consumes(MediaType.APPLICATION_JSON)
    public String push(String messageJSON) {
        try {
            JSONObject json = new JSONObject(messageJSON);
            String sender = toStr(json, "sender");
            JSONObject notification = json.getJSONObject("notification");
            String msg = toStr(notification, "message")+" ["+toStr(notification, "messageId")+"]";
            JSONObject alerts = json.getJSONObject("alerts");
            boolean sound = Helper.bool(toStr(alerts, "Sound"));
            boolean badge = Helper.bool(toStr(alerts, "Badge"));
            int n = PNSService.me().push(sender, msg, sound? PNSService.MessageType.PNSSound: PNSService.MessageType.PNSAlert, badge, new String[0], new String[0]);
            return "{\"status\": \"GOOD\", \"statusMessage\": \"Message has been pushed to "+n+" devices.\"}";
        } catch (Exception e) {
            e.printStackTrace();
            return "{\"status\": \"ERROR\", \"statusMessage\": \""+e.getMessage()+"\"}";
        }
    }

    static String toStr(JSONObject obj, String key) {
        try {
            return obj==null? "": Helper.trim(obj.getString(key));
        } catch (Exception e) {
            return "";
        }
    }
}

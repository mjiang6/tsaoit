package gov.tsa.itip.rest;
/**
 *  Created by Michael Jiang on 12/3/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */

import javax.ws.rs.ApplicationPath;
import gov.tsa.itip.service.PNSService;

import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class MyApp extends Application {
    public Set<Class<?>> getClasses() {
        PNSService.me();
        final Set<Class<?>> classes = new HashSet<Class<?>>();
        return classes;
    }
}

package gov.tsa.itip.models;
/**
 *  Created by Michael Jiang on 12/4/2015.
 *  Copyright © 2015 CSRA. All rights reserved.
 */


import java.util.HashMap;
import java.util.Map;

public class NetResponse {
    public NetResponse(int code) {
        this.code = code;
        this.headers = new HashMap<String, String>();
        this.cookies = "";
    }
    public String loc;
    public String cookies;
    public String refer;
    public String redirectedUrl;
    public String contentType;
    public byte[] content;
    public int code;
    public Map<String, String> headers;
}

/**
 * Main application routes
 */

'use strict';

var errors = require('./components/errors');
var path = require('path');

module.exports = function(app) {

  // Insert routes below
  app.use('/api/incidents', require('./api/incident'));
  app.use('/api/comments', require('./api/comment'));
  app.use('/api/incidentTypes', require('./api/incidentType'));
  app.use('/api/siteCodes', require('./api/siteCode'));
  app.use('/api/severity', require('./api/severity'));
  app.use('/api/devices', require('./api/device'));

  // For PNS server registration
  app.use('/api/pns/', require('./api/pns'));

  // For authentication
  app.use('/api/auth/', require('./api/auth'));

  // All undefined asset or api routes should return a 404
  app.route('/:url(api|auth|components|app|bower_components|assets)/*')
   .get(errors[404]);

  // All other routes should redirect to the index.html
  app.route('/*')
    .get(function(req, res) {
      res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
    });
};

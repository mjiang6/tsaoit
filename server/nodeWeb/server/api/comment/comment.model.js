'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var CommentSchema = new Schema({
  text: String,
  type: String,
  username: String,
  created_time: { type : Date, default: Date.now },

  // References
  // user: {type : mongoose.Schema.ObjectId, ref : 'User'},
});

module.exports = mongoose.model('Comment', CommentSchema);

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /siteCodes              ->  index
 * POST    /siteCodes              ->  create
 * GET     /siteCodes/:id          ->  show
 * PUT     /siteCodes/:id          ->  update
 * DELETE  /siteCodes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var SiteCode = require('./siteCode.model');

// Get list of siteCodes
exports.index = function(req, res) {
  var category = req.query.category;
  console.log('{Filter: {category: "', category, '"}}');
  if (!category || category === 'ALL') {
    SiteCode.find(function (err, siteCodes) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(siteCodes);
    });
  } else {
    SiteCode.find({'category': category}, function (err, siteCodes) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(siteCodes);
    });
  }
};

// Get list of siteCodes
exports.sub = function(req, res) {
  SiteCode.find(function (err, siteCodes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(siteCodes);
  });
};

// Get a single siteCode
exports.show = function(req, res) {
  SiteCode.findById(req.params.id, function (err, siteCode) {
    if(err) { return handleError(res, err); }
    if(!siteCode) { return res.status(404).send('Not Found'); }
    return res.json(siteCode);
  });
};

// Creates a new siteCode in the DB.
exports.create = function(req, res) {
  SiteCode.create(req.body, function(err, siteCode) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(siteCode);
  });
};

// Updates an existing siteCode in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  SiteCode.findById(req.params.id, function (err, siteCode) {
    if (err) { return handleError(res, err); }
    if(!siteCode) { return res.status(404).send('Not Found'); }
    var updated = _.merge(siteCode, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(siteCode);
    });
  });
};

// Deletes a siteCode from the DB.
exports.destroy = function(req, res) {
  SiteCode.findById(req.params.id, function (err, siteCode) {
    if(err) { return handleError(res, err); }
    if(!siteCode) { return res.status(404).send('Not Found'); }
    siteCode.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}

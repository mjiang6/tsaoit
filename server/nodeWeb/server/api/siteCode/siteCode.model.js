'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SiteCodeSchema = new Schema({
  code: String,
  category: String,
  address: String,
  city: String,
  state: String,
  zipCode: String,
  longitude: String,
  latitude: String
});

module.exports = mongoose.model('SiteCode', SiteCodeSchema);

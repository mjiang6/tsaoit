'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var IncidentTypeSchema = new Schema({
  description: String

  // References
  // user: {type : mongoose.Schema.ObjectId, ref : 'User'}, 
});

module.exports = mongoose.model('IncidentType', IncidentTypeSchema);
/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /incidentTypes              ->  index
 * POST    /incidentTypes              ->  create
 * GET     /incidentTypes/:id          ->  show
 * PUT     /incidentTypes/:id          ->  update
 * DELETE  /incidentTypes/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var IncidentType = require('./incidentType.model');

// Get list of incidentTypes
exports.index = function(req, res) {
  IncidentType.find(function (err, incidentTypes) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(incidentTypes);
  });
};

// Get a single incidentType
exports.show = function(req, res) {
  IncidentType.findById(req.params.id, function (err, incidentType) {
    if(err) { return handleError(res, err); }
    if(!incidentType) { return res.status(404).send('Not Found'); }
    return res.json(incidentType);
  });
};

// Creates a new incidentType in the DB.
exports.create = function(req, res) {
  IncidentType.create(req.body, function(err, incidentType) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(incidentType);
  });
};

// Updates an existing incidentType in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  IncidentType.findById(req.params.id, function (err, incidentType) {
    if (err) { return handleError(res, err); }
    if(!incidentType) { return res.status(404).send('Not Found'); }
    var updated = _.merge(incidentType, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(incidentType);
    });
  });
};

// Deletes a incidentType from the DB.
exports.destroy = function(req, res) {
  IncidentType.findById(req.params.id, function (err, incidentType) {
    if(err) { return handleError(res, err); }
    if(!incidentType) { return res.status(404).send('Not Found'); }
    incidentType.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /incidents              ->  index
 * POST    /incidents              ->  create
 * GET     /incidents/:id          ->  show
 * PUT     /incidents/:id          ->  update
 * DELETE  /incidents/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var mongoose = require('mongoose');
var Incident = require('./incident.model');
var Message = require('../pns/pns.model');
var http = require('http');
var url = require('url');

var PNS_BASE_URL = 'http://localhost:8080/javaWS/api/pns';

// Get list of incidents
exports.index = function(req, res) {
  var filter = req.query.filter;
  console.log('{Filter: {filter: "', filter, '"}}');
  Incident.find({})
    .populate('incident_type')
    .populate('site_code')
    .populate('severity')
    .populate('comments')
    .exec(function (err, incidents) {
      if(err) { return handleError(res, err); }
      return res.status(200).json(!filter || 'all' === filter.toLowerCase()? incidents:  _.filter(incidents, function(incident) {
        if ('Open' === filter) {
          return !incident.completed;
        }
        if ('Initial' === filter) {
          return (incident.incident_type && 'Initial' === incident.incident_type.description);
        }
        if ('Low' === filter) {
          return incident.severity.level === 'S3';
        }
        if ('Medium' === filter) {
          return incident.severity.level === 'S2';
        }
        if ('High' === filter) {
          return incident.severity.level === 'S1';
        }
        return incident.severity.level === filter;
      }));
  });
};

// Get a single incident
exports.show = function(req, res) {
  Incident.findById(req.params.id)
    .populate('incident_type')
    .populate('site_code')
    .populate('severity')
    .populate('comments')
    .exec(function (err, incident) {
      if(err) { return handleError(res, err); }
      if(!incident) { return res.status(404).send('Not Found'); }
      return res.json(incident);
  });
};

// Creates a new incident in the DB.
exports.create = function(req, res) {

  if (!req.body._id)  { // Create
    req.body._id = mongoose.Types.ObjectId();
    Incident.create(req.body, function(err, incident) {
      if(err) { return handleError(res, err); }
      Message.create({
        message : 'A new incident has been reported',
        type: 'new',
        status: 'created',
        incident: incident
      }, function(err, message) {
        if(err) { return handleError(res, err); }
        // before return, send notification ...
        var urlParts = url.parse(global.pnsURL || PNS_BASE_URL);
        var notification = '{"notification":{"message":"'+message.message+'","messageId":"'+message._id+'"},"alerts":{"Badge":"true"}}';
        var request = new http.request({
          hostname: urlParts.hostname,
          port: urlParts.port,
          path: urlParts.pathname,
          method: 'POST',
          headers: {'Content-Type': 'application/json'}
        }, function(res) {
          res.on('data', function (body) {
            console.log('Notification Body: ' + body);
          });
        });
        request.on('error', function(e) {
          console.log('problem with request: ' + e.message);
        });
        request.write(notification, 'utf8');
        request.end();
        return res.status(201).json(incident);
      });
    });
  } else { // Update
    Incident.findOneAndUpdate({_id: req.body._id}, req.body, function(err, incident) {
      if(err) { return handleError(res, err); }
      Message.create({
        message : 'An incident has been updated',
        type: 'update',
        status: 'updated',
        incident: incident
      }, function(err, message) {
        if(err) { return handleError(res, err); }
        // before return, send notification ...
        var urlParts = url.parse(global.pnsURL || PNS_BASE_URL);
        var notification = '{"notification":{"message":"'+message.message+'","messageId":"'+message._id+'"},"alerts":{"Badge":"true"}}';
        var request = new http.request({
          hostname: urlParts.hostname,
          port: urlParts.port,
          path: urlParts.pathname,
          method: 'POST',
          headers: {'Content-Type': 'application/json'}
        }, function(res) {
          res.on('data', function (body) {
            console.log('Notification Body: ' + body);
          });
        });
        request.on('error', function(e) {
          console.log('problem with request: ' + e.message);
        });
        request.write(notification, 'utf8');
        request.end();
        return res.status(201).json(incident);
      });
    });
  }
};

// Updates an existing incident in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Incident.findById(req.params.id, function (err, incident) {
    if (err) { return handleError(res, err); }
    if(!incident) { return res.status(404).send('Not Found'); }
    var updated = _.merge(incident, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      Message.create({
        message : 'An incident has been updated',
        type: 'update',
        status: 'updated',
        incident: incident
      }, function(err, message) {
        if(err) { return handleError(res, err); }
        // before return, send notification ...
        var urlParts = url.parse(global.pnsURL || PNS_BASE_URL);
        var notification = '{"notification":{"message":"'+message.message+'","messageId":"'+message._id+'"},"alerts":{"Badge":"true"}}';
        var request = new http.request({
          hostname: urlParts.hostname,
          port: urlParts.port,
          path: urlParts.pathname,
          method: 'POST',
          headers: {'Content-Type': 'application/json'}
        }, function(res) {
          res.on('data', function (body) {
            console.log('Notification Body: ' + body);
          });
        });
        request.on('error', function(e) {
          console.log('problem with request: ' + e.message);
        });
        request.write(notification, 'utf8');
        request.end();
        return res.status(201).json(incident);
      });
    });
  });
};

// Deletes a incident from the DB.
exports.destroy = function(req, res) {
  Incident.findById(req.params.id, function (err, incident) {
    if(err) { return handleError(res, err); }
    if(!incident) { return res.status(404).send('Not Found'); }
    incident.remove(function(err) {
      if(err) { return handleError(res, err); }
      Message.create({
        message : 'An incident has been deleted',
        type: 'update',
        status: 'deleted',
        incident: incident
      }, function(err, message) {
        if(err) { return handleError(res, err); }
        // before return, send notification ...
        var urlParts = url.parse(global.pnsURL || PNS_BASE_URL);
        var notification = '{"notification":{"message":"'+message.message+'","messageId":"'+message._id+'"},"alerts":{"Badge":"true"}}';
        var request = new http.request({
          hostname: urlParts.hostname,
          port: urlParts.port,
          path: urlParts.pathname,
          method: 'POST',
          headers: {'Content-Type': 'application/json'}
        }, function(res) {
          res.on('data', function (body) {
            console.log('Notification Body: ' + body);
          });
        });
        request.on('error', function(e) {
          console.log('problem with request: ' + e.message);
        });
        request.write(notification, 'utf8');
        request.end();
        return res.status(201).json(incident);
      });
    });
  });
};

function handleError(res, err) {
  console.log(err);
  return res.status(500).send(err);
}

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var IncidentSchema = new Schema({
  name: String,
  description: String,
  reported_time: { type : Date, default: Date.now },
  incident_time: { type : Date, default: Date.now },

  completed: {type: Boolean, default: false},


  // References
  site_code: {type : mongoose.Schema.ObjectId, ref : 'SiteCode'},
  incident_type: {type : mongoose.Schema.ObjectId, ref : 'IncidentType'},
  severity: {type : mongoose.Schema.ObjectId, ref : 'Severity'},
  comments: [ {type : mongoose.Schema.ObjectId, ref : 'Comment'} ]
  // user: {type : mongoose.Schema.ObjectId, ref : 'User'},
});

module.exports = mongoose.model('Incident', IncidentSchema);
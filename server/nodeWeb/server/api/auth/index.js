'use strict';

var express = require('express');
var controller = require('./auth.controller');

var router = express.Router();

router.post('/', controller.login);
router.post('/logout', controller.logout);
router.get('/logout/:username', controller.logout);

module.exports = router;

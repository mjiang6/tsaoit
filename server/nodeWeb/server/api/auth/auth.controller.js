'use strict';
var _ = require('lodash');
var Device = require('../device/device.model');

exports.login = function(req, res) {
  var uid = req.body.username;
  var pwd = req.body.password;
  console.log("User ", uid, " logged in ...");
  if (req.body.token) {
    Device.find({token: req.body.token}, function (err, devices) {
      if (err) { return handleError(res, err); }
      if (devices && devices.length > 0) {
        var updated = _.merge(devices[0], {owner: uid});
        updated.save(function (err) {
          if (err) { return handleError(res, err); }
          console.log("Device owner updated to ", uid, " ...");
        });
      }
    });
  }
  return res.status(201).json('{"status":"AUTHENTICATED"}');
};

exports.logout = function(req, res) {
  var uid = req.body.username;
  console.log("User ", uid, " logged out ...");
  return res.status(201).json('{"status":"NOT_AUTHENTICATED"}');
};

function handleError(res, err) {
  return res.status(500).send(err);
}

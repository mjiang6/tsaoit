'use strict';

var _ = require('lodash');
var url = require('url');
var config = require('../../config/environment');
var Message = require('./pns.model');

// Get list of PNS messages
exports.index = function(req, res) {
  Message.find({})
    .populate('incident')
    .exec(function (err, messages) {
      if (err) { return handleError(res, err); }
      var mlist = [];
      _.forEach(messages, function(msg) {
        var m = {
          message: msg.message,
          messageId: msg._id,
          incidentTitle: msg.incident.name,
          incidentId: msg.incident._id,
          create_time:msg.create_time
        };
        mlist.push(m);
      });
      return res.status(200).json(mlist);
    });
};

// Get a single PNS message
exports.show = function(req, res) {
  Message.findById(req.params.id)
    .populate('incident')
    .exec(function (err, message) {
      if(err) { return handleError(res, err); }
      if(!message) { return res.status(404).send('PNS Message Not Found'); }
      var m = {
        message: message.message,
        messageId: message._id,
        incidentTitle: message.incident.name,
        incidentId: message.incident._id,
        create_time:message.create_time
      };

      return res.json(m);
    });
};

// Creates a new PNS message in the DB.
exports.create = function(req, res) {
  Message.create(req.body, function(err, message) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(message);
  });
};

// Updates an existing PNS message in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Message.findById(req.params.id, function (err, message) {
    if (err) { return handleError(res, err); }
    if(!message) { return res.status(404).send('PNS Message Not Found'); }
    var updated = _.merge(message, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(message);
    });
  });
};

// Deletes a PNS message from the DB.
exports.destroy = function(req, res) {
  Message.findById(req.params.id, function (err, message) {
    if(err) { return handleError(res, err); }
    if(!message) { return res.status(404).send('PNS Message Not Found'); }
    message.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

// Register PNS server URL for pushing notifications
exports.register = function(req, res) {
  global.pnsURL = req.params.pnsURL;      // URL for POST notification.json
  console.log("pnsURL: ", global.pnsURL);
  var dbParts = url.parse(config.mongo.uri);
  console.log("dbHost: ", dbParts.hostname);
  console.log("dbPort: ", dbParts.port || '27017');
  console.log("dbName: ", dbParts.pathname.substr(1));
  return res.json({
    dbHost: dbParts.hostname,
    dbPort: dbParts.port || '27017',
    dbName: dbParts.pathname.substr(1)
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}

'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var MessageSchema = new Schema({
  message: String,
  type: String,
  status: String,
  create_time: { type : Date, default: Date.now },

  // References
  incident: {type : mongoose.Schema.ObjectId, ref : 'Incident'},
  // user: {type : mongoose.Schema.ObjectId, ref : 'User'},
});

module.exports = mongoose.model('Message', MessageSchema);

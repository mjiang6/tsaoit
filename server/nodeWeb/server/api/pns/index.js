'use strict';

var express = require('express');
var controller = require('./pns.controller');

var router = express.Router();

router.get('/messages', controller.index);
router.get('/messages/:id', controller.show);
router.post('/messages', controller.create);
router.put('/messages/:id', controller.update);
router.patch('/messages/:id', controller.update);
router.delete('/messages/:id', controller.destroy);
router.get('/register/:pnsURL', controller.register);

module.exports = router;

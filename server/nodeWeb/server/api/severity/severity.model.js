'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var SeveritySchema = new Schema({
  level: String,
   warning_level: {type: String, enum: ['red', 'yellow', 'green'], default: 'yellow'}
});

module.exports = mongoose.model('Severity', SeveritySchema);
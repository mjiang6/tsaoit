/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /severitys              ->  index
 * POST    /severitys              ->  create
 * GET     /severitys/:id          ->  show
 * PUT     /severitys/:id          ->  update
 * DELETE  /severitys/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Severity = require('./severity.model');

// Get list of severitys
exports.index = function(req, res) {
  Severity.find(function (err, severitys) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(severitys);
  });
};

// Get a single severity
exports.show = function(req, res) {
  Severity.findById(req.params.id, function (err, severity) {
    if(err) { return handleError(res, err); }
    if(!severity) { return res.status(404).send('Not Found'); }
    return res.json(severity);
  });
};

// Creates a new severity in the DB.
exports.create = function(req, res) {
  Severity.create(req.body, function(err, severity) {
    if(err) { return handleError(res, err); }
    return res.status(201).json(severity);
  });
};

// Updates an existing severity in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Severity.findById(req.params.id, function (err, severity) {
    if (err) { return handleError(res, err); }
    if(!severity) { return res.status(404).send('Not Found'); }
    var updated = _.merge(severity, req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      return res.status(200).json(severity);
    });
  });
};

// Deletes a severity from the DB.
exports.destroy = function(req, res) {
  Severity.findById(req.params.id, function (err, severity) {
    if(err) { return handleError(res, err); }
    if(!severity) { return res.status(404).send('Not Found'); }
    severity.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}
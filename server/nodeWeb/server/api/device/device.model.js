'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var DeviceSchema = new Schema({
  name: String,
  token: String,
  uuid: String,
  owner: String,
  channel: String,
  created_time: { type : Date, default: Date.now },

  // References
  // user: {type : mongoose.Schema.ObjectId, ref : 'User'},
});

module.exports = mongoose.model('Device', DeviceSchema);

/**
 * Using Rails-like standard naming convention for endpoints.
 * GET     /devices              ->  index
 * POST    /devices              ->  create
 * GET     /devices/:id          ->  show
 * PUT     /devices/:token       ->  update
 * DELETE  /devices/:id          ->  destroy
 */

'use strict';

var _ = require('lodash');
var Device = require('./device.model');

// Get list of devices
exports.index = function(req, res) {
  Device.find(function (err, devices) {
    if(err) { return handleError(res, err); }
    return res.status(200).json(devices);
  });
};

// Get a single device
exports.show = function(req, res) {
  Device.findById(req.params.id, function (err, device) {
    if(err) { return handleError(res, err); }
    if(!device) { return res.status(404).send('Not Found'); }
    return res.json(device);
  });
};

// Creates a new device in the DB.
exports.create = function(req, res) {
  Device.find({token: req.body.token}, function (err, devices) {
    if (err) {return handleError(res, err);}
    if (devices && devices.length > 0) {
      var updated = _.merge(devices[0], req.body);
      updated.save(function (err) {
        if (err) { return handleError(res, err); }
        console.log("Device re-registered: ", updated);
        return res.status(200).json(updated);
      });
    } else {
      Device.create(req.body, function(err, device) {
        if(err) { return handleError(res, err); }
        console.log("Device registered: ", device);
        return res.status(201).json(device);
      });
    }
  });
};

// Updates an existing device in the DB.
exports.update = function(req, res) {
  if(req.body._id) { delete req.body._id; }
  Device.find({token: req.body.token}, function (err, devices) {
    if (err) { return handleError(res, err); }
    if(!devices || devices.length <= 0) { return res.status(404).send('Not Found'); }
    var updated = _.merge(devices[0], req.body);
    updated.save(function (err) {
      if (err) { return handleError(res, err); }
      console.log("Device updated: ", devices[0]);
      return res.status(200).json(devices[0]);
    });
  });
};

// Deletes a device from the DB.
exports.destroy = function(req, res) {
  Device.findById(req.params.id, function (err, device) {
    if(err) { return handleError(res, err); }
    if(!device) { return res.status(404).send('Not Found'); }
    device.remove(function(err) {
      if(err) { return handleError(res, err); }
      return res.status(204).send('No Content');
    });
  });
};

function handleError(res, err) {
  return res.status(500).send(err);
}

'use strict';

// Development specific configuration
// ==================================
module.exports = {


	// Server port
  port:     process.env.PORT ||
            9000,

  // SSL Server port
  port_ssl: process.env.SSL_PORT ||
            443,

  // MongoDB connection options
  mongo: {
    uri: 'mongodb://localhost/webapp-dev'
  },

  seedDB: false
};

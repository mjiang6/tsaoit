/**
 * Populate DB with sample data on server start
 * to disable, edit config/environment/index.js, and set `seedDB: false`
 */

'use strict';
var _ = require('lodash');
var q = require('q')

var Incident = require('../api/incident/incident.model');
var Comment = require('../api/comment/comment.model');
var IncidentType = require('../api/incidentType/incidentType.model');
var SiteCode = require('../api/siteCode/siteCode.model');
var Severity = require('../api/severity/severity.model');
var Device = require('../api/device/device.model');
var Message = require('../api/pns/pns.model');


populateAll();


function populateAll() {
  var deferred = q.defer();
  q.all([
      populateIncidentTypes(),
      populateSiteCodes(),
      populateSeverities(),
      populateComments(),
      populateDevices()
    ]).then(function() {
      Comment.find(function(err, comments) {
        IncidentType.find(function(err, incidentTypes) {
          SiteCode.find(function(err, siteCodes) {
            Severity.find(function(err, severities) {
              populateIncidents(comments, incidentTypes, siteCodes, severities).then(function () {
                Incident.find(function(err, incidents) {
                  populateMessages(incidents);
                });
              });
            });
          });
        });
      });
  });
}

function populateIncidentTypes() {
  var deferred = q.defer();
  IncidentType.create({
        description : 'Initial'
      }, {
        description : 'Server Down'
      }, {
        description : 'Maintenance'
    }, function(err, comment) {
        deferred.resolve();
    });
  return deferred.promise;
}

function populateSiteCodes() {
  var deferred = q.defer();
  SiteCode.create({
    code: 'ATL',
    category: 'X',
    address: '6000 North Terminal Parkway Suite 435',
    city: 'Atlanta',
    state: 'GA',
    zipCode: '30320'
  }, {
    code: 'BOS',
    category: 'X',
    address: 'One Harborside Dr. Suite 200S',
    city: 'S. Boston',
    state: 'MA',
    zipCode: '02128'
  }, {
    code: 'BWI',
    category: 'X',
    address: '7062 Aviation Blvd',
    city: 'Linthicum',
    state: 'MD',
    zipCode: '21240'
  }, {
    code: 'CLT',
    category: 'X',
    address: '5501 Josh Birmingham Parkway',
    city: 'Charlotte',
    state: 'NC',
    zipCode: '28208'
  }, {
    code: 'CVG',
    category: 'X',
    address: '3013 Terminal Dr',
    city: 'Hebron',
    state: 'KY',
    zipCode: '41048'
  }, {
    code: 'DCA',
    category: 'X',
    address: 'Metropolitan Washington Airports Authority 1 Aviation Circle',
    city: 'Washington',
    state: 'DC',
    zipCode: '20001'
  }, {
    code: 'DEN',
    category: 'X',
    address: '8700 Pena Blvd., Room 3074',
    city: 'Denver',
    state: 'CO',
    zipCode: '80249'
  }, {
    code: 'DFW',
    category: 'X',
    address: 'Terminal A DFW Airport',
    city: 'Dallas',
    state: 'TX',
    zipCode: '75261'
  }, {
    code: 'DTW',
    category: 'X',
    address: '2583 World Gateway Place',
    city: 'Romulus',
    state: 'MI',
    zipCode: '48242'
  }, {
    code: 'EWR',
    category: 'X',
    address: 'Newark Liberty International Airport',
    city: 'Newark',
    state: 'NJ',
    zipCode: '07114'
  }, {
    code: 'HNL',
    category: 'X',
    address: '300 Rodgers Blvd. #45',
    city: 'Honolulu',
    state: 'HI',
    zipCode: '96819'
  }, {
    code: 'IAD',
    category: 'X',
    address: '1 Saarinen Circle',
    city: 'Dulles',
    state: 'VA',
    zipCode: '20166'
  }, {
    code: 'IAH',
    category: 'X',
    address: '3500 N. Terminal Road',
    city: 'Houston',
    state: 'TX',
    zipCode: '77032'
  }, {
    code: 'JFK',
    category: 'X',
    address: 'John F. Kennedy International Airport',
    city: 'Jamaica',
    state: 'NY',
    zipCode: '11430'
  }, {
    code: 'LAS',
    category: 'X',
    address: '5757 Wayne Newton Blvd.',
    city: 'Las Vegas',
    state: 'NV',
    zipCode: '89119'
  }, {
    code: 'LAX',
    category: 'X',
    address: '100 Worldway',
    city: 'Los Angles',
    state: 'CA',
    zipCode: '90045'
  }, {
    code: 'LGA',
    category: 'X',
    address: 'TSA, LaGuardia Airport, Central Terminal Building Room 3815',
    city: 'Flushing',
    state: 'NY',
    zipCode: '11371'
  }, {
    code: 'MCO',
    category: 'X',
    address: '9265B Airport Boulevard',
    city: 'Orlando',
    state: 'FL',
    zipCode: '32827'
  }, {
    code: 'MIA',
    category: 'X',
    address: '4200 NW 21 Street',
    city: 'Miami',
    state: 'FL',
    zipCode: '33142'
  }, {
    code: 'MSP',
    category: 'X',
    address: 'Lindbergh Terminal - 4300 Glumack Dr. Suite C-1137',
    city: 'Minneapolis',
    state: 'MN',
    zipCode: '55111'
  }, {
    code: 'ORD',
    category: 'X',
    address: '1000 W O\'Hare Avenue Rotunda Mezz betweeen termials 4/5',
    city: 'Chicago (O\'Hare)',
    state: 'IL',
    zipCode: '60666-1010'
  }, {
    code: 'PHL',
    category: 'X',
    address: 'Philadelphia International Airport City of Philadelphia',
    city: 'Philadelphia',
    state: 'PA',
    zipCode: '19153'
  }, {
    code: 'PHX',
    category: 'X',
    address: '3800 Sky Harbor Blvd.',
    city: 'Phoenix',
    state: 'AZ',
    zipCode: '85008'
  }, {
    code: 'SEA',
    category: 'X',
    address: '17801 Pacific HWY SO, Rm MT6031',
    city: 'Seattle',
    state: 'WA',
    zipCode: '98158'
  }, {
    code: 'SFO',
    category: 'X',
    address: 'P.O. Box 251926 (Mail) - International Terminal - South Shoulder, 5th Floor - Suite 114 (No Cargo)',
    city: 'San Francisco',
    state: 'CA',
    zipCode: '94125'
  }, {
    code: 'SJU',
    category: 'X',
    address: 'Luis Muñoz Marin International Airport,  Terminal D, suite 4010',
    city: 'Carolina',
    state: 'PR',
    zipCode: '00979'
  }, {
    code: 'STL',
    category: 'X',
    address: '10701 Lambert International Blvd',
    city: 'St. Louis',
    state: 'MO',
    zipCode: '63145'
  }, {
    code: 'ABQ',
    category: 'I',
    address: '2200 Sunport Blvd',
    city: 'Albuquerque',
    state: 'NM',
    zipCode: '87106'
  }, {
    code: 'ALB',
    category: 'I',
    address: '737 Albany-Shaker Road',
    city: 'Albany',
    state: 'NY',
    zipCode: '12211'
  }, {
    code: 'ANC',
    category: 'I',
    address: '4600 Postmark Drive',
    city: 'Anchorage',
    state: 'AK',
    zipCode: '99502'
  }, {
    code: 'AUS',
    category: 'I',
    address: '3600 Presidential Blvd',
    city: 'Austin',
    state: 'TX',
    zipCode: '78719'
  }, {
    code: 'BDL',
    category: 'I',
    address: 'Bradley Int\'L Airport',
    city: 'Windsor Locks',
    state: 'CT',
    zipCode: '06096'
  }, {
    code: 'BHM',
    category: 'I',
    address: '5900 Messer Airport Highway',
    city: 'Birmingham',
    state: 'AL',
    zipCode: '35212'
  }, {
    code: 'BNA',
    category: 'I',
    address: '1 Terminal Drive, Suite 321',
    city: 'Nashville',
    state: 'TN',
    zipCode: '37214'
  }, {
    code: 'BOI',
    category: 'I',
    address: '3201 Airport Wy, Ste 1200',
    city: 'Boise',
    state: 'ID',
    zipCode: '83705'
  }, {
    code: 'BUF',
    category: 'I',
    address: '4200 Genesee Street',
    city: 'Cheektowaga',
    state: 'NY',
    zipCode: '14225'
  }, {
    code: 'BUR',
    category: 'I',
    address: '2627 N. Hollywood Way',
    city: 'Burbank',
    state: 'CA',
    zipCode: '91505'
  }, {
    code: 'CLE',
    category: 'I',
    address: '5300 Riverside Dr.',
    city: 'Cleveland',
    state: 'OH',
    zipCode: '44135'
  }, {
    code: 'CMH',
    category: 'I',
    address: '4600 International Gateway',
    city: 'Columbus',
    state: 'OH',
    zipCode: '43219'
  }, {
    code: 'COS',
    category: 'I',
    address: '7770 Milton E. Proby Pkwy',
    city: 'Colorado Springs',
    state: 'CO',
    zipCode: '80916'
  }, {
    code: 'DAL',
    category: 'I',
    address: '8008 Cedar Spring Road, Suite 13',
    city: 'Dallas',
    state: 'TX',
    zipCode: '75235'
  }, {
    code: 'DAY',
    category: 'I',
    address: '3600 Terminal Drive, Tower Room 212',
    city: 'Dayton',
    state: 'OH',
    zipCode: '45377'
  }, {
    code: 'ELP',
    category: 'I',
    address: '6701 Convair Rd',
    city: 'El Paso',
    state: 'TX',
    zipCode: '79925'
  }, {
    code: 'FLL',
    category: 'I',
    address: '320 Terminal Drive',
    city: 'Ft. Lauderdale',
    state: 'FL',
    zipCode: '33315'
  }, {
    code: 'GEG',
    category: 'I',
    address: '9000 West Airport Drive',
    city: 'Spokane',
    state: 'WA',
    zipCode: '99224'
  }, {
    code: 'GSO',
    category: 'I',
    address: '6415 Bryan Blvd',
    city: 'Greensboro',
    state: 'NC',
    zipCode: '27409'
  }, {
    code: 'GUM',
    category: 'I',
    address: '355 Chalan Pasaheru, Suite C270',
    city: 'Tamuning',
    state: 'GU',
    zipCode: '96913'
  }, {
    code: 'HOU',
    category: 'I',
    address: '7800 Airport Boulevard',
    city: 'Houston',
    state: 'TX',
    zipCode: '77061'
  }, {
    code: 'IND',
    category: 'I',
    address: '2500 South High School Road',
    city: 'Indianappolis',
    state: 'IN',
    zipCode: '46241'
  }, {
    code: 'JAX',
    category: 'I',
    address: '2400 Yankee Clipper Drive',
    city: 'Jacksonville',
    state: 'FL',
    zipCode: '32218'
  }, {
    code: 'KOA',
    category: 'I',
    address: '73-202 Kupipi St',
    city: 'Kailua Kona',
    state: 'HI',
    zipCode: '96740-2645'
  }, {
    code: 'LGB',
    category: 'I',
    address: '4225 Donald Douglas Dr',
    city: 'Long Beach',
    state: 'CA',
    zipCode: '90808'
  }, {
    code: 'LIH',
    category: 'I',
    address: '3901 Mokulele Loop, Unit 30',
    city: 'Lihue',
    state: 'HI',
    zipCode: '96766'
  }, {
    code: 'LIT',
    category: 'I',
    address: 'One Airport Road',
    city: 'Little Rock',
    state: 'AR',
    zipCode: '72202 72202'
  }, {
    code: 'MCI',
    category: 'I',
    address: 'Amsterdam Circle - A Term',
    city: 'Kansas City',
    state: 'MO',
    zipCode: '64163'
  }, {
    code: 'MDW',
    category: 'I',
    address: '5700 Cicero Ave',
    city: 'Chicago (Midway)',
    state: 'IL',
    zipCode: '60638'
  }, {
    code: 'MEM',
    category: 'I',
    address: '2491 Winchester Rd',
    city: 'Memphis',
    state: 'TN',
    zipCode: '38116'
  }, {
    code: 'MHT',
    category: 'I',
    address: 'One Airport Road',
    city: 'Manchester',
    state: 'NH',
    zipCode: '03103'
  }, {
    code: 'MKE',
    category: 'I',
    address: '5007 S. Howell, Ave',
    city: 'Milwalkee',
    state: 'WI',
    zipCode: '53207'
  }, {
    code: 'MSY',
    category: 'I',
    address: '900 Airline Highway',
    city: 'Kenner',
    state: 'LA',
    zipCode: '70062'
  }, {
    code: 'OAK',
    category: 'I',
    address: '1 Airport Drive',
    city: 'Oakland',
    state: 'CA',
    zipCode: '94621'
  }, {
    code: 'OGG',
    category: 'I',
    address: '1 Kahului Airport Road, Unit 19',
    city: 'Kahului',
    state: 'HI',
    zipCode: '96732'
  }, {
    code: 'OKC',
    category: 'I',
    address: '7100 Terminal Drive, Room 11011',
    city: 'Oklahoma City',
    state: 'OK',
    zipCode: '73159'
  }, {
    code: 'OMA',
    category: 'I',
    address: '4501 Abbott Drive, Ste 2300',
    city: 'Omaha',
    state: 'NE',
    zipCode: '68110'
  }, {
    code: 'ONT',
    category: 'I',
    address: '2900 E. Airport Drive',
    city: 'Ontario',
    state: 'CA',
    zipCode: '91761'
  }, {
    code: 'ORF',
    category: 'I',
    address: 'N Military Hwy & Robin Hood Rd',
    city: 'Norfolk',
    state: 'VA',
    zipCode: '23518'
  }, {
    code: 'PBI',
    category: 'I',
    address: '1000 Turnage Boulevard',
    city: 'West Palm Beach',
    state: 'FL',
    zipCode: '33406'
  }, {
    code: 'PDX',
    category: 'I',
    address: '7000 N.E. Airport Way',
    city: 'Portland',
    state: 'OR',
    zipCode: '97218'
  }, {
    code: 'PHF',
    category: 'I',
    address: 'Newport News / Williamsburg International',
    city: 'Newport News',
    state: 'VA',
    zipCode: '23602'
  }, {
    code: 'PIT',
    category: 'I',
    address: '1000 Airport Boulevard',
    city: 'Pittsburgh',
    state: 'PA.',
    zipCode: '15231'
  }, {
    code: 'PVD',
    category: 'I',
    address: '2000 Post Road Ê',
    city: 'Warwick',
    state: 'RI',
    zipCode: '02886'
  }, {
    code: 'RDU',
    category: 'I',
    address: '1600 Terminal Blvd, RDU Airport',
    city: 'Raleigh',
    state: 'NC',
    zipCode: '27623'
  }, {
    code: 'RIC',
    category: 'I',
    address: '1 Richard E Byrd Terminal #200',
    city: 'Richmond',
    state: 'VA',
    zipCode: '23250'
  }, {
    code: 'RNO',
    category: 'I',
    address: '2001 E. Plumb Lane',
    city: 'Reno',
    state: 'NV',
    zipCode: '89502'
  }, {
    code: 'RSW',
    category: 'I',
    address: '11000 Terminal Access Road, Suite 8635',
    city: 'Ft. Myers',
    state: 'FL',
    zipCode: '33913'
  }, {
    code: 'SAN',
    category: 'I',
    address: '3665 North Harbor Drive',
    city: 'San Diego',
    state: 'CA',
    zipCode: '92101'
  }, {
    code: 'SDF',
    category: 'I',
    address: '600 Terminal Dr, Room 203',
    city: 'Louisville',
    state: 'KY',
    zipCode: '40209'
  }, {
    code: 'SJC',
    category: 'I',
    address: '1661 Airport Blvd',
    city: 'San Jose',
    state: 'CA',
    zipCode: '95110'
  }, {
    code: 'SLC',
    category: 'I',
    address: '776 North Terminal Drive',
    city: 'Salt Lake City',
    state: 'UT',
    zipCode: '84116'
  }, {
    code: 'SMF',
    category: 'I',
    address: '2446 Del Paso Rd, Suite #100',
    city: 'Sacramento',
    state: 'CA',
    zipCode: '95834'
  }, {
    code: 'SNA',
    category: 'I',
    address: '18881 Von Karman Ave Suite 1800',
    city: 'Irvine',
    state: 'CA',
    zipCode: '92612'
  }, {
    code: 'TPA',
    category: 'I',
    address: '5503 West Spruce Street',
    city: 'Tampa',
    state: 'FL',
    zipCode: '33607'
  }, {
    code: 'TUL',
    category: 'I',
    address: '7777 East Apache St., Room B-220',
    city: 'Tulsa',
    state: 'OK',
    zipCode: '74115'
  }, {
    code: 'TUS',
    category: 'I',
    address: '6550 S.Bay Colony dr., Suite 120',
    city: 'Tuscon',
    state: 'AZ',
    zipCode: '85706'
  }, {
    code: 'ABE',
    category: 'II',
    address: '3311 Airport Road Ste 303',
    city: 'Allentown',
    state: 'PA',
    zipCode: '18109'
  }, {
    code: 'ACY',
    category: 'II',
    address: '101 Atlantic City Intl Airport',
    city: 'Egg Harbor',
    state: 'NJ',
    zipCode: '08234'
  }, {
    code: 'AMA',
    category: 'II',
    address: '10801 Airport Blvd',
    city: 'Amarillo',
    state: 'TX',
    zipCode: '79111'
  }, {
    code: 'ATW',
    category: 'II',
    address: 'W6390 Challenger Drive, Suite 235',
    city: 'Appleton',
    state: 'WI',
    zipCode: '54914'
  }, {
    code: 'AVL',
    category: 'II',
    address: '708-8 Airport Road',
    city: 'Fletcher',
    state: 'NC',
    zipCode: '28732'
  }, {
    code: 'BGR',
    category: 'II',
    address: '287 Godfrey Boulevard',
    city: 'Bangor',
    state: 'ME',
    zipCode: '04401'
  }, {
    code: 'BIL',
    category: 'II',
    address: '1901 Terminal Circle',
    city: 'Billings',
    state: 'MT',
    zipCode: '59105'
  }, {
    code: 'BTR',
    category: 'II',
    address: '9430 Jackie Cochran Drive',
    city: 'Baton Rouge',
    state: 'LA',
    zipCode: '70807'
  }, {
    code: 'BTV',
    category: 'II',
    address: '1200 Airport Drive',
    city: 'South Burlington',
    state: 'VT',
    zipCode: '05403'
  }, {
    code: 'BZN',
    category: 'II',
    address: '850 Gallatin Field Road',
    city: 'Belgrade',
    state: 'MT',
    zipCode: '59714'
  }, {
    code: 'CAE',
    category: 'II',
    address: '3000 Aviation Way',
    city: 'West Columbia',
    state: 'SC',
    zipCode: '29170'
  }, {
    code: 'CAK',
    category: 'II',
    address: '5400 Lauby Road',
    city: 'North Canton',
    state: 'OH',
    zipCode: '44720'
  }, {
    code: 'CHA',
    category: 'II',
    address: '1001 Airport Road, Suite 13',
    city: 'Chattanooga',
    state: 'TN',
    zipCode: '37421'
  }, {
    code: 'CHS',
    category: 'II',
    address: '5300 International Blvd., Suite C104',
    city: 'Charleston',
    state: 'SC',
    zipCode: '29418'
  }, {
    code: 'CID',
    category: 'II',
    address: '2121 Arthur Collins Pkwy S.W., Suite 8',
    city: 'Cedar Rapids',
    state: 'IA',
    zipCode: '52404'
  }, {
    code: 'CRP',
    category: 'II',
    address: '1000 International Boulevard',
    city: 'Corpus Christi',
    state: 'TX',
    zipCode: '78406'
  }, {
    code: 'CRW',
    category: 'II',
    address: '100 Airport Road',
    city: 'Charleston',
    state: 'WV',
    zipCode: '25311'
  }, {
    code: 'DAB',
    category: 'II',
    address: '700 Catalina Drive',
    city: 'Daytona Beach',
    state: 'FL',
    zipCode: '32114'
  }, {
    code: 'DSM',
    category: 'II',
    address: '5800 Fleur Drive',
    city: 'Des Moines',
    state: 'IA',
    zipCode: '50321'
  }, {
    code: 'EUG',
    category: 'II',
    address: '28801 Douglas Drive',
    city: 'Eugene',
    state: 'OR',
    zipCode: '97402'
  }, {
    code: 'EYW',
    category: 'II',
    address: '3491 South Roosevelt Blvd.',
    city: 'Key West',
    state: 'FL',
    zipCode: '33040'
  }, {
    code: 'FAI',
    category: 'II',
    address: '6450 Old Airport Way',
    city: 'Fairbanks',
    state: 'AK',
    zipCode: '99709'
  }, {
    code: 'FAR',
    category: 'II',
    address: '2801 32nd Avenue - Rm 257',
    city: 'Fargo',
    state: 'ND',
    zipCode: '58102'
  }, {
    code: 'FAT',
    category: 'II',
    address: '5175 E. Clinton Way',
    city: 'Fresno',
    state: 'CA',
    zipCode: '93727'
  }, {
    code: 'FNT',
    category: 'II',
    address: 'G-3425 Bristol Rd',
    city: 'Flint',
    state: 'MI',
    zipCode: '48507-3183'
  }, {
    code: 'FSD',
    category: 'II',
    address: '2801 N Jaycee Lane',
    city: 'Soiux Falls',
    state: 'SD',
    zipCode: '57104'
  }, {
    code: 'FWA',
    category: 'II',
    address: '3867 Ferguson Road, Room W104F',
    city: 'Ft. Wayne',
    state: 'IN',
    zipCode: '46809'
  }, {
    code: 'GPT',
    category: 'II',
    address: '14035 Airport Road',
    city: 'Gulfport',
    state: 'MS',
    zipCode: '39503'
  }, {
    code: 'GRB',
    category: 'II',
    address: '2077 Airport Drive Suite 3I',
    city: 'Green Bay',
    state: 'WI',
    zipCode: '54313'
  }, {
    code: 'GRR',
    category: 'II',
    address: '5500 44th Street, SE',
    city: 'Grand Rapids',
    state: 'MI',
    zipCode: '49512'
  }, {
    code: 'GSN',
    category: 'II',
    address: 'MH-II Building Suite 302 PO Box 500050',
    city: 'Saipan',
    state: 'MP',
    zipCode: '96950'
  }, {
    code: 'GSP',
    category: 'II',
    address: '2000 GSP Drive',
    city: 'Greer',
    state: 'SC',
    zipCode: '29651'
  }, {
    code: 'HPN',
    category: 'II',
    address: '240 Airport Rd.',
    city: 'White Plains',
    state: 'NY',
    zipCode: '10604'
  }, {
    code: 'HRL',
    category: 'II',
    address: '3002 Heritage Way',
    city: 'Harlingen',
    state: 'TX',
    zipCode: '78550'
  }, {
    code: 'HSV',
    category: 'II',
    address: '1000 Glenn Hearn Blvd',
    city: 'Huntsville',
    state: 'AL',
    zipCode: '35824'
  }, {
    code: 'ICT',
    category: 'II',
    address: '2299 Airport Road',
    city: 'Witchta',
    state: 'KS',
    zipCode: '67209'
  }, {
    code: 'ILM',
    category: 'II',
    address: '1740 Airport Blvd',
    city: 'Wilmington',
    state: 'NC',
    zipCode: '28405'
  }, {
    code: 'ISP',
    category: 'II',
    address: '100 Arrivals Ave, Airline Terminal Building',
    city: 'Ronkonkoma',
    state: 'NY',
    zipCode: '11779'
  }, {
    code: 'ITO',
    category: 'II',
    address: 'Hilo International Airport',
    city: 'Hilo',
    state: 'HI',
    zipCode: '96720'
  }, {
    code: 'JAN',
    category: 'II',
    address: '100 International Dri., Suite 500',
    city: 'Pearl',
    state: 'MS',
    zipCode: '39208'
  }, {
    code: 'JNU',
    category: 'II',
    address: '1873 Shell Simmons Dr.',
    city: 'Juneau',
    state: 'AK',
    zipCode: '99801'
  }, {
    code: 'LAN',
    category: 'II',
    address: '4100 Capitol City Blvd',
    city: 'Lansing',
    state: 'MI',
    zipCode: '48906'
  }, {
    code: 'LBB',
    category: 'II',
    address: '5401 N Martin Luther King Blvd',
    city: 'Lubbock',
    state: 'TX',
    zipCode: '79403'
  }, {
    code: 'LEX',
    category: 'II',
    address: '4000 Terminal Drive, Suite 217',
    city: 'Lexington',
    state: 'KY',
    zipCode: '40510'
  }, {
    code: 'MAF',
    category: 'II',
    address: 'PO Box 61874',
    city: 'Midland',
    state: 'TX',
    zipCode: '79711'
  }, {
    code: 'MDT',
    category: 'II',
    address: 'One Terminal Drive',
    city: 'Middletown',
    state: 'PA',
    zipCode: '17057'
  }, {
    code: 'MLB',
    category: 'II',
    address: '830 Ed Foster Road',
    city: 'Melbourne',
    state: 'FL',
    zipCode: '32901'
  }, {
    code: 'MLI',
    category: 'II',
    address: '2200 69th Ave',
    city: 'Moline',
    state: 'IL',
    zipCode: '61265'
  }, {
    code: 'MOB',
    category: 'II',
    address: '8400 Airport Blvd., Room 2H',
    city: 'Mobile',
    state: 'AL',
    zipCode: '36608'
  }, {
    code: 'MSN',
    category: 'II',
    address: '4000 Int\'l Ln',
    city: 'Madison',
    state: 'WI',
    zipCode: '53704'
  }, {
    code: 'MSO',
    category: 'II',
    address: '5225 Highway 10 West',
    city: 'Missoula',
    state: 'MT',
    zipCode: '59808'
  }, {
    code: 'MYR',
    category: 'II',
    address: '1100 Jetport Road',
    city: 'Myrtle Beach',
    state: 'SC',
    zipCode: '29577'
  }, {
    code: 'PHF',
    category: 'II',
    address: '12350 Jefferson Ave, Suite 120',
    city: 'Newport News',
    state: 'VA',
    zipCode: '23602'
  }, {
    code: 'PIE',
    category: 'II',
    address: '14700 Terminal Boulevard',
    city: 'Clearwater',
    state: 'FL',
    zipCode: '33762'
  }, {
    code: 'PNS',
    category: 'II',
    address: '2430 Airport Blvd',
    city: 'Pensacola',
    state: 'FL',
    zipCode: '32504'
  }, {
    code: 'PSP',
    category: 'II',
    address: '3400 E. Tahquitz Canyon Way, Suite 10',
    city: 'Palm Springs',
    state: 'CA',
    zipCode: '92262'
  }, {
    code: 'PWM',
    category: 'II',
    address: '1001 Westbrook Street',
    city: 'Portland',
    state: 'ME',
    zipCode: '04102'
  }, {
    code: 'ROA',
    category: 'II',
    address: '5202 Aviation Dr NW',
    city: 'Roanoke',
    state: 'VA',
    zipCode: '24012'
  }, {
    code: 'ROC',
    category: 'II',
    address: '1200 Brooks Avenue',
    city: 'Rochester',
    state: 'NY',
    zipCode: '14624'
  }, {
    code: 'SAT',
    category: 'II',
    address: '9800 Airport Blvd STE 494',
    city: 'San Antonio',
    state: 'TX',
    zipCode: '78216'
  }, {
    code: 'SAV',
    category: 'II',
    address: '468 Airport Road',
    city: 'Savannah',
    state: 'GA',
    zipCode: '31408'
  }, {
    code: 'SBA',
    category: 'II',
    address: '500 James Fowler Rd',
    city: 'Goleta',
    state: 'CA',
    zipCode: '93117'
  }, {
    code: 'SBN',
    category: 'II',
    address: '4642 Progress Drive',
    city: 'South Bend',
    state: 'IN',
    zipCode: '46628'
  }, {
    code: 'SFB',
    category: 'II',
    address: '3181 Red Cleveland Blvd',
    city: 'Sanford',
    state: 'FL',
    zipCode: '32773'
  }, {
    code: 'SGF',
    category: 'II',
    address: '',
    city: 'Springfield',
    state: 'MO',
    zipCode: ''
  }, {
    code: 'SHV',
    category: 'II',
    address: '5103 Hollywood Ave., Suite 200',
    city: 'Shreveport',
    state: 'LA',
    zipCode: '71109'
  }, {
    code: 'SRQ',
    category: 'II',
    address: '6000 Airport Road',
    city: 'Sarasota',
    state: 'FL',
    zipCode: '34243'
  }, {
    code: 'STT',
    category: 'II',
    address: '8100 Lindberg Bay 2nd Floor',
    city: 'St. Thomas',
    state: 'VI',
    zipCode: '00802'
  }, {
    code: 'SYR',
    category: 'II',
    address: '152 Air Cargo Road',
    city: 'North Syracuse',
    state: 'NY',
    zipCode: '13212'
  }, {
    code: 'TLH',
    category: 'II',
    address: '3300 Capital Circle, SW Suite 33',
    city: 'Tallahassee',
    state: 'FL',
    zipCode: '32310-8742'
  }, {
    code: 'TOL',
    category: 'II',
    address: '11013 Airport Highway',
    city: 'Swanton',
    state: 'OH',
    zipCode: '43558'
  }, {
    code: 'TYS',
    category: 'II',
    address: '2055 Airport Highway',
    city: 'Alcoa',
    state: 'TN',
    zipCode: '37701'
  }, {
    code: 'VPS',
    category: 'II',
    address: '1701 State Road 85N',
    city: 'Eglin Air Force Base',
    state: 'FL',
    zipCode: '32542'
  }, {
    code: 'XNA',
    category: 'II',
    address: '1 Airport Boulevard, Suite 116',
    city: 'Bentonville',
    state: 'AR',
    zipCode: '72712'
  }, {
    code: 'ABI',
    category: 'III',
    address: '2933 Airport Blvd',
    city: 'Abilene',
    state: 'TX',
    zipCode: '79602'
  }, {
    code: 'ACK',
    category: 'III',
    address: '30 Macys Ln # 1',
    city: 'Nantucket',
    state: 'MA',
    zipCode: '02554'
  }, {
    code: 'ACT',
    category: 'III',
    address: '7909 Karl May Dr',
    city: 'Waco',
    state: 'TX',
    zipCode: '76708'
  }, {
    code: 'ACV',
    category: 'III',
    address: '3561 Boeing Ave #10',
    city: 'McKinleyville',
    state: 'CA',
    zipCode: '95519'
  }, {
    code: 'ADQ',
    category: 'III',
    address: '1647 Airport Way',
    city: 'Kodiak',
    state: 'AK',
    zipCode: '99615'
  }, {
    code: 'AGS',
    category: 'III',
    address: '1501 Aviation Way',
    city: 'Augusta',
    state: 'GA',
    zipCode: '30906'
  }, {
    code: 'AKN',
    category: 'III',
    address: '#1 Main Street',
    city: 'King Salmon',
    state: 'AK',
    zipCode: '99613'
  }, {
    code: 'ASE',
    category: 'III',
    address: '0233 E Airport Rd.',
    city: 'Aspen',
    state: 'CO',
    zipCode: '81611'
  }, {
    code: 'AVP',
    category: 'III',
    address: '100 Terminal Road',
    city: 'Avoca',
    state: 'PA',
    zipCode: '18641'
  }, {
    code: 'AZO',
    category: 'III',
    address: '5235 Portage Road',
    city: 'Kalamazoo',
    state: 'MI',
    zipCode: '49002'
  }, {
    code: 'BED',
    category: 'III',
    address: '200 Hanscom Drive',
    city: 'Bedford',
    state: 'MA',
    zipCode: '01730'
  }, {
    code: 'BET',
    category: 'III',
    address: '3521 State Highway',
    city: 'Bethel',
    state: 'AK',
    zipCode: '99599'
  }, {
    code: 'BFL',
    category: 'III',
    address: '3701 Wings Way, Suite 234',
    city: 'Bakersfield',
    state: 'CA',
    zipCode: '93308'
  }, {
    code: 'BGM',
    category: 'III',
    address: '2534 Airport Road',
    city: 'Johnson City',
    state: 'NY',
    zipCode: '13790'
  }, {
    code: 'BIS',
    category: 'III',
    address: '2301 University Drive',
    city: 'Bismarck',
    state: 'ND',
    zipCode: '58504'
  }, {
    code: 'BKW',
    category: 'III',
    address: '176 Airport Circle',
    city: 'Beaver',
    state: 'WV',
    zipCode: '25813'
  }, {
    code: 'BLI',
    category: 'III',
    address: '4255 Mitchell Way, Suite 206',
    city: 'Bellingham',
    state: 'WA',
    zipCode: '98226'
  }, {
    code: 'BLV',
    category: 'III',
    address: '9768 Airport Blvd',
    city: 'Mascoutah',
    state: 'IL',
    zipCode: '85635'
  }, {
    code: 'BMI',
    category: 'III',
    address: '3201 Cira Drive',
    city: 'Bloomington',
    state: 'IL',
    zipCode: '61704'
  }, {
    code: 'BQN',
    category: 'III',
    address: 'Rafael Hernendez Airport',
    city: 'Aguadilla',
    state: 'PR',
    zipCode: '00604'
  }, {
    code: 'BRO',
    category: 'III',
    address: '700 South Minnesota Ave',
    city: 'Brownsville',
    state: 'TX',
    zipCode: '78521'
  }, {
    code: 'BRW',
    category: 'III',
    address: '1078 Kodiak 3rd Floor',
    city: 'Barrow',
    state: 'AK',
    zipCode: '99723'
  }, {
    code: 'BTM',
    category: 'III',
    address: '111 Airport Road',
    city: 'Butte',
    state: 'MT',
    zipCode: '59701'
  }, {
    code: 'CDV',
    category: 'III',
    address: 'Mile 13, Copper River Highway',
    city: 'Cordova',
    state: 'AK',
    zipCode: '99574'
  }, {
    code: 'CEF',
    category: 'III',
    address: '255 Padgette Street',
    city: 'Chicopee',
    state: 'MA',
    zipCode: '01022'
  }, {
    code: 'CHO',
    category: 'III',
    address: '100 Bowen Loop',
    city: 'Charlottesville',
    state: 'VA',
    zipCode: '22911'
  }, {
    code: 'CMI',
    category: 'III',
    address: '11 Airport Road',
    city: 'Savoy',
    state: 'IL',
    zipCode: '61874'
  }, {
    code: 'CSG',
    category: 'III',
    address: '3250 W. Britt David Road',
    city: 'Columbus',
    state: 'GA',
    zipCode: '31909'
  }, {
    code: 'CWA',
    category: 'III',
    address: '100 CWA Drive, Suite 107',
    city: 'Mosinee',
    state: 'WI',
    zipCode: '54455'
  }, {
    code: 'DLG',
    category: 'III',
    address: '803 Airport Rd',
    city: 'Dillingham',
    state: 'AK',
    zipCode: '99576'
  }, {
    code: 'DLH',
    category: 'III',
    address: '4701 Grinden Drive',
    city: 'Duluth',
    state: 'MN',
    zipCode: '55811'
  }, {
    code: 'DRO',
    category: 'III',
    address: '1000 Airport Rd Box 8',
    city: 'Durango',
    state: 'CO',
    zipCode: '81301'
  }, {
    code: 'EGE',
    category: 'III',
    address: '217 Eldon Wilson Rd.',
    city: 'Gypsum',
    state: 'CO',
    zipCode: '81637'
  }, {
    code: 'EKO',
    category: 'III',
    address: '975 Terminal Way',
    city: 'Elko',
    state: 'NV',
    zipCode: '89801'
  }, {
    code: 'ELM',
    category: 'III',
    address: '276 Sing Sing Road',
    city: 'Horseheads',
    state: 'NY',
    zipCode: '14845'
  }, {
    code: 'ENV',
    category: 'III',
    address: '191 Airport Apron',
    city: 'Wendover',
    state: 'UT',
    zipCode: '84083'
  }, {
    code: 'ERI',
    category: 'III',
    address: '4411 West 12th St, Suite 15',
    city: 'Erie',
    state: 'PA',
    zipCode: '16505'
  }, {
    code: 'EVV',
    category: 'III',
    address: '7801 Bussing Drive',
    city: 'Evansville',
    state: 'IN',
    zipCode: '47725'
  }, {
    code: 'EWN',
    category: 'III',
    address: '200 Terminal Drive',
    city: 'New Bern',
    state: 'NC',
    zipCode: '28564'
  }, {
    code: 'FAY',
    category: 'III',
    address: '400 Airport Road',
    city: 'Fayetteville',
    state: 'NC',
    zipCode: '28306'
  }, {
    code: 'FMS',
    category: 'III',
    address: '6700 McKennon Blvd, Ste 104',
    city: 'Ft Smith',
    state: 'AR',
    zipCode: '72903'
  }, {
    code: 'GFK',
    category: 'III',
    address: '2787 Airport Drive',
    city: 'Grand Forks',
    state: 'ND',
    zipCode: '58203'
  }, {
    code: 'GGG',
    category: 'III',
    address: '269 Terminal Circle',
    city: 'Longview',
    state: 'TX',
    zipCode: '75603'
  }, {
    code: 'GJT',
    category: 'III',
    address: '2828 Walker Field Drive, Suite 209',
    city: 'Grand Junction',
    state: 'CO',
    zipCode: '81506'
  }, {
    code: 'GNV',
    category: 'III',
    address: '3880 N.E. 39th Avenue, Suite A',
    city: 'Gainesville',
    state: 'FL',
    zipCode: '32609'
  }, {
    code: 'GPI',
    category: 'III',
    address: '4170 Highway 2 East',
    city: 'Kalispell',
    state: 'MT',
    zipCode: '59901'
  }, {
    code: 'GRK',
    category: 'III',
    address: '8101 Clear Creek Rd. Ste D-113',
    city: 'Killeen',
    state: 'TX',
    zipCode: '76549'
  }, {
    code: 'GST',
    category: 'III',
    address: '1 Airport Rd.',
    city: 'Gustavus',
    state: 'AK',
    zipCode: '99826'
  }, {
    code: 'GTF',
    category: 'III',
    address: '2800 Terminal Drive',
    city: 'Great Falls',
    state: 'MT',
    zipCode: '59404'
  }, {
    code: 'GUC',
    category: 'III',
    address: '711 Rio Grande',
    city: 'Gunnison',
    state: 'CO',
    zipCode: '81230'
  }, {
    code: 'GYY',
    category: 'III',
    address: '6001 W Industrial Highway',
    city: 'Gary',
    state: 'IN',
    zipCode: '46406'
  }, {
    code: 'HDN',
    category: 'III',
    address: '11005 Routt Cnty Rd 51A',
    city: 'Hayden',
    state: 'CO',
    zipCode: '81639'
  }, {
    code: 'HLN',
    category: 'III',
    address: '2850 Skyway Drive',
    city: 'Helena',
    state: 'MT',
    zipCode: '59602'
  }, {
    code: 'HTS',
    category: 'III',
    address: '1449 Airport Rd',
    city: 'Huntington',
    state: 'WV',
    zipCode: '25704'
  }, {
    code: 'HYA',
    category: 'III',
    address: '480 Barnstable Rd # 2',
    city: 'Hyannis',
    state: 'MA',
    zipCode: '02601'
  }, {
    code: 'IAG',
    category: 'III',
    address: 'Niagara Falls Blvd. at Porter Rd.',
    city: 'Niagara Falls',
    state: 'NY',
    zipCode: '14304'
  }, {
    code: 'IDA',
    category: 'III',
    address: '2140 N. Skyline Drive',
    city: 'Idaho Falls',
    state: 'ID',
    zipCode: '83402'
  }, {
    code: 'IFP',
    category: 'III',
    address: '2850 Laughlin View Dr., Box 11',
    city: 'Bullhead City',
    state: 'AZ',
    zipCode: '86429'
  }, {
    code: 'ISO',
    category: 'III',
    address: '2780 Jetport Road',
    city: 'Kinston',
    state: 'NC',
    zipCode: '28504'
  }, {
    code: 'IWA',
    category: 'III',
    address: '6033 S. Sossaman Rd.',
    city: 'Mesa',
    state: 'AZ',
    zipCode: '85212'
  }, {
    code: 'JAC',
    category: 'III',
    address: '8500 Airport Parkway, Room 217',
    city: 'Jackson',
    state: 'WY',
    zipCode: '82604'
  }, {
    code: 'KTN',
    category: 'III',
    address: '1 Airport Terminal',
    city: 'Ketchikan',
    state: 'AK',
    zipCode: '99901'
  }, {
    code: 'LCK',
    category: 'III',
    address: '7161 Second St',
    city: 'Columbus',
    state: 'OH',
    zipCode: '43217'
  }, {
    code: 'LFT',
    category: 'III',
    address: '200 Terminal Drive',
    city: 'Lafayette',
    state: 'LA',
    zipCode: '70508'
  }, {
    code: 'LNK',
    category: 'III',
    address: '3101 NW 12th Street',
    city: 'Lincoln',
    state: 'NE',
    zipCode: '64524'
  }, {
    code: 'LRD',
    category: 'III',
    address: '5210 Bob Bullock Loop',
    city: 'Laredo',
    state: 'TX',
    zipCode: '78041'
  }, {
    code: 'LSE',
    category: 'III',
    address: '2850 Airport Drive',
    city: 'La Crosse',
    state: 'WI',
    zipCode: '54603'
  }, {
    code: 'LWB',
    category: 'III',
    address: 'Route 219 N',
    city: 'Lewisburg',
    state: 'WV',
    zipCode: '24901'
  }, {
    code: 'LWS',
    category: 'III',
    address: '406 Burrell Avenue, Suite 301',
    city: 'Lewiston',
    state: 'ID',
    zipCode: '83501'
  }, {
    code: 'MBS',
    category: 'III',
    address: '8500 Garfield Rd., Suite 101',
    city: 'Freeland',
    state: 'MI',
    zipCode: '48623'
  }, {
    code: 'MCN',
    category: 'III',
    address: '1000 Terminal Drive',
    city: 'Macon',
    state: 'GA',
    zipCode: '31297'
  }, {
    code: 'MFE',
    category: 'III',
    address: '2500 South Bicentennial Blvd, Suite 104',
    city: 'McAllen',
    state: 'TX',
    zipCode: '78503'
  }, {
    code: 'MFR',
    category: 'III',
    address: '3650 Biddle Road',
    city: 'Medford',
    state: 'OR',
    zipCode: '97504'
  }, {
    code: 'MGM',
    category: 'III',
    address: '4445 Selma Hwy',
    city: 'Montgomery',
    state: 'AL',
    zipCode: '36108'
  }, {
    code: 'MOT',
    category: 'III',
    address: '25 Airport Road',
    city: 'Minot',
    state: 'ND',
    zipCode: '58701'
  }, {
    code: 'MRY',
    category: 'III',
    address: '110 Fred Kane Drive Monterey',
    city: 'Monterey',
    state: 'CA',
    zipCode: '93940'
  }, {
    code: 'MTJ',
    category: 'III',
    address: '2100 Airport Rd',
    city: 'Montrose',
    state: 'CO',
    zipCode: '81401'
  }, {
    code: 'MVY',
    category: 'III',
    address: '71 Airport Rd',
    city: 'Vineyard Haven',
    state: 'MA',
    zipCode: '02568'
  }, {
    code: 'OAJ',
    category: 'III',
    address: '264 A.J. Ellis Airport Road',
    city: 'Richlands',
    state: 'NC',
    zipCode: '28574'
  }, {
    code: 'OME',
    category: 'III',
    address: 'Seppala Drive & Airport Rd',
    city: 'Nome',
    state: 'AK',
    zipCode: '99762'
  }, {
    code: 'OTZ',
    category: 'III',
    address: '1 Airport Way',
    city: 'Kotzebue',
    state: 'AK',
    zipCode: '99752'
  }, {
    code: 'PGD',
    category: 'III',
    address: '28000 A-1 Airport Road',
    city: 'Punta Gorda',
    state: 'FL',
    zipCode: '33982'
  }, {
    code: 'PIA',
    category: 'III',
    address: '6100 W. Everett Dirksen Pkwy, #301',
    city: 'Peoria',
    state: 'IL',
    zipCode: '61607'
  }, {
    code: 'PIH',
    category: 'III',
    address: '1950 S. Airport Way',
    city: 'Pocatello',
    state: 'ID',
    zipCode: '83204'
  }, {
    code: 'PPG',
    category: 'III',
    address: '1 Airport Road',
    city: 'Pago Pago',
    state: 'AS',
    zipCode: '96799'
  }, {
    code: 'PSC',
    category: 'III',
    address: '3601 North 20th Avenue',
    city: 'Pasco',
    state: 'WA',
    zipCode: '99301'
  }, {
    code: 'PSE',
    category: 'III',
    address: 'P.O. Box 323',
    city: 'Ponce',
    state: 'PR',
    zipCode: '00715'
  }, {
    code: 'PSG',
    category: 'III',
    address: '1500 Haugen Drive',
    city: 'Petersburg',
    state: 'AK',
    zipCode: '99833'
  }, {
    code: 'PSM',
    category: 'III',
    address: '36 Airline Ave, Newington, NH 03801',
    city: 'Portsmouth',
    state: 'NH',
    zipCode: '03801'
  }, {
    code: 'PUW',
    category: 'III',
    address: '3200 Airport Complex North',
    city: 'Pullman',
    state: 'WA',
    zipCode: '99163'
  }, {
    code: 'RAP',
    category: 'III',
    address: '4550 Terminal Road',
    city: 'Rapid City',
    state: 'SD',
    zipCode: '57703'
  }, {
    code: 'RDD',
    category: 'III',
    address: '6751 Woodrum Circle #140',
    city: 'Redding',
    state: 'CA',
    zipCode: '96002'
  }, {
    code: 'RDG',
    category: 'III',
    address: '2501 Bernville Road',
    city: 'Reading',
    state: 'PA',
    zipCode: '19605'
  }, {
    code: 'RDM',
    category: 'III',
    address: '2522 SE Jesse Butler Circle',
    city: 'Redmond',
    state: 'OR',
    zipCode: '97756'
  }, {
    code: 'RFD',
    category: 'III',
    address: '60 Airport Drive',
    city: 'Rockford',
    state: 'IL',
    zipCode: '85212'
  }, {
    code: 'RST',
    category: 'III',
    address: '7600 Helgerson Dr. SW',
    city: 'Rochester',
    state: 'MN',
    zipCode: '55902'
  }, {
    code: 'SCC',
    category: 'III',
    address: '419 James Dalton Hwy',
    city: 'Prudhoe Bay',
    state: 'AK',
    zipCode: '99734'
  }, {
    code: 'SCK',
    category: 'III',
    address: '5000 S Airport Way Stockton',
    city: 'Stockton',
    state: 'CA',
    zipCode: '95026'
  }, {
    code: 'SGJ',
    category: 'III',
    address: '4796 US 1 North',
    city: 'St Augustine',
    state: 'FL',
    zipCode: '32095'
  }, {
    code: 'SIT',
    category: 'III',
    address: '605 Airport Rd',
    city: 'Sitka',
    state: 'AK',
    zipCode: '99835'
  }, {
    code: 'SMX',
    category: 'III',
    address: '3249 Terminal drive Suite 201',
    city: 'Santa Maria',
    state: 'CA',
    zipCode: '93455'
  }, {
    code: 'SPI',
    category: 'III',
    address: '2005 Capital Airport Drive, Suite 272',
    city: 'Springfield',
    state: 'IL',
    zipCode: '62707'
  }, {
    code: 'SPS',
    category: 'III',
    address: '4000 Armstrong',
    city: 'Wichita Falls',
    state: 'TX',
    zipCode: '76305'
  }, {
    code: 'STS',
    category: 'III',
    address: '2290 Airport Blvd',
    city: 'Sonoma',
    state: 'CA',
    zipCode: '95403'
  }, {
    code: 'STX',
    category: 'III',
    address: 'Henry E. Rohlsen 9918 RR2',
    city: 'Christiansted',
    state: 'VI',
    zipCode: '00850'
  }, {
    code: 'SUN',
    category: 'III',
    address: '1610 Airport Way',
    city: 'Hailey',
    state: 'ID',
    zipCode: '83333'
  }, {
    code: 'SUX',
    category: 'III',
    address: '2403 Aviation Ave',
    city: 'Sioux City',
    state: 'IA',
    zipCode: '51111'
  }, {
    code: 'SWF',
    category: 'III',
    address: '1130 First Street',
    city: 'New  Windsor',
    state: 'NY',
    zipCode: '12553'
  }, {
    code: 'TRI',
    category: 'III',
    address: '2525 Highway 75, Room 123',
    city: 'Blountville',
    state: 'TN',
    zipCode: '37617'
  }, {
    code: 'TVC',
    category: 'III',
    address: '727 Fly Don\'t Dr.',
    city: 'Traverse City',
    state: 'MI',
    zipCode: '49686'
  }, {
    code: 'TXK',
    category: 'III',
    address: '303 Airport Rd',
    city: 'Texarkana',
    state: 'AR',
    zipCode: '71854'
  }, {
    code: 'UCA',
    category: 'III',
    address: '5900 Airport Road',
    city: 'Oriskany',
    state: 'NY',
    zipCode: '13424'
  }, {
    code: 'UTA',
    category: 'III',
    address: '209 South Airport Boulevard',
    city: 'Tunica',
    state: 'MS',
    zipCode: '38676'
  }, {
    code: 'VLD',
    category: 'III',
    address: '1750 Airport Road Suite 6',
    city: 'Valdosta',
    state: 'GA',
    zipCode: '31601'
  }, {
    code: 'WRG',
    category: 'III',
    address: '1 Airport Loop Rd',
    city: 'Wrangell',
    state: 'AK',
    zipCode: '99929'
  }, {
    code: 'YAK',
    category: 'III',
    address: '997 Airport Rd',
    city: 'Yakutat',
    state: 'AK',
    zipCode: '99689'
  }, {
    code: 'YNG',
    category: 'III',
    address: '1453 Youngstown-Kingsville Rd NE',
    city: 'Vienna',
    state: 'OH',
    zipCode: '44473'
  }, {
    code: 'ABR',
    category: 'IV',
    address: '4740 6th Ave, SE',
    city: 'Aberdeen',
    state: 'SD',
    zipCode: '57401'
  }, {
    code: 'ABY',
    category: 'IV',
    address: '3905 Newton Road  Suite 100',
    city: 'Albany',
    state: 'GA',
    zipCode: '31701'
  }, {
    code: 'ADK',
    category: 'IV',
    address: '100 Airport Road',
    city: 'Adak',
    state: 'AK',
    zipCode: '99546'
  }, {
    code: 'AEX',
    category: 'IV',
    address: '1303 Billy Mitchell Blvd',
    city: 'Alexandria',
    state: 'LA',
    zipCode: '71303'
  }, {
    code: 'AHN',
    category: 'IV',
    address: '1035 Ben Epps Drive',
    city: 'Athens',
    state: 'GA',
    zipCode: '30605'
  }, {
    code: 'AIA',
    category: 'IV',
    address: '5631 Sarpy Drive',
    city: 'Alliance',
    state: 'NE',
    zipCode: '69301'
  }, {
    code: 'ALO',
    category: 'IV',
    address: '2790 Livingston Ln',
    city: 'Waterloo',
    state: 'IA',
    zipCode: '50703'
  }, {
    code: 'ALS',
    category: 'IV',
    address: '2490 State Ave.',
    city: 'Alamosa',
    state: 'CO',
    zipCode: '81101'
  }, {
    code: 'ALW',
    category: 'IV',
    address: '45 Terminal Rd, Suite 10',
    city: 'Walla Walla',
    state: 'WA',
    zipCode: '99362'
  }, {
    code: 'AOO',
    category: 'IV',
    address: '2 Airport Drive',
    city: 'Martinsburg',
    state: 'PA',
    zipCode: '16662'
  }, {
    code: 'APF',
    category: 'IV',
    address: 'TSA Trailer, 160 Aviation Drive North',
    city: 'Naples',
    state: 'FL',
    zipCode: '34104'
  }, {
    code: 'APN',
    category: 'IV',
    address: '1617 Airport Road',
    city: 'Alpena',
    state: 'MI',
    zipCode: '90808'
  }, {
    code: 'ART',
    category: 'IV',
    address: 'Airport Drive',
    city: 'Dexter',
    state: 'NY',
    zipCode: '13634'
  }, {
    code: 'ARY',
    category: 'IV',
    address: '2416 Boeing Drive',
    city: 'Watertown',
    state: 'SD',
    zipCode: '57201'
  }, {
    code: 'AUG',
    category: 'IV',
    address: '75 Airport Road',
    city: 'Augusta',
    state: 'ME',
    zipCode: '04330'
  }, {
    code: 'BFD',
    category: 'IV',
    address: '212 Airport Drive, Suite E',
    city: 'Lewis Run',
    state: 'PA',
    zipCode: '16738'
  }, {
    code: 'BFF',
    category: 'IV',
    address: '2500025 Airport Terminal  St',
    city: 'Scottsbluff,',
    state: 'NE',
    zipCode: '69361'
  }, {
    code: 'BHB',
    category: 'IV',
    address: '143 Caruso Drive, Suite 1',
    city: 'Ellsworth',
    state: 'ME',
    zipCode: '04605'
  }, {
    code: 'BJI',
    category: 'IV',
    address: '3824 Moberg Rd. NW, Suite 301',
    city: 'Bemidji',
    state: 'MN',
    zipCode: '56601'
  }, {
    code: 'BKX',
    category: 'IV',
    address: '149 Airport Avenue',
    city: 'Brookings',
    state: 'SD',
    zipCode: '57006'
  }, {
    code: 'BPT',
    category: 'IV',
    address: '6000 Airline Dr. - TSA',
    city: 'Beaumont',
    state: 'TX',
    zipCode: '77705'
  }, {
    code: 'BQK',
    category: 'IV',
    address: '295 Aviation Parkway Suite 158',
    city: 'Brunswick',
    state: 'GA',
    zipCode: '31525'
  }, {
    code: 'BRD',
    category: 'IV',
    address: '16384 Airport Road Suite #10',
    city: 'Brainerd',
    state: 'MN',
    zipCode: '56401'
  }, {
    code: 'BRL',
    category: 'IV',
    address: '2515 Summer street',
    city: 'Burlington',
    state: 'IA',
    zipCode: '52610-3330'
  }, {
    code: 'CDC',
    category: 'IV',
    address: '2560 West Aviation Way, Suite 3',
    city: 'Cedar City',
    state: 'UT',
    zipCode: '84720'
  }, {
    code: 'CDR',
    category: 'IV',
    address: '90 Airport Road',
    city: 'Chadron',
    state: 'NE',
    zipCode: '69337'
  }, {
    code: 'CEC',
    category: 'IV',
    address: '202 Dale Rupert Road',
    city: 'Crescent City',
    state: 'CA',
    zipCode: '95531'
  }, {
    code: 'CEZ',
    category: 'IV',
    address: '22874 County Rd F P.O.Box 36',
    city: 'Cortez',
    state: 'CO',
    zipCode: '81321'
  }, {
    code: 'CGI',
    category: 'IV',
    address: '3900 Nash Road, Scott City, MO',
    city: 'Cape Girardeau',
    state: 'MO',
    zipCode: '63780'
  }, {
    code: 'CIC',
    category: 'IV',
    address: '150 Airpark Blvd',
    city: 'Chico',
    state: 'CA',
    zipCode: '95973'
  }, {
    code: 'CIU',
    category: 'IV',
    address: '5315 West Airport Drive',
    city: 'Kincheloe',
    state: 'MI',
    zipCode: '32827'
  }, {
    code: 'CKB',
    category: 'IV',
    address: '2000 Aviation Way',
    city: 'Bridgeport',
    state: 'WV',
    zipCode: '26330'
  }, {
    code: 'CLL',
    category: 'IV',
    address: '1 McKenzie Terminal Blvd. Ste 207',
    city: 'College Station',
    state: 'TX',
    zipCode: '77845'
  }, {
    code: 'CMX',
    category: 'IV',
    address: '23810 Airpark Blvd, Suite 113',
    city: 'Calumet',
    state: 'MI',
    zipCode: '60638'
  }, {
    code: 'CNY',
    category: 'IV',
    address: 'Canyonland Airfield North Highway 191',
    city: 'Moab',
    state: 'UT',
    zipCode: '84532'
  }, {
    code: 'COD',
    category: 'IV',
    address: '3001 Duggleby Drive, Mailing address 1108 14th Street #267',
    city: 'Cody',
    state: 'WY',
    zipCode: '82414-2748'
  }, {
    code: 'COU',
    category: 'IV',
    address: '11300 South Airport Drive',
    city: 'Columbia',
    state: 'MO',
    zipCode: '65201'
  }, {
    code: 'CPR',
    category: 'IV',
    address: '8500 Airport Parkway, Room 217',
    city: 'Casper',
    state: 'WY',
    zipCode: '82604'
  }, {
    code: 'CRQ',
    category: 'IV',
    address: '2198 Palomar Airport Road',
    city: 'Carlsbad',
    state: 'CA',
    zipCode: '92011'
  }, {
    code: 'CYS',
    category: 'IV',
    address: '300 E. 8th Ave.',
    city: 'Cheyenne',
    state: 'WY',
    zipCode: '82003'
  }, {
    code: 'DBQ',
    category: 'IV',
    address: '11000 Airport Road',
    city: 'Dubuque',
    state: 'IA',
    zipCode: '52003-9555'
  }, {
    code: 'DDC',
    category: 'IV',
    address: '100 Airport Road',
    city: 'Dodge City',
    state: 'KS',
    zipCode: '67801'
  }, {
    code: 'DEC',
    category: 'IV',
    address: '910 South Airport Road',
    city: 'Decatur',
    state: 'IL',
    zipCode: '62521'
  }, {
    code: 'DHN',
    category: 'IV',
    address: '800 Airport Drive, Suite 15',
    city: 'Dothan',
    state: 'AL',
    zipCode: '36303'
  }, {
    code: 'DIK',
    category: 'IV',
    address: '11120 42nd St SW',
    city: 'Dickinson',
    state: 'ND',
    zipCode: '58601'
  }, {
    code: 'DRT',
    category: 'IV',
    address: '1104 West 10th Street',
    city: 'Del Rio',
    state: 'TX',
    zipCode: '78840'
  }, {
    code: 'DUJ',
    category: 'IV',
    address: '5290 Airport Road',
    city: 'Falls Creek',
    state: 'PA',
    zipCode: '15851'
  }, {
    code: 'DVL',
    category: 'IV',
    address: 'Highway 19 W',
    city: 'Devils Lake',
    state: 'ND',
    zipCode: '58301'
  }, {
    code: 'EAR',
    category: 'IV',
    address: '5145 Airport Road',
    city: 'Kearney',
    state: 'NE',
    zipCode: '68847'
  }, {
    code: 'EAT',
    category: 'IV',
    address: 'One Pangborn Drive',
    city: 'East Wenatchee',
    state: 'WA',
    zipCode: '98802'
  }, {
    code: 'EAU',
    category: 'IV',
    address: '3800 Starr Avenue',
    city: 'Eau Claire',
    state: 'WI',
    zipCode: '54703'
  }, {
    code: 'ELD',
    category: 'IV',
    address: '418 Airport Drive',
    city: 'El Dorado',
    state: 'AR',
    zipCode: '71730'
  }, {
    code: 'ENA',
    category: 'IV',
    address: '305 N. Willow Street',
    city: 'Kenai',
    state: 'AK',
    zipCode: '99611'
  }, {
    code: 'ESC',
    category: 'IV',
    address: '3300 Airport Road',
    city: 'Escanaba',
    state: 'MI',
    zipCode: '58703'
  }, {
    code: 'FHU',
    category: 'IV',
    address: 'TSA has closed this location',
    city: 'Tucson',
    state: 'AZ',
    zipCode: '85706'
  }, {
    code: 'FKL',
    category: 'IV',
    address: '1560 Airport Road',
    city: 'Franklin',
    state: 'PA',
    zipCode: '16323'
  }, {
    code: 'FLG',
    category: 'IV',
    address: '6200 S. Pulliam Dr. Ste. #112',
    city: 'Flagstaff',
    state: 'AZ',
    zipCode: '86001'
  }, {
    code: 'FLO',
    category: 'IV',
    address: '2100 Terminal Drive',
    city: 'Florence',
    state: 'SC',
    zipCode: '29506'
  }, {
    code: 'FMN',
    category: 'IV',
    address: '1300 West Navajo',
    city: 'Farmington',
    state: 'NM',
    zipCode: '87106'
  }, {
    code: 'FNL',
    category: 'IV',
    address: '4900 Earhart Rd',
    city: 'Loveland',
    state: 'CO',
    zipCode: '80538'
  }, {
    code: 'FOD',
    category: 'IV',
    address: '1639 Nelson Avenue, Suite 6',
    city: 'Fort Dodge',
    state: 'IA',
    zipCode: '13662'
  }, {
    code: 'FOE',
    category: 'IV',
    address: 'Need TSA Airport Address',
    city: 'Topeka',
    state: 'KS',
    zipCode: ''
  }, {
    code: 'GBD',
    category: 'IV',
    address: '9046 6th Street',
    city: 'Great Bend',
    state: 'KS',
    zipCode: '67530'
  }, {
    code: 'GCC',
    category: 'IV',
    address: '2000 Airport Rd., Ste. 148',
    city: 'Gillette',
    state: 'WY',
    zipCode: '82716'
  }, {
    code: 'GCK',
    category: 'IV',
    address: '2225 S Air Service Drive # 112',
    city: 'Garden City',
    state: 'KS',
    zipCode: '67846'
  }, {
    code: 'GLH',
    category: 'IV',
    address: '166 5th Avenue, Suite 300',
    city: 'Greenville',
    state: 'MS',
    zipCode: '39703'
  }, {
    code: 'GON',
    category: 'IV',
    address: '155 Tower Avenue',
    city: 'Groton',
    state: 'CT',
    zipCode: '06340'
  }, {
    code: 'GRI',
    category: 'IV',
    address: '3751 N Sky Park Road, Suite1',
    city: 'Grand Island',
    state: 'NE',
    zipCode: '68801'
  }, {
    code: 'GRO',
    category: 'IV',
    address: 'MH-II Building Suite 302 PO Box 500050',
    city: 'Rota',
    state: 'MP',
    zipCode: '96950'
  }, {
    code: 'GTR',
    category: 'IV',
    address: '2080 Airport Road',
    city: 'Columbus',
    state: 'MS',
    zipCode: '39701'
  }, {
    code: 'GUP',
    category: 'IV',
    address: '2111West Highway 66',
    city: 'Gallup',
    state: 'MN',
    zipCode: '87305'
  }, {
    code: 'HIB',
    category: 'IV',
    address: '11038 Hwy. 37',
    city: 'Hibbing',
    state: 'MN',
    zipCode: '55746'
  }, {
    code: 'HOM',
    category: 'IV',
    address: '3720 FAA Road',
    city: 'Homer',
    state: 'AK',
    zipCode: '99603'
  }, {
    code: 'HON',
    category: 'IV',
    address: '1501 Colorado NW',
    city: 'Huron',
    state: 'SD',
    zipCode: '57350'
  }, {
    code: 'HOT',
    category: 'IV',
    address: '525 Airport Road Term Building',
    city: 'Hot Springs',
    state: 'AR',
    zipCode: '71913'
  }, {
    code: 'HRO',
    category: 'IV',
    address: '2524 Airport Road',
    city: 'Harrison',
    state: 'AR',
    zipCode: '72601'
  }, {
    code: 'HVN',
    category: 'IV',
    address: '155 Burr St',
    city: 'New Haven',
    state: 'CT',
    zipCode: '06512'
  }, {
    code: 'HXD',
    category: 'IV',
    address: '120 Beach City Road',
    city: 'Hilton Head',
    state: 'SC',
    zipCode: '29925'
  }, {
    code: 'HYS',
    category: 'IV',
    address: '3950 E 8th Street',
    city: 'Hays',
    state: 'KS',
    zipCode: '67601'
  }, {
    code: 'IGM',
    category: 'IV',
    address: '5900 Flightline Dr.',
    city: 'Kingman',
    state: 'AZ',
    zipCode: '86401'
  }, {
    code: 'ILG',
    category: 'IV',
    address: '151 North Dupont Highway',
    city: 'New Castle',
    state: 'DE',
    zipCode: '19720'
  }, {
    code: 'IMT',
    category: 'IV',
    address: '500 Airport Road',
    city: 'Kingsford',
    state: 'MI',
    zipCode: '42301'
  }, {
    code: 'INL',
    category: 'IV',
    address: '3214 2nd Ave. East',
    city: 'International Falls',
    state: 'MN',
    zipCode: '56649'
  }, {
    code: 'IPL',
    category: 'IV',
    address: 'Suite M, 1101 Airport Road',
    city: 'Imperial',
    state: 'CA',
    zipCode: '92251'
  }, {
    code: 'IPT',
    category: 'IV',
    address: '700 Airport Rd',
    city: 'Montoursville',
    state: 'PA',
    zipCode: '17754'
  }, {
    code: 'IRK',
    category: 'IV',
    address: '27171 Airport Trail',
    city: 'Kirksville',
    state: 'MO',
    zipCode: '63501'
  }, {
    code: 'ISN',
    category: 'IV',
    address: '601 Airport Road',
    city: 'Williston',
    state: 'ND',
    zipCode: '58802'
  }, {
    code: 'ITH',
    category: 'IV',
    address: '72 Brown Road',
    city: 'Ithaca',
    state: 'NY',
    zipCode: '14850'
  }, {
    code: 'IWD',
    category: 'IV',
    address: 'E5560 Airport Road',
    city: 'Ironwood',
    state: 'MI',
    zipCode: '27835'
  }, {
    code: 'IYK',
    category: 'IV',
    address: '6900 Monache Road',
    city: 'Inyokern',
    state: 'CA',
    zipCode: '93527'
  }, {
    code: 'JBR',
    category: 'IV',
    address: '4027 Lindberg Dr',
    city: 'Jonesboro',
    state: 'AR',
    zipCode: '72401'
  }, {
    code: 'JHM',
    category: 'IV',
    address: '4050 Honoapi\'ilani Hwy.',
    city: 'Lahaina',
    state: 'HI',
    zipCode: '96761'
  }, {
    code: 'JHW',
    category: 'IV',
    address: '3163 Airport Drive',
    city: 'Jamestown',
    state: 'NY',
    zipCode: '14701'
  }, {
    code: 'JLN',
    category: 'IV',
    address: '5501 N. Dennis Weaver Drive,',
    city: 'Joplin',
    state: 'MO',
    zipCode: '64801'
  }, {
    code: 'JMS',
    category: 'IV',
    address: '1600 Airport Road, PO Box 1560',
    city: 'Jamestown',
    state: 'ND',
    zipCode: '58402'
  }, {
    code: 'JST',
    category: 'IV',
    address: '479 Airport Road',
    city: 'Johnstown',
    state: 'PA',
    zipCode: '15904'
  }, {
    code: 'LAR',
    category: 'IV',
    address: '555 General Brees',
    city: 'Laramie',
    state: 'WY',
    zipCode: '82070'
  }, {
    code: 'LAW',
    category: 'IV',
    address: '3401 South 11th St.',
    city: 'Lawton',
    state: 'OK',
    zipCode: '73501'
  }, {
    code: 'LBE',
    category: 'IV',
    address: '148 Aviation Lane, Suite #302',
    city: 'Latrobe',
    state: 'PA',
    zipCode: '15650'
  }, {
    code: 'LBF',
    category: 'IV',
    address: '5400 E Lee Bird Dr, Suite 12',
    city: 'North Platte',
    state: 'NE',
    zipCode: '69101'
  }, {
    code: 'LBL',
    category: 'IV',
    address: '720 Terminal Road',
    city: 'Liberal',
    state: 'KS',
    zipCode: '67905'
  }, {
    code: 'LCH',
    category: 'IV',
    address: '500 Airport Blvd',
    city: 'Lake Charles',
    state: 'LA',
    zipCode: '70605'
  }, {
    code: 'LEB',
    category: 'IV',
    address: '5 Airpark Road',
    city: 'Lebanon',
    state: 'NH',
    zipCode: '03784'
  }, {
    code: 'LMT',
    category: 'IV',
    address: '3000 Airport Rd.',
    city: 'Klamath Falls',
    state: 'OR',
    zipCode: '97603'
  }, {
    code: 'LNS',
    category: 'IV',
    address: '500 Airport Road',
    city: 'Lititz',
    state: 'PA',
    zipCode: '17543'
  }, {
    code: 'LNY',
    category: 'IV',
    address: 'Kaumalapau Hwy., Airport Road',
    city: 'Lanai City',
    state: 'HI',
    zipCode: '96763'
  }, {
    code: 'LYH',
    category: 'IV',
    address: '4308 Wards Rd',
    city: 'Lynchburg',
    state: 'VA',
    zipCode: '24502'
  }, {
    code: 'MBL',
    category: 'IV',
    address: '2323 Airport Road',
    city: 'Manistee',
    state: 'MI',
    zipCode: '49660'
  }, {
    code: 'MCE',
    category: 'IV',
    address: '20 Macready Drive',
    city: 'Merced',
    state: 'CA',
    zipCode: '95340'
  }, {
    code: 'MCK',
    category: 'IV',
    address: '1200 Airport Road',
    city: 'McCook',
    state: 'NE',
    zipCode: '69001'
  }, {
    code: 'MCW',
    category: 'IV',
    address: 'Hwy 18W',
    city: 'Mason City',
    state: 'IA',
    zipCode: '50402'
  }, {
    code: 'MEI',
    category: 'IV',
    address: '2811 Highway 11S',
    city: 'Meridian',
    state: 'MS',
    zipCode: '39304'
  }, {
    code: 'MGW',
    category: 'IV',
    address: '100 Hart Field Rd',
    city: 'Morgantown',
    state: 'WV',
    zipCode: '26505'
  }, {
    code: 'MHK',
    category: 'IV',
    address: '5500 Fort Riley Boulevard',
    city: 'Manhattan',
    state: 'KS',
    zipCode: '66502'
  }, {
    code: 'MKG',
    category: 'IV',
    address: '99 Sinclair Drive',
    city: 'Muskegon',
    state: 'MI',
    zipCode: '49441'
  }, {
    code: 'MKK',
    category: 'IV',
    address: 'Hoolehua',
    city: 'Hoolehua',
    state: 'HI',
    zipCode: '96729'
  }, {
    code: 'MKL',
    category: 'IV',
    address: '308 Grady Montgomery',
    city: 'Jackson',
    state: 'TN',
    zipCode: '38301'
  }, {
    code: 'MLU',
    category: 'IV',
    address: '5400 Operations Road, TSA Bldg B',
    city: 'Monroe',
    state: 'LA',
    zipCode: '71203'
  }, {
    code: 'MOD',
    category: 'IV',
    address: '617 Airport Way',
    city: 'Modesto',
    state: 'CA',
    zipCode: '95354'
  }, {
    code: 'MSL',
    category: 'IV',
    address: '1729 TED Campbell Drive Suite A',
    city: 'Muscle Shoals',
    state: 'AL',
    zipCode: '35661'
  }, {
    code: 'MSS',
    category: 'IV',
    address: '200 Administration Road',
    city: 'Massena',
    state: 'NY',
    zipCode: '13662'
  }, {
    code: 'MWA',
    category: 'IV',
    address: '10400 Terminal Drive, Suite 200',
    city: 'Marion',
    state: 'IL',
    zipCode: '62959'
  }, {
    code: 'OGS',
    category: 'IV',
    address: '5840 State Highway',
    city: 'Ogdensburg',
    state: 'NY',
    zipCode: '13669'
  }, {
    code: 'OTH',
    category: 'IV',
    address: '2348 Colorado Blvd.',
    city: 'North Bend',
    state: 'OR',
    zipCode: '97603'
  }, {
    code: 'OWB',
    category: 'IV',
    address: '2200 Airport Road',
    city: 'Owensboro',
    state: 'KY',
    zipCode: '42301'
  }, {
    code: 'OXR',
    category: 'IV',
    address: '2889 W. fifth St.',
    city: 'Oxnard',
    state: 'CA',
    zipCode: '93030'
  }, {
    code: 'PAH',
    category: 'IV',
    address: '2901 Fisher Road',
    city: 'Paducah',
    state: 'KY',
    zipCode: '42086'
  }, {
    code: 'PLB',
    category: 'IV',
    address: '42 Airport Lane',
    city: 'Plattsburgh',
    state: 'NY',
    zipCode: '12903'
  }, {
    code: 'PDT',
    category: 'IV',
    address: '2016 Airport Rd',
    city: 'Pendleton',
    state: 'OR',
    zipCode: '97801'
  }, {
    code: 'PGA',
    category: 'IV',
    address: '238 10th Ave',
    city: 'Page',
    state: 'AZ',
    zipCode: '86040'
  }, {
    code: 'PGV',
    category: 'IV',
    address: '400 Airport Road',
    city: 'Greenville',
    state: 'NC',
    zipCode: '27834'
  }, {
    code: 'PIB',
    category: 'IV',
    address: '1002 Terminal Drive',
    city: 'Moselle',
    state: 'MS',
    zipCode: '39459'
  }, {
    code: 'PIR',
    category: 'IV',
    address: '4001 Airport Road - Suite 202',
    city: 'Pierre',
    state: 'SD',
    zipCode: '57501'
  }, {
    code: 'PKB',
    category: 'IV',
    address: 'Rt. 31 and Airport Road',
    city: 'Parkersburg',
    state: 'WV',
    zipCode: '26187'
  }, {
    code: 'PLN',
    category: 'IV',
    address: '1395 US 31 Highway North',
    city: 'Pellston',
    state: 'MI',
    zipCode: '33913'
  }, {
    code: 'PMD',
    category: 'IV',
    address: '41000 20th St. East',
    city: 'Palmdale',
    state: 'CA',
    zipCode: '93550-2111'
  }, {
    code: 'PNC',
    category: 'IV',
    address: 'PO Box 1450',
    city: 'Ponca City',
    state: 'OK',
    zipCode: '74602'
  }, {
    code: 'PQI',
    category: 'IV',
    address: '650 Airport Dr # 11',
    city: 'Presque Isle',
    state: 'ME',
    zipCode: '04769'
  }, {
    code: 'PRC',
    category: 'IV',
    address: '6500 MacCurdy Dr.',
    city: 'Prescott',
    state: 'AZ',
    zipCode: '86301'
  }, {
    code: 'PUB',
    category: 'IV',
    address: '31201 Bryan Circle',
    city: 'Pueblo',
    state: 'CO',
    zipCode: '80100'
  }, {
    code: 'PVC',
    category: 'IV',
    address: 'Race Point Rd',
    city: 'Provincetown',
    state: 'MA',
    zipCode: '02657'
  }, {
    code: 'RHI',
    category: 'IV',
    address: '3375 Airport Road',
    city: 'Rhinelander',
    state: 'WI',
    zipCode: '54501'
  }, {
    code: 'RIW',
    category: 'IV',
    address: '4800 Airport Rd., Ste. 400',
    city: 'Riverton',
    state: 'WY',
    zipCode: '82501'
  }, {
    code: 'RKD',
    category: 'IV',
    address: '23 Terminal Lane',
    city: 'Owlshead',
    state: 'ME',
    zipCode: '04854'
  }, {
    code: 'RKS',
    category: 'IV',
    address: 'Bldg. #382 Highway 370',
    city: 'Rock Springs',
    state: 'WY',
    zipCode: '82935'
  }, {
    code: 'ROW',
    category: 'IV',
    address: '#1 Jerry Smith Circle',
    city: 'Roswell',
    state: 'NM',
    zipCode: '88203'
  }, {
    code: 'RUT',
    category: 'IV',
    address: '1002 Airport Road',
    city: 'North Clarendon',
    state: 'VT',
    zipCode: '05759'
  }, {
    code: 'SAF',
    category: 'IV',
    address: '121 Aviation DR.',
    city: 'Santa Fe',
    state: 'NM',
    zipCode: '87507'
  }, {
    code: 'SAW',
    category: 'IV',
    address: '232 Airport Avenue',
    city: 'Gwinn',
    state: 'MI',
    zipCode: '98158'
  }, {
    code: 'SBP',
    category: 'IV',
    address: '903-5 Airport Drive',
    city: 'San Luis Obispo',
    state: 'CA',
    zipCode: '93401'
  }, {
    code: 'SBY',
    category: 'IV',
    address: '5485 Airport Terminal Rd Unit A',
    city: 'Salisbury',
    state: 'MD',
    zipCode: '21804'
  }, {
    code: 'SGU',
    category: 'IV',
    address: '620 Airport Rd',
    city: 'St. George',
    state: 'UT',
    zipCode: '84770'
  }, {
    code: 'SHD',
    category: 'IV',
    address: '77 Aviation Dr',
    city: 'Weyers Cave',
    state: 'VA',
    zipCode: '24486'
  }, {
    code: 'SHR',
    category: 'IV',
    address: '901 Brundage Lane, Ste. 200',
    city: 'Sheridan',
    state: 'WY',
    zipCode: '82801'
  }, {
    code: 'SJT',
    category: 'IV',
    address: '8618 Terminal Circle Ste 101',
    city: 'San Angelo',
    state: 'TX',
    zipCode: '76904'
  }, {
    code: 'SLE',
    category: 'IV',
    address: '2290 25th St. SE',
    city: 'Salem',
    state: 'OR',
    zipCode: '97302'
  }, {
    code: 'SLK',
    category: 'IV',
    address: '96 Airport Road',
    city: 'Saranac Lake',
    state: 'NY',
    zipCode: '12983'
  }, {
    code: 'SOP',
    category: 'IV',
    address: '7865 Hwy 22 North',
    city: 'Carthage',
    state: 'NC',
    zipCode: '28327'
  }, {
    code: 'SOW',
    category: 'IV',
    address: '3150 Airport Loop # 100',
    city: 'ShowLow',
    state: 'AZ',
    zipCode: '86901'
  }, {
    code: 'TBN',
    category: 'IV',
    address: 'Forney Army Airfield, Building 5002 Iowa Avenue',
    city: 'Ft. Leonard Wood',
    state: 'MO',
    zipCode: '65473'
  }, {
    code: 'TEX',
    category: 'IV',
    address: '1500 Last Dollar Rd',
    city: 'Telluride',
    state: 'CO',
    zipCode: '81435'
  }, {
    code: 'TTN',
    category: 'IV',
    address: 'Bear Tavern Rd',
    city: 'West Trenton',
    state: 'NJ',
    zipCode: '08628'
  }, {
    code: 'TUP',
    category: 'IV',
    address: '2704 W. Jackson Steet',
    city: 'Tupelo',
    state: 'MS',
    zipCode: '38801'
  }, {
    code: 'TVF',
    category: 'IV',
    address: '13722 Airport Drive',
    city: 'Thief River Falls',
    state: 'MN',
    zipCode: '56701'
  }, {
    code: 'TWF',
    category: 'IV',
    address: '524 Airport Loop Road',
    city: 'Twin Falls',
    state: 'ID',
    zipCode: '83301'
  }, {
    code: 'TYR',
    category: 'IV',
    address: '700 Skyway Blvd., Suite 102',
    city: 'Tyler',
    state: 'TX',
    zipCode: '75704'
  }, {
    code: 'UIN',
    category: 'IV',
    address: '1645 Highway 104 East',
    city: 'Quincy',
    state: 'IL',
    zipCode: '62305'
  }, {
    code: 'UNV',
    category: 'IV',
    address: '2493 Fox Hill Rd.',
    city: 'University Park',
    state: 'PA',
    zipCode: '16803'
  }, {
    code: 'VCT',
    category: 'IV',
    address: '609 Foster Field Drive, Suite F',
    city: 'Victoria',
    state: 'TX',
    zipCode: '77904'
  }, {
    code: 'VDZ',
    category: 'IV',
    address: '300 Airport Road',
    city: 'Valdez',
    state: 'AK',
    zipCode: '99686'
  }, {
    code: 'VEL',
    category: 'IV',
    address: '835 South 500 East',
    city: 'Vernal',
    state: 'UT',
    zipCode: '84078'
  }, {
    code: 'VIS',
    category: 'IV',
    address: '9500 W. Airport Drive',
    city: 'Visalia',
    state: 'CA',
    zipCode: '93277'
  }, {
    code: 'WRL',
    category: 'IV',
    address: '1436 Airport Rd.',
    city: 'Worland',
    state: 'WY',
    zipCode: '82401'
  }, {
    code: 'WYS',
    category: 'IV',
    address: '721 Yellowstone Airport Road',
    city: 'West Yellowstone',
    state: 'MT',
    zipCode: '59578'
  }, {
    code: 'YKM',
    category: 'IV',
    address: '2300 W Washington Ave',
    city: 'Yakima',
    state: 'WA',
    zipCode: '98903'
  }, {
    code: 'NYL',
    category: 'IV',
    address: '2191 E. 32nd St',
    city: 'Yuma',
    state: 'AZ',
    zipCode: '85365'
  }, {
    code: 'MSY2',
    category: 'Off-Site',
    address: '120 Mallard Street, Suite 200',
    city: 'St. Rose',
    state: 'LA',
    zipCode: '70087'
  }, {
    code: 'AVP2',
    category: 'Off-Site',
    address: '201 Hangar Rd, Suite 106',
    city: 'Avoca',
    state: 'PA',
    zipCode: '18641'
  }, {
    code: 'EKO2',
    category: 'Off-Site',
    address: '803 Murray Way',
    city: 'Elko',
    state: 'NV',
    zipCode: '89801'
  }, {
    code: 'MLU2',
    category: 'Off-Site',
    address: '1130 Pecanland Road',
    city: 'Monroe',
    state: 'LA',
    zipCode: '71203'
  }, {
    code: 'AEX2',
    category: 'Off-Site',
    address: '7228 England Drive, Profitt Center, Suite 6',
    city: 'Alexandria',
    state: 'LA',
    zipCode: '71303'
  }, {
    code: 'HPN2',
    category: 'Off-Site',
    address: '10 New King Street',
    city: 'White Plains',
    state: 'NY',
    zipCode: '10604'
  }, {
    code: 'ROA2',
    category: 'Off-Site',
    address: '5251 Concourse Drive',
    city: 'Roanoke',
    state: 'VA',
    zipCode: '24019'
  }, {
    code: 'SBP2',
    category: 'Off-Site',
    address: '805 Aerovista Place suite 203',
    city: 'San Luis Obispo',
    state: 'CA',
    zipCode: '93401'
  }, {
    code: 'LIH2',
    category: 'Off-Site',
    address: '4280A Rice St',
    city: 'Lihue',
    state: 'HI',
    zipCode: '96766'
  }, {
    code: 'RAP2',
    category: 'Off-Site',
    address: '4275 Airport Road, Suite C',
    city: 'Rapid City',
    state: 'SD',
    zipCode: '57703'
  }, {
    code: 'FSD2',
    category: 'Off-Site',
    address: '200 E 10th Street- Suite 202',
    city: 'Soiux Falls',
    state: 'SD',
    zipCode: '57104'
  }, {
    code: 'MSO2',
    category: 'Off-Site',
    address: '100 West Railroad Street',
    city: 'Missoula',
    state: 'MT',
    zipCode: '59802'
  }, {
    code: 'CAK2',
    category: 'Off-Site',
    address: '5399 Lauby Rd, Centrum One, Ste 100',
    city: 'Green',
    state: 'OH',
    zipCode: '44720'
  }, {
    code: 'MHT2',
    category: 'Off-Site',
    address: '4 Technology Drive',
    city: 'Londonderry',
    state: 'NH',
    zipCode: '03053'
  }, {
    code: 'OAK2',
    category: 'Off-Site',
    address: '7677 Oakport Street Suite 900',
    city: 'Oakland',
    state: 'CA',
    zipCode: '94621'
  }, {
    code: 'PNS2',
    category: 'Off-Site',
    address: '700 South Palafox Street  Suite 205',
    city: 'Pensacola',
    state: 'FL',
    zipCode: '32502'
  }, {
    code: 'RDU2',
    category: 'Off-Site',
    address: 'One Copley Pkwy Suite 600',
    city: 'Morrisville',
    state: 'NC',
    zipCode: '27560'
  }, {
    code: 'BTV2',
    category: 'Off-Site',
    address: '110 Kimball Avenue Suite 100',
    city: 'South Burlington',
    state: 'VT',
    zipCode: '05403'
  }, {
    code: 'KOA2',
    category: 'Off-Site',
    address: '74-5620 Palani Rd Suite 207',
    city: 'Kailua Kona',
    state: 'HI',
    zipCode: '96740'
  }, {
    code: 'SBN2',
    category: 'Off-Site',
    address: '1843 Commerce Drive, Suite 200',
    city: 'South Bend',
    state: 'IN',
    zipCode: '46628'
  }, {
    code: 'LAN2',
    category: 'Off-Site',
    address: '3121 Circle Drive, Suite 200',
    city: 'Lansing',
    state: 'MI',
    zipCode: '48906'
  }, {
    code: 'ONT2',
    category: 'Off-Site',
    address: '3401 Centrelake Dr., #625',
    city: 'Ontario',
    state: 'CA',
    zipCode: '91761'
  }, {
    code: 'ABE2',
    category: 'Off-Site',
    address: '961 Marcon Blvd, Suite #105',
    city: 'Allentown',
    state: 'PA',
    zipCode: '18109'
  }, {
    code: 'EUG2',
    category: 'Off-Site',
    address: '28845 Lockheed Dr.',
    city: 'Eugene',
    state: 'OR',
    zipCode: '97402'
  }, {
    code: 'GSP2',
    category: 'Off-Site',
    address: '181-C Johns Road',
    city: 'Greer',
    state: 'SC',
    zipCode: '29650'
  }, {
    code: 'LBB2',
    category: 'Off-Site',
    address: '1304 Ave. K',
    city: 'Lubbock',
    state: 'TX',
    zipCode: '79401'
  }, {
    code: 'OGG2',
    category: 'Off-Site',
    address: '425 Koloa Street, Suite 106',
    city: 'Kahului',
    state: 'HI',
    zipCode: '96732'
  }, {
    code: 'OGG2',
    category: 'Off-Site',
    address: '33 Lono Ave Suite 270-290',
    city: 'Kahului',
    state: 'HI',
    zipCode: '96732'
  }, {
    code: 'BIL2',
    category: 'Off-Site',
    address: '1737 US Hwy 3',
    city: 'Billings',
    state: 'MT',
    zipCode: '59105'
  }, {
    code: 'MDT2',
    category: 'Off-Site',
    address: '517 Airport Drive',
    city: 'Middletown',
    state: 'PA',
    zipCode: '17057'
  }, {
    code: 'MFR2',
    category: 'Off-Site',
    address: '4002 Cirrus Dr.  2nd floor',
    city: 'Medford',
    state: 'OR',
    zipCode: '97504'
  }, {
    code: 'TYS2',
    category: 'Off-Site',
    address: '3401 Russ Circle, Suite G',
    city: 'Alcoa',
    state: 'TN',
    zipCode: '37701'
  }, {
    code: 'RIC2',
    category: 'Off-Site',
    address: '5707 Huntsman Rd. Suite 200',
    city: 'Richmond',
    state: 'VA',
    zipCode: '23250'
  }, {
    code: 'RNO2',
    category: 'Off-Site',
    address: '1755 E. Plumb Lane Ste. 241',
    city: 'Reno',
    state: 'NV',
    zipCode: '89502'
  }, {
    code: 'PHL2',
    category: 'Off-Site',
    address: '2 International Plaza Suite 640',
    city: 'Philadelphia',
    state: 'PA',
    zipCode: '19113'
  }, {
    code: 'BHM2',
    category: 'Off-Site',
    address: '6500 43 rd Ave North',
    city: 'Birmingham',
    state: 'AL',
    zipCode: '35206'
  }, {
    code: 'GEG2',
    category: 'Off-Site',
    address: '7904 West Pilot Drive',
    city: 'Spokane',
    state: 'WA',
    zipCode: '99224'
  }, {
    code: 'IAD2',
    category: 'Off-Site',
    address: '45045 Aviation Drive',
    city: 'Dulles',
    state: 'VA',
    zipCode: '20166'
  }, {
    code: 'JAX2',
    category: 'Off-Site',
    address: '14201 Pecan Park Road',
    city: 'Jacksonville',
    state: 'FL',
    zipCode: '32218'
  }, {
    code: 'AUS2',
    category: 'Off-Site',
    address: '6800 Burleson Rd. Bldg 310; Ste 160',
    city: 'Austin',
    state: 'TX',
    zipCode: '78744'
  }, {
    code: 'ORF2',
    category: 'Off-Site',
    address: '2200 Norview Ave',
    city: 'Norfolk',
    state: 'VA',
    zipCode: '23518'
  }, {
    code: 'ISP2',
    category: 'Off-Site',
    address: 'One Corporate Drive, Suite GL4,',
    city: 'Ronkonkoma',
    state: 'NY',
    zipCode: '11716'
  }, {
    code: 'BOS2',
    category: 'Off-Site',
    address: 'Boston Logan Office Center, 1 Harborside Drive',
    city: 'Boston',
    state: 'MA',
    zipCode: '02128'
  }, {
    code: 'BOS2',
    category: 'Off-Site',
    address: 'Fish Pier West, Suite 303, 212 Northern Ave',
    city: 'Boston',
    state: 'MA',
    zipCode: '02210'
  }, {
    code: 'BOS2',
    category: 'Off-Site',
    address: '70 Everett Avenue',
    city: 'Chelsea',
    state: 'MA',
    zipCode: '02150'
  }, {
    code: 'BDL2',
    category: 'Off-Site',
    address: '334 Ella Grasso Turnpike - Suite 200',
    city: 'Windsor Locks',
    state: 'CT',
    zipCode: '06096'
  }, {
    code: 'GSO2',
    category: 'Off-site',
    address: '6415 Bryan Blvd',
    city: 'Greensboro',
    state: 'NC',
    zipCode: '27409'
  }, {
    code: 'PVD2',
    category: 'Off-Site',
    address: '2000 Post Road Ê',
    city: 'Warwick',
    state: 'RI',
    zipCode: '02886'
  }, {
    code: 'HOU2',
    category: 'Off-Site',
    address: '7135 Office City Drive, Suite 200',
    city: 'Houston',
    state: 'TX',
    zipCode: '77087'
  }, {
    code: 'CLE2',
    category: 'Off-Site',
    address: '20445 Emerald Parkway Suite 300',
    city: 'Cleveland',
    state: 'OH',
    zipCode: '44135'
  }, {
    code: 'ELP2',
    category: 'Off-Site',
    address: '1200 Golden Key Circle, Suite 260',
    city: 'El Paso',
    state: 'TX',
    zipCode: '79925'
  }, {
    code: 'DAL2',
    category: 'Off-Site',
    address: '3890 West Northwest Highway, Ste 102',
    city: 'Dallas',
    state: 'TX',
    zipCode: '75220'
  }, {
    code: 'IND2',
    category: 'Off-Site',
    address: '5420 W Southern Avenue, Suite 203',
    city: 'Indianappolis',
    state: 'IN',
    zipCode: '46241'
  }, {
    code: 'MEM2',
    category: 'Off-Site',
    address: '1991 Corporate Avenue Suite 300',
    city: 'Memphis',
    state: 'TN',
    zipCode: '38132'
  }, {
    code: 'OKC2',
    category: 'Off-Site',
    address: '5700 SW 36th Street, Suite A',
    city: 'Oklahoma City',
    state: 'OK',
    zipCode: '73179'
  }, {
    code: 'PDX2',
    category: 'Off-Site',
    address: '8338 NE Alderwood Rd. Suite 200',
    city: 'Portland',
    state: 'OR',
    zipCode: '97220'
  }, {
    code: 'LGA2',
    category: 'Off-Site',
    address: '75-20 Astoria Blvd ste 300',
    city: 'East Elmhurst',
    state: 'NY',
    zipCode: '11370'
  }, {
    code: 'CLT2',
    category: 'Off-site',
    address: '3800 Arco Corporate Drive, Suite 400',
    city: 'Charlotte',
    state: 'NC',
    zipCode: '28273'
  }, {
    code: 'SFO2',
    category: 'Off-Site',
    address: '245 South Spruce Ave.',
    city: 'South San Fransicso',
    state: 'CA',
    zipCode: '94080'
  }, {
    code: 'PHX2',
    category: 'Off-Site',
    address: '4127 E. Van Buren St., Ste. 255',
    city: 'Phoenix',
    state: 'AZ',
    zipCode: '85008'
  }, {
    code: 'STL2',
    category: 'Off-Site',
    address: '10801 Pear Tree Lane Suite 300',
    city: 'St. Ann,',
    state: 'MO',
    zipCode: '63074'
  }, {
    code: 'IAH2',
    category: 'Off-Site',
    address: '3838 N. Sam Houston Pkwy E., Ste. 510',
    city: 'Houston',
    state: 'TX',
    zipCode: '77032'
  }, {
    code: 'SAN2',
    category: 'Off-Site',
    address: 'Suite 1800, 401 West A Street',
    city: 'San Diego',
    state: 'CA',
    zipCode: '92101'
  }, {
    code: 'LAX2',
    category: 'Off-Site',
    address: '5757 Century Blvd., Suite 750',
    city: 'Los Angles',
    state: 'CA',
    zipCode: '90045'
  }, {
    code: 'LAX2',
    category: 'Off-site',
    address: '5520 Arbor Vitae',
    city: 'Westchester',
    state: 'CA',
    zipCode: '90045'
  }, {
    code: 'LAX2',
    category: 'Off-site',
    address: '1 Worldway 6th Floor',
    city: 'Los Angles',
    state: 'CA',
    zipCode: '90045'
  }, {
    code: 'BWI2',
    category: 'Off-Site',
    address: '801 International Drive, Suite 300',
    city: 'Linthicum',
    state: 'MD',
    zipCode: '21090'
  }, {
    code: 'SLC2',
    category: 'Off-Site',
    address: '2284 West 160 North',
    city: 'Salt Lake City',
    state: 'UT',
    zipCode: '84116'
  }, {
    code: 'DTW2',
    category: 'Off-site',
    address: '11895 S Wayne Road, Suite 103 Dock#10',
    city: 'Romulus',
    state: 'MI',
    zipCode: '48174'
  }, {
    code: 'DTW2',
    category: 'Off-Site',
    address: '11100 Metro Airport Center Drive, Suite 160',
    city: 'Romulus',
    state: 'MI',
    zipCode: '48174'
  }, {
    code: 'MSP2',
    category: 'Off-Site',
    address: '3001 Metro Drive, Suite 200',
    city: 'Bloomington',
    state: 'MN',
    zipCode: '55425'
  }, {
    code: 'ORD2',
    category: 'Off-Site',
    address: '1700 W Higgins- Suite 480',
    city: 'Chicago (Ohare)',
    state: 'IL',
    zipCode: '60018'
  }, {
    code: 'MCO2',
    category: 'Off-Site',
    address: '5850 T G Lee Blvd  Suite 610',
    city: 'Orlando',
    state: 'FL',
    zipCode: '32822'
  }, {
    code: 'DEN2',
    category: 'Off-Site',
    address: '3855 Lewiston Street, Suite 400',
    city: 'Aurora',
    state: 'CO',
    zipCode: '80011'
  }, {
    code: 'ATL2',
    category: 'Off-Site',
    address: 'International Office Park 3848 Northwest Drive',
    city: 'Atlanta',
    state: 'GA',
    zipCode: '30337'
  }, {
    code: 'ABI2',
    category: 'Off-Site',
    address: '1215 E. S. 11th St., Suite B',
    city: 'Abilene',
    state: 'TX',
    zipCode: '79602'
  }, {
    code: 'ABQ2',
    category: 'Off-site',
    address: '2920-A Yale Blvd SE',
    city: 'Albuquerque',
    state: 'NM',
    zipCode: '87106'
  }, {
    code: 'ACY2',
    category: 'Off-Site',
    address: '3 Canale Drive - Suite 10',
    city: 'Egg Harbor',
    state: 'NJ',
    zipCode: '08234'
  }, {
    code: 'ADQ2',
    category: 'Off-Site',
    address: '1420 Airport Way',
    city: 'Kodiak',
    state: 'AK',
    zipCode: '99615'
  }, {
    code: 'ANC2',
    category: 'Off-Site',
    address: '4000 W.50th Ste#300',
    city: 'Anchorage',
    state: 'AK',
    zipCode: '99502'
  }, {
    code: 'ASE2',
    category: 'Off-Site',
    address: '113B Aspen Aiport Business Center',
    city: 'Aspen',
    state: 'CO',
    zipCode: '81611'
  }, {
    code: 'AZO2',
    category: 'Off-Site',
    address: '2314 Helen Avenue, Suite 200',
    city: 'Kalamazoo',
    state: 'MI',
    zipCode: '49002'
  }, {
    code: 'BIS2',
    category: 'Off-Site',
    address: '919 S 7th St, Ste 602',
    city: 'Bismarck',
    state: 'ND',
    zipCode: '58504'
  }, {
    code: 'BMI2',
    category: 'Off-Site',
    address: '2901 East Empire, Suite 200',
    city: 'Bloomington',
    state: 'IL',
    zipCode: '61704'
  }, {
    code: 'BRW2',
    category: 'Off-Site',
    address: '1078 Kongak (Wells Fargo Bldg)',
    city: 'Barrow',
    state: 'AK',
    zipCode: '99709'
  }, {
    code: 'BTR2',
    category: 'Off-Site',
    address: '9191 Plank Road',
    city: 'Baton Rouge',
    state: 'LA',
    zipCode: '70811'
  }, {
    code: 'BUR2',
    category: 'Off-Site',
    address: '2919 W. Empire Ave',
    city: 'Burbank',
    state: 'CA',
    zipCode: '91504'
  }, {
    code: 'BZN2',
    category: 'Off-Site',
    address: '550 Gallatin Field Road',
    city: 'Belgrade',
    state: 'MT',
    zipCode: '59714'
  }, {
    code: 'CDV2',
    category: 'Off-Site',
    address: 'Jim Air Building, Mile 13, Copper River Highway',
    city: 'Cordova',
    state: 'AK',
    zipCode: '99574'
  }, {
    code: 'CMH2',
    category: 'Off-Site',
    address: '2780 Airport Drive  Suite 200',
    city: 'Columbus',
    state: 'OH',
    zipCode: '43219'
  }, {
    code: 'CRQ2',
    category: 'Off-Site',
    address: 'Suite 370, 2141 Palomar Airport Road',
    city: 'Carlsbad',
    state: 'CA',
    zipCode: '92011'
  }, {
    code: 'CVG2',
    category: 'Off-Site',
    address: '3900 Olympic Blvd Suite 200',
    city: 'Erlanger',
    state: 'KY',
    zipCode: '41018'
  }, {
    code: 'CWA2',
    category: 'Off-Site',
    address: '425B Orbiting Drive',
    city: 'Mosinee',
    state: 'WI',
    zipCode: '54455'
  }, {
    code: 'DAY2',
    category: 'Off-Site',
    address: '303 Corporate Center Drive Suite 200',
    city: 'Vandalia',
    state: 'OH',
    zipCode: '45377'
  }, {
    code: 'DFW2',
    category: 'Off-Site',
    address: '510 Airline Drive, Suite 110',
    city: 'Coppell',
    state: 'TX',
    zipCode: '75019'
  }, {
    code: 'DSM2',
    category: 'Off-Site',
    address: '2120 Rittenhouse Street, Suite B',
    city: 'Des Moines',
    state: 'IA',
    zipCode: '50321'
  }, {
    code: 'EAR2',
    category: 'Off-Site',
    address: '816 E 25th Street, Suite 5',
    city: 'Kearney',
    state: 'NE',
    zipCode: '68847'
  }, {
    code: 'EWR2',
    category: 'Off-Site',
    address: '614 Frelinghuysen Ave.',
    city: 'Newark',
    state: 'NJ',
    zipCode: '07101'
  }, {
    code: 'FAI2',
    category: 'Off-Site',
    address: '5904 Old Airport Way, Suite 2B',
    city: 'Fairbanks',
    state: 'AK',
    zipCode: '99709'
  }, {
    code: 'FAT2',
    category: 'Off-Site',
    address: '1907 N. Gateway, Suite 101',
    city: 'Fresno',
    state: 'CA',
    zipCode: '93727'
  }, {
    code: 'FLL2',
    category: 'Off-Site',
    address: '1050 Lee Wagener Boulevard, Suite 303',
    city: 'Ft. Lauderdale',
    state: 'FL',
    zipCode: '33315'
  }, {
    code: 'GBD2',
    category: 'Off-Site',
    address: '9015 8th Street',
    city: 'Great Bend',
    state: 'KS',
    zipCode: '67530'
  }, {
    code: 'GRR2',
    category: 'Off-Site',
    address: '4665 Broadmoor Ave,. SE Suite 250',
    city: 'Grand Rapids',
    state: 'MI',
    zipCode: '49512'
  }, {
    code: 'GST2',
    category: 'Off-Site',
    address: 'P.O. Box',
    city: 'Gustavus',
    state: 'AK',
    zipCode: '99826'
  }, {
    code: 'GUC2',
    category: 'Off-Site',
    address: '500 W Hwy 50, Suite 101A',
    city: 'Gunnison',
    state: 'CO',
    zipCode: '81230'
  }, {
    code: 'HOM2',
    category: 'Off-Site',
    address: '1529 Ocean Drive, Suite 2',
    city: 'Homer',
    state: 'AK',
    zipCode: '99603'
  }, {
    code: 'JAN2',
    category: 'Off-Site',
    address: '10 Canebrake Suite 310',
    city: 'Flowood',
    state: 'MS',
    zipCode: '39232'
  }, {
    code: 'JFK2',
    category: 'Off-Site',
    address: '230-59 Rockaway Blvd, Suite 210',
    city: 'Jamaica',
    state: 'NY',
    zipCode: '11413'
  }, {
    code: 'JNU2',
    category: 'Off-Site',
    address: '8800 Glacier Hwy., Ste. 215',
    city: 'Juneau',
    state: 'AK',
    zipCode: '99801'
  }, {
    code: 'KTN2',
    category: 'Off-Site',
    address: '1000 Airport Terminal, Hanger 1, 2d Floor',
    city: 'Ketchikan',
    state: 'AK',
    zipCode: '99901'
  }, {
    code: 'LAS2',
    category: 'Off-Site',
    address: '375 E. Warm Springs Rd., Suite 200',
    city: 'Las Vegas',
    state: 'NV',
    zipCode: '89119'
  }, {
    code: 'MCI2',
    category: 'Off-Site',
    address: '12200 N. Ambassador Drive Suite 212',
    city: 'Kansas City',
    state: 'MO',
    zipCode: '64163'
  }, {
    code: 'MDW2',
    category: 'Off-Site',
    address: '5333 S. Laramie Ave., Suite 220',
    city: 'Chicago (Midway)',
    state: 'IL',
    zipCode: '60638'
  }, {
    code: 'MIA2',
    category: 'Off-Site',
    address: '8400 NW 36th Street  Suite 300',
    city: 'Miami',
    state: 'FL',
    zipCode: '33166'
  }, {
    code: 'MKE2',
    category: 'Off-Site',
    address: '5007 South Howell Ave, Suite 200',
    city: 'Milwalkee',
    state: 'WI',
    zipCode: '53207'
  }, {
    code: 'MKG2',
    category: 'Off-Site',
    address: '5000 Hakes Dr, Suite 700',
    city: 'Muskegon',
    state: 'MI',
    zipCode: '49441'
  }, {
    code: 'MSN2',
    category: 'Off-Site',
    address: '2701 Int\'l Ln, Suite 106',
    city: 'Madison',
    state: 'WI',
    zipCode: '53704'
  }, {
    code: 'OMA2',
    category: 'Off-Site',
    address: '1102 E Hartman Ave.',
    city: 'Omaha',
    state: 'NE',
    zipCode: '68110'
  }, {
    code: 'OME2',
    category: 'Off-Site',
    address: '214 Front Street (Sitnasauk Bldg)',
    city: 'Nome',
    state: 'AK',
    zipCode: '99762'
  }, {
    code: 'OTZ2',
    category: 'Off-Site',
    address: '241  5th Ave',
    city: 'Kotzebue',
    state: 'AK',
    zipCode: '99752'
  }, {
    code: 'PIT2',
    category: 'Off-Site',
    address: '220 Airside Drive',
    city: 'Corapolis',
    state: 'PA.',
    zipCode: '15108'
  }, {
    code: 'PPG2',
    category: 'Off-Site',
    address: '1 Ottoville Road, Tradewinds Hotel, Suite 110',
    city: 'Pago Pago',
    state: 'AS',
    zipCode: '96799'
  }, {
    code: 'PSC2',
    category: 'Off-Site',
    address: '2815 St. Andrews Loop, Suite 4D',
    city: 'Pasco',
    state: 'WA',
    zipCode: '99301'
  }, {
    code: 'PSG2',
    category: 'Off-Site',
    address: '1500-C Haugen Drive',
    city: 'Petersburg',
    state: 'AK',
    zipCode: '99833'
  }, {
    code: 'ROC2',
    category: 'Off-Site',
    address: 'One Airport Way Suite 200',
    city: 'Rochester',
    state: 'NY',
    zipCode: '14624'
  }, {
    code: 'SAT2',
    category: 'Off-Site',
    address: '70 NE Loop 410 STE 1000',
    city: 'San Antonio',
    state: 'TX',
    zipCode: '78216'
  }, {
    code: 'SBA2',
    category: 'Off-Site',
    address: '6144 Calle Real Suite 201',
    city: 'Goleta',
    state: 'CA',
    zipCode: '93117'
  }, {
    code: 'SCC2',
    category: 'Off-Site',
    address: '100 Sag River Rd (Carlile Passenger Terminal)',
    city: 'Prudhoe Bay',
    state: 'AK',
    zipCode: '99734'
  }, {
    code: 'SEA2',
    category: 'Off-Site',
    address: '18800 8th Ave S Suite 2400',
    city: 'Seattle',
    state: 'WA',
    zipCode: '98148'
  }, {
    code: 'SEA2',
    category: 'Off-Site',
    address: '2800 So. 192 St.',
    city: 'SEATAC',
    state: 'WA',
    zipCode: '98188'
  }, {
    code: 'SGF2',
    category: 'Off-Site',
    address: '2530 Airport Plaza  Avenue',
    city: 'Springfield',
    state: 'MO',
    zipCode: '65803'
  }, {
    code: 'SGU2',
    category: 'Off-Site',
    address: '317 Donlee Drive',
    city: 'St. George',
    state: 'UT',
    zipCode: '84770'
  }, {
    code: 'SIT2',
    category: 'Off-Site',
    address: '601 Alice Loop Rd., Ste 102',
    city: 'Sitka',
    state: 'AK',
    zipCode: '99835'
  }, {
    code: 'SJC2',
    category: 'Off-Site',
    address: '1735 Technology Dr. Suite 240',
    city: 'San Jose',
    state: 'CA',
    zipCode: '95110'
  }, {
    code: 'SJT2',
    category: 'Off-Site',
    address: '5376 Stewart Lane',
    city: 'San Angelo',
    state: 'TX',
    zipCode: '76904'
  }, {
    code: 'SWF2',
    category: 'Off-Site',
    address: '33 Airport Center Drive',
    city: 'New  Windsor',
    state: 'NY',
    zipCode: '12553'
  }, {
    code: 'TUS2',
    category: 'Off-Site',
    address: '7250 S. Plumer Ave',
    city: 'Tuscon',
    state: 'AZ',
    zipCode: '85706'
  }, {
    code: 'TVC2',
    category: 'Off-Site',
    address: '1760 Forest Ridge Drive., Suite B',
    city: 'Traverse City',
    state: 'MI',
    zipCode: '49686'
  }, {
    code: 'WRG2',
    category: 'Off-Site',
    address: '223 Front Street',
    city: 'Wrangell',
    state: 'AK',
    zipCode: '99929'
  }, {
    code: 'YAK2',
    category: 'Off-Site',
    address: '427 Waldron Blvd. #203',
    city: 'Yakutat',
    state: 'AK',
    zipCode: '99689'
  }, {
    code: 'BLF',
    category: 'N/A',
    address: 'Route 5, Box 202',
    city: 'Bluefield',
    state: 'WV',
    zipCode: '24701'
  }, {
    code: 'HGR',
    category: 'N/A',
    address: '18434 Showalter Rd # 1',
    city: 'Hagerstown',
    state: 'MD',
    zipCode: '21742'
  }, {
    code: 'VQS',
    category: 'N/A',
    address: 'Puerto Rico Ports Authority',
    city: 'Vieques',
    state: 'PR',
    zipCode: '00765'
  }, {
    code: 'ECP',
    category: 'N/A',
    address: '6300 West Bay Parkway',
    city: 'Panama City',
    state: 'FL',
    zipCode: '32409'
  }, {
    code: 'LAL',
    category: 'N/A',
    address: '3900 Don Emerson Drive',
    city: 'Lakeland',
    state: 'FL',
    zipCode: '33811'
  }, {
    code: 'GCN',
    category: 'N/A',
    address: 'Highway 64/1300 Liberator Drive',
    city: 'Grand Canyon City',
    state: 'AZ',
    zipCode: '86023'
  }, {
    code: 'HOB',
    category: 'N/A',
    address: '6601 W. Carlsbad Highway',
    city: 'Hobbs',
    state: 'NM',
    zipCode: '88240'
  }, {
    code: 'PVU',
    category: 'N/A',
    address: '3421 Mike Jense Parkway',
    city: 'Provo',
    state: 'UT',
    zipCode: '84601'
  }, {
    code: 'MSC1 - PA',
    category: 'MSC',
    address: '701 Market St.; Suite 3200',
    city: 'Philadelphia',
    state: 'PA',
    zipCode: '19106'
  }, {
    code: 'MSC2 - GA',
    category: 'MSC',
    address: '285 Peachtree Center Ave., Suite 900 Marquis Tower II',
    city: 'Atlanta',
    state: 'GA',
    zipCode: '30303'
  }, {
    code: 'MSC3 - MI',
    category: 'MSC',
    address: '1000 Towne Center, Suite 1850',
    city: 'Southfield',
    state: 'MI',
    zipCode: '48075'
  }, {
    code: 'MSC4 - TX',
    category: 'MSC',
    address: '8615 North Freeport Parkway',
    city: 'Irving',
    state: 'TX',
    zipCode: '75063'
  }, {
    code: 'MSC5 - CA',
    category: 'MSC',
    address: '450 Golden Gate Ave Suite 1-5246',
    city: 'San Francisco',
    state: 'CA',
    zipCode: '94012'
  }, {
    code: 'MSC6 - CA',
    category: 'MSC',
    address: '345 Middlefield Rd.',
    city: 'Menlo Park',
    state: 'CA',
    zipCode: '94025'
  }, {
    code: 'TTAC - MD',
    category: 'FIELD',
    address: '132 National Business Parkway',
    city: 'Annapolis Junction',
    state: 'MD.',
    zipCode: ''
  }, {
    code: 'TTAC - CO',
    category: 'FIELD',
    address: '121 South Tejon St., Suite 1200,',
    city: 'Colorado Springs',
    state: 'CO',
    zipCode: ''
  }, {
    code: 'TSA Personnel Security',
    category: 'FIELD',
    address: '7404 Executive Place Suite 325',
    city: 'Lanham',
    state: 'MD',
    zipCode: '20706'
  }, {
    code: 'TCC',
    category: 'FIELD',
    address: '460 Industrial Blvd.',
    city: 'London',
    state: 'KY',
    zipCode: '40741'
  }, {
    code: 'Bashen Corporation',
    category: 'FIELD',
    address: '1616 South Voss Suite 300',
    city: 'Houston',
    state: 'TX',
    zipCode: '77057'
  }, {
    code: 'TTAC Herndon',
    category: 'FIELD',
    address: '3076 Centerville Road Suite 200',
    city: 'Herndon',
    state: 'VA',
    zipCode: '20171'
  }, {
    code: 'Crossroads Mediation',
    category: 'FIELD',
    address: '9250 Mosby Street',
    city: 'Manassas',
    state: 'VA',
    zipCode: '20111'
  }, {
    code: 'P3',
    category: 'FIELD',
    address: '1235 South Clark Street Suite 1302',
    city: 'Arlington',
    state: 'VA',
    zipCode: '22202'
  }, {
    code: 'Transportation Technology Center',
    category: 'FIELD',
    address: '55500 D.O.T. Road',
    city: 'Pueblo',
    state: 'CO',
    zipCode: '81001'
  }, {
    code: 'IBM Courthouse',
    category: 'FIELD',
    address: '2000 North 14th Street Suite 320',
    city: 'Arlington',
    state: 'VA',
    zipCode: '22201'
  }, {
    code: 'TSA Training Facility',
    category: 'FIELD',
    address: '1131 Chapel Crossing Road, TH376',
    city: 'Glyco',
    state: 'GA',
    zipCode: '31524'
  }, {
    code: 'TSIF',
    category: 'FIELD',
    address: '1 W Post Office Road',
    city: 'Washington',
    state: 'DC',
    zipCode: '20001'
  }, {
    code: 'TSA SOC',
    category: 'FIELD',
    address: '22001 Loudoun County Parkway',
    city: 'Ashburn',
    state: 'VA',
    zipCode: '20147'
  }, {
    code: 'TSA SOC',
    category: 'FIELD',
    address: '7000 Weston Parkway',
    city: 'Cary',
    state: 'NC',
    zipCode: '27513'
  }, {
    code: 'TSA National Targeting Center',
    category: 'FIELD',
    address: '12825 Worldgate Drive',
    city: 'Herndon',
    state: 'VA',
    zipCode: '20598'
  }, {
    code: 'NTSB',
    category: 'FIELD',
    address: '45065 Riverside Parkway',
    city: 'Ashburn',
    state: 'VA',
    zipCode: '20147'
  }, {
    code: 'FAMS HQ',
    category: 'FIELD',
    address: '1900 Oracle Way Suites 4/5/8',
    city: 'Reston',
    state: 'VA',
    zipCode: '20190'
  }, {
    code: 'PMO',
    category: 'FIELD',
    address: '1200 South Hayes',
    city: 'Arlington',
    state: 'VA',
    zipCode: '22202'
  }, {
    code: 'Merrifield',
    category: 'FIELD',
    address: '8613 Lee Highway',
    city: 'Fairfax',
    state: 'VA',
    zipCode: '22031'
  }, {
    code: 'SITE W',
    category: 'FIELD',
    address: '19844 Blueridge Mountain Road',
    city: 'Bluemont',
    state: 'VA',
    zipCode: '20135'
  }, {
    code: 'Springfield',
    category: 'FIELD',
    address: '6810 Loisdale Road',
    city: 'Springfield',
    state: 'VA',
    zipCode: '22150'
  }, {
    code: 'Headquarters Main',
    category: 'HQ',
    address: '601 South Street',
    city: 'Arlington',
    state: 'VA',
    zipCode: '22202'
  }, {
    code: 'Headquarters Main',
    category: 'HQ',
    address: '701 South Street',
    city: 'Arlington',
    state: 'VA',
    zipCode: '22202'
  }, {
    code: 'Metro Park',
    category: 'HQ',
    address: '6534 Walker Lane',
    city: 'Alexandria',
    state: 'VA',
    zipCode: '22310'
  }, {
    code: 'Freedom Center (Formerly named TSOC)',
    category: 'HQ',
    address: '13555 EDS Drive',
    city: 'Herndon',
    state: 'VA',
    zipCode: '20171'
  }, {
    code: 'Los Angeles',
    category: 'FAMS',
    address: '1700 East Walnut Avenue',
    city: 'El Segundo',
    state: 'CA',
    zipCode: '90245'
  }, {
    code: 'San Diego RAC',
    category: 'FAMS',
    address: '33050 Nixie Way Suite 160',
    city: 'San Diego',
    state: 'CA',
    zipCode: '92147'
  }, {
    code: 'San Francisco',
    category: 'FAMS',
    address: '395 Oyster Point Blvd',
    city: 'South San Francisco',
    state: 'CA',
    zipCode: '94080'
  }, {
    code: 'Denver',
    category: 'FAMS',
    address: '4400 Kittredge Street',
    city: 'Denver',
    state: 'CO',
    zipCode: '80239'
  }, {
    code: 'Orlando',
    category: 'FAMS',
    address: '5950 Hazeltine National Drive',
    city: 'Orlando',
    state: 'FL',
    zipCode: '32822'
  }, {
    code: 'Tampa RAC',
    category: 'FAMS',
    address: '4200 George J. Bean Pkwy Suite 2108',
    city: 'Tampa',
    state: 'FL',
    zipCode: '33607'
  }, {
    code: 'Miami',
    category: 'FAMS',
    address: '13800 N.W. 14th St.',
    city: 'Sunrise',
    state: 'FL',
    zipCode: '33323'
  }, {
    code: 'Atlanta',
    category: 'FAMS',
    address: '1075 S. Inner Loop Rd',
    city: 'College Park',
    state: 'GA',
    zipCode: '30337'
  }, {
    code: 'Chicago',
    category: 'FAMS',
    address: '899 Upper Express Dr.',
    city: 'Chicago',
    state: 'IL',
    zipCode: '60666'
  }, {
    code: 'Cincinnati',
    category: 'FAMS',
    address: '4243 Olympic Blvd.',
    city: 'Erlanger',
    state: 'KY',
    zipCode: '41018'
  }, {
    code: 'Boston',
    category: 'FAMS',
    address: '70 Everett Avenue',
    city: 'Chelsea',
    state: 'MA',
    zipCode: '02150'
  }, {
    code: 'Baltimore',
    category: 'FAMS',
    address: '7037 Ridge Road',
    city: 'Hanover',
    state: 'MD',
    zipCode: '21076'
  }, {
    code: 'Detroit',
    category: 'FAMS',
    address: '11301 Metro Airport Center Drive',
    city: 'Romulus',
    state: 'MI',
    zipCode: '48174'
  }, {
    code: 'Minneapolis',
    category: 'FAMS',
    address: '7900 West 78th Street',
    city: 'Edina',
    state: 'MN',
    zipCode: '55439'
  }, {
    code: 'Charlotte',
    category: 'FAMS',
    address: '2231 Edge Lake Drive',
    city: 'Charlotte',
    state: 'NC',
    zipCode: '28217'
  }, {
    code: 'Newark',
    category: 'FAMS',
    address: '10 Rooney Circle',
    city: 'West Orange',
    state: 'NJ',
    zipCode: '07052'
  }, {
    code: 'Las Vegas',
    category: 'FAMS',
    address: '6380 S. Valley View Blvd.',
    city: 'Las Vegas',
    state: 'NV',
    zipCode: '89118'
  }, {
    code: 'Phoenix RAC',
    category: 'FAMS',
    address: '4646 E. Van Buren Suite 150',
    city: 'Phoenix',
    state: 'AZ',
    zipCode: '85008'
  }, {
    code: 'New York',
    category: 'FAMS',
    address: '75-20 Astoria Blvd.',
    city: 'East Elmhurst',
    state: 'NY',
    zipCode: '11370'
  }, {
    code: 'Cleveland',
    category: 'FAMS',
    address: '1200 Resource Drive',
    city: 'Brooklyn Heights',
    state: 'OH',
    zipCode: '44131'
  }, {
    code: 'Philadelphia',
    category: 'FAMS',
    address: '2 International Plaza',
    city: 'Philadelphia',
    state: 'PA',
    zipCode: '19113'
  }, {
    code: 'Dallas',
    category: 'FAMS',
    address: '510 Airline Drive',
    city: 'Coppell',
    state: 'TX',
    zipCode: '75109'
  }, {
    code: 'Artesia - Training Center',
    category: 'FAMS',
    address: '1300 W. Richey Avenue  Building 25, Second Floor',
    city: 'Artesia',
    state: 'NM',
    zipCode: '88210'
  }, {
    code: 'Houston',
    category: 'FAMS',
    address: '10055 Regal Row',
    city: 'Houston',
    state: 'TX',
    zipCode: '77040'
  }, {
    code: 'Washington, DC',
    category: 'FAMS',
    address: '14434 Albemarle Point Place',
    city: 'Chantilly',
    state: 'VA',
    zipCode: '20151'
  }, {
    code: 'Seattle',
    category: 'FAMS',
    address: '500 Powell Ave. SW',
    city: 'Renton',
    state: 'WA',
    zipCode: '98057'
  }, {
    code: 'OLE/FAMS - Reston',
    category: 'FAMS',
    address: '1901 Oracle Way',
    city: 'Reston',
    state: 'VA',
    zipCode: '20190'
  }, {
    code: 'Ashburn Datacenter',
    category: 'FAMS',
    address: '45065 Riverside Parkway',
    city: 'Ashburn',
    state: 'VA',
    zipCode: '20147'
  }, {
    code: 'Freedom Center',
    category: 'FAMS',
    address: '13555 EDS Drive (And NOVA)',
    city: 'Herndon',
    state: 'VA',
    zipCode: '20171'
  }, {
    code: 'Atlantic City - Training Center & SSF',
    category: 'FAMS',
    address: 'William J. Hughes Tech Center Bldg. 201',
    city: 'Atlantic City Int\'l',
    state: 'NJ',
    zipCode: '08405'
  }, {
    code: 'TSA HQ - Pentagon City',
    category: 'FAMS',
    address: '601 South Street',
    city: 'Arlington',
    state: 'VA',
    zipCode: '20598'
  }, {
    code: 'Lackland Air Force Base - K9',
    category: 'FAMS',
    address: '1320 Truemper St Bldg 9122 Room 2611',
    city: 'San Antonio',
    state: 'TX',
    zipCode: '78236'
  }, function(err, siteCode) {
    deferred.resolve();
  });
  return deferred.promise;
}

function populateSeverities() {
  var deferred = q.defer();
  Severity.create({
        level : 'S3',
        warning_level: 'green'
      }, {
        level : 'S2',
        warning_level: 'yellow'
      }, {
        level : 'S1',
        warning_level: 'red'
  }, function(err, severity) {
    deferred.resolve();
  });
  return deferred.promise;
}

function populateComments() {
  var deferred = q.defer();
  Comment.create({
      text : 'I have a problem where I am initialising a variable on the scope in a controller. Then it gets changed in another controller when a user logs in. This variable is used to control things such as the navigation bar and restricts access to parts of the site depending on the type of user, so its important that it holds its value. The problem with it is that the controller that initialises it, gets called again by angular some how and then resets the variable back to its initial value. I assume this is not the correct way of declaring and initialising global variables, well its not really global, so my question is what is the correct way and is there any good examples around that work with the current version of angular?',
      username: 'mjiang@42six.com'
    }, {
      text : 'Services are singletons that you can inject to any controller and expose their values in a controllers scope. Services, being singletons are still "global" but you have got far better control over where those are used and exposed.',
      type: 'update',
      username: 'user1@42six.com'
    }, {
      text : 'The same thing can be achieved using a Provider, Factory, or Service since they are "just syntactic sugar on top of a provider recipe" but using Value will achieve what you want with minimal syntax.',
      type: 'update',
      username: 'user2@42six.com'
    }, {
      text : 'Please correct me if I am wrong, but when Angular 2.0 is released I do not believe$rootScope will be around. My conjecture is based on the fact that $scope is being removed as well. Obviously controllers, will still exist, just not in the ng-controller fashion.Think of injecting controllers into directives instead. As the release comes imminent, it will be best to use services as global variables if you want an easier time to switch from verison 1.X to 2.0.',
      type: 'update',
      username: 'user3@42six.com'
    }, {
      text : 'Comment #5'
  }, function(err, comments) {
    deferred.resolve();
  });
  return deferred.promise;
}

function populateIncidents(comments, incidentTypes, siteCodes, severities) {
  var deferred = q.defer();
  Incident.create({
    name : 'LA Servers down',
    description: 'the servers in LA are not responding',
    comments: [comments[0], comments[1], comments[2], comments[3]],
    site_code: _.find(siteCodes, {code:'MSY2'}),
    incident_type: incidentTypes[1],
    severity: severities[0]
  }, {
    name : 'Mainframe Meltdown',
    description: 'mainframe overheated',
    completed: true,
    comments: [comments[0], comments[1]],
    site_code: siteCodes[500],
    incident_type: incidentTypes[1],
    severity: severities[2]
  }, {
    name : 'Can\'t communicate with FAA',
    description: 'we have been unable to reach FAA all morning',
    comments: [comments[4]],
    site_code: siteCodes[530],
    incident_type: incidentTypes[2],
    severity: severities[1]
  },  {
    name : 'We don\'t know whats going on',
    description: 'total chaos',
    comments: [comments[3]],
    site_code: siteCodes[520],
    incident_type: incidentTypes[0],
    severity: severities[0]
  },  {
    name : 'Issue with switches in New York',
    description: 'switches are dropping half of the packets it receives',
    comments: [comments[0], comments[1], comments[2]],
    site_code: _.find(siteCodes, {code:'HPN2'}),
    incident_type: incidentTypes[1],
    severity: severities[1]
  },{
    name : 'Power outages',
    description: 'power outages have left our servers in Florida down',
    comments: [],
    site_code: _.find(siteCodes, {code:'MCO2'}),
    incident_type: incidentTypes[1],
    severity: severities[2]
  }, function(err, incidents) {
    deferred.resolve();
  });
  return deferred.promise;
}

function populateDevices() {
  var deferred = q.defer();
  Device.create({
    name: "User1's iPad Air",
    token: "derfe2-34ffg-rt65g-32frgf",
    uuid: "derfe2-34ffg-rt65g-32frgf",
    owner: "user1@42six.com",
    channel: "sendbox"
  }, {
    name: "User2's Galaxy S3",
    token: "jk34k-45klk-6kjh2-klkj4",
    uuid: "jk34k-45klk-6kjh2-klkj4",
    owner: "user2@42six.com",
    channel: "gcm"
  }, function(err, devices) {
    deferred.resolve();
  });
  return deferred.promise;
}

function populateMessages(incidents) {
  var deferred = q.defer();
  Message.create({
    message : 'A new incident has been reported',
    type: 'new',
    status: 'created',
    incident: incidents[0]
  }, {
    message : 'A new incident has been reported',
    type: 'new',
    status: 'viewed',
    incident: incidents[1]
  }, {
    message : 'An incident has been updated',
    type: 'update',
    status: 'updated',
    incident: incidents[2]
  },  {
    message : 'A new incident has been reported',
    type: 'new',
    status: 'created',
    incident: incidents[3]
  },  {
    message : 'An incident has been updated',
    type: 'update',
    status: 'viewed',
    incident: incidents[4]
  },{
    message : 'A new incident has been reported',
    type: 'new',
    status: 'created',
    incident: incidents[5]
  }, function(err, messages) {
    deferred.resolve();
  });
  return deferred.promise;
}

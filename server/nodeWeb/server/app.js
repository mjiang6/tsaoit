/**
 * Main application file
 */

'use strict';

// Set default node environment to development
process.env.NODE_ENV = process.env.NODE_ENV || 'development';

// enable http
var http = require('http');

var express = require('express');
var mongoose = require('mongoose');
var config = require('./config/environment');

// Connect to database
mongoose.connect(config.mongo.uri, config.mongo.options);
mongoose.connection.on('error', function(err) {
	console.error('MongoDB connection error: ' + err);
	process.exit(-1);
	}
);
// Populate DB with sample data
if(config.seedDB) { require('./config/seed'); }

// Setup server
var app = express();
//var server = require('http').createServer(app);
var httpServer = http.createServer(app);

if ('production' === app.get('env')) {
  // enable https
  var fs = require('fs');
  var https = require('https');
  var privateKey  = fs.readFileSync('/etc/pki/tls/certs/42six.com.key', 'utf8');
  var certificate = fs.readFileSync('/etc/pki/tls/certs/42six.com.crt', 'utf8');
  var credentials = {key: privateKey, cert: certificate};
  var httpsServer = https.createServer(credentials, app);
}

require('./config/express')(app);
require('./routes')(app);

// Start server
//server.listen(config.port, config.ip, function () {
//  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
//});
httpServer.listen(config.port, config.ip, function () {
  console.log('Express server listening on %d, in %s mode', config.port, app.get('env'));
});

if ('production' === app.get('env')) {
  // enable https
  httpsServer.listen(config.port_ssl, config.ip, function () {
    console.log('Express server listening on %d, in %s mode', config.port_ssl, app.get('env'));
  });
}

// Expose app
module.exports = app;

'use strict';

angular.module('webappApp')
  .service('idManager', idManager);

function idManager() {
	/*jshint validthis: true */
	var service = this;

	
	/** Service Variables **/


	/** Service Functions **/
	service.getGUID = _getGUID;


	/****** Implementation ******/

	/**
	 * Generates a GUID and returns it
	 *
	 * @returns an array of incident objects
	 */
	function _getGUID() {
		function s4() {
      		return Math.floor((1 + Math.random()) * 0x10000)
        		.toString(16)
        		.substring(1);
    	}
    	return s4() + s4() + '-' + s4() + '-' + s4() + '-' + s4() + '-' + s4() + s4() + s4();
	}
	
}
'use strict';

angular.module('webappApp')
  .service('pushNotificationsManager', pushNotifications);

function pushNotifications($q, $http) {
	/*jshint validthis: true */
	var service = this;

	
	/** Service Variables **/
	service.PNSServerIP = 'http://do-not-know.com';

	/** Service Functions **/
	service.sendNotification = _sendNotification;



	/****** Implementation ******/

	/**
	 * Sends a Push Notification message about an incident to 
	 * the PNS server that will enable it to send push notifications 
	 * to the proper iphone users
	 *
     * @param inicident: An Incident model object
	 *
	 * @returns null
	 */
	function _sendNotification(incident) {
		var deferred = $q.defer();

		$http.post(service.PNSServerIP, incident.toPushNotification)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

}
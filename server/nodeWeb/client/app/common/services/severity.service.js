'use strict';

angular.module('webappApp')
  .service('severityManager', severityManager);

function severityManager($q, $http, Severity) {
	/*jshint validthis: true */
	var service = this;

	
	/** Service Variables **/


	/** Service Functions **/
	service.getAll = _getAll;


	/****** Implementation ******/

	/**
	 * Retrieves an array of every incident type in the database using
	 * the node API
	 *
	 * @returns an array of incident type objects
	 */
	function _getAll() {
		var deferred = $q.defer();

		$http.get('api/severity')
			.success(function(severityObjs) {
				var severities = [];
				severityObjs.forEach(function(severityObj) {
					severities.push(new Severity(severityObj));
				});
				deferred.resolve(severities);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}
	
}
'use strict';

angular.module('webappApp')
  .service('commentsManager', commentsManager);

function commentsManager($q, $http) {
	/*jshint validthis: true */
	var service = this;


	/** Service Variables **/


	/** Service Functions **/
	service.createComment = _createComment;


	/****** Implementation ******/


	/**
	 * Creates an Comment in the database by sending a post to the
	 * node API
	 *
	 * @returns nothing
	 */
	function _createComment(comment) {
    comment.type = 'new';
		var deferred = $q.defer();

		$http.post('api/comments', comment)
			.success(function(data, status) {
				deferred.resolve(status);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

}

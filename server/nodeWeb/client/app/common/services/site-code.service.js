'use strict';

angular.module('webappApp')
  .service('siteCodesManager', siteCodesManager);

function siteCodesManager($q, $http, SiteCode) {
	/*jshint validthis: true */
	var service = this;


	/** Service Variables **/


	/** Service Functions **/
	service.getAll = _getAll;


	/****** Implementation ******/

	/**
	 * Retrieves an array of site codes by category, or
   * all site codes if category is not presented
	 * the node API
	 *
	 * @returns an array of siteCode type objects
	 */
	function _getAll(category) {
		var deferred = $q.defer();
    var url = 'api/siteCodes';
    if (category) {
      url += '?category='+category;
    }
    console.log('URL: ', url);
		$http.get(url)
			.success(function(siteCodeObjs) {
				var siteCodes = [];
        _.forEach(siteCodeObjs, function(siteCodeObj) {
          siteCodes.push(new SiteCode(siteCodeObj));
        });
				deferred.resolve(_.sortBy(siteCodes, 'code'));
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

}

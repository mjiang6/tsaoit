'use strict';

angular.module('webappApp')
  .service('incidentsManager', incidentsManager);

function incidentsManager($q, $http, Incident) {
	/*jshint validthis: true */
	var service = this;


	/** Service Variables **/


	/** Service Functions **/
	service.getAll = _getAll;
	service.getById = _getById;
	service.upsertIncident = _upsertIncident;


	/****** Implementation ******/

	/**
	 * Retrieves an array of every incident in the database using
	 * the node API
	 *
	 * @returns an array of incident objects
	 */
	function _getAll() {
		var deferred = $q.defer();

		$http.get('api/incidents')
			.success(function(incidentObjs) {
				var incidents = [];
				incidentObjs.forEach(function(incidentObj) {
					incidents.push(new Incident(incidentObj));
				});
        _.sortBy(incidents, function(incident) {
          return new Date(incident.incident_time);
        });
				deferred.resolve(incidents.reverse());
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

	/**
	 * Retrieves a specific incident from the database
	 *
	 * @returns an incident object
	 */
	function _getById(id) {
		var deferred = $q.defer();

		$http.get('api/incidents/' + id)
			.success(function(incidentObjs) {
				deferred.resolve(new Incident(incidentObjs));
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

	/**
	 * Creates an Incident in the database by sending a post to the
	 * node API
	 *
	 * @returns nothing
	 */
	function _upsertIncident(incident) {
		var deferred = $q.defer();

		$http.post('api/incidents', incident)
			.success(function(data) {
				deferred.resolve(data);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

}

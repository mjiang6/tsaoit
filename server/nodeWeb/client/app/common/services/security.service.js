'use strict';

angular.module('webappApp')
  .service('securityManager', securityManager);

function securityManager($q, $http) {
	/*jshint validthis: true */
	var service = this;

	/** Service Variables **/


	/** Service Functions **/
	service.authenticate = _authenticate;
  service.getUsername = _getUsername;
  service.username = 'anonymous';


	/****** Implementation ******/


	/**
	 * Authenticate a user by sending a post to the
	 * node API
	 *
	 * @returns nothing
	 */
	function _authenticate(username, password) {
    service.username = username;

		var deferred = $q.defer();
	  $http.post('api/auth', {username: username, password: password})
			.success(function(data, status) {
				deferred.resolve(status);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

  function _getUsername() {
    return service.username;
  }

}

'use strict';

angular.module('webappApp')
  .service('incidentTypesManager', incidentTypesManager);

function incidentTypesManager($q, $http, IncidentType) {
	/*jshint validthis: true */
	var service = this;

	
	/** Service Variables **/


	/** Service Functions **/
	service.getAll = _getAll;
	service.createIncidentType = _createIncidentType;

	/****** Implementation ******/

	/**
	 * Retrieves an array of every incident type in the database using
	 * the node API
	 *
	 * @returns an array of incident type objects
	 */
	function _getAll() {
		var deferred = $q.defer();

		$http.get('api/incidentTypes')
			.success(function(incidentTypeObjs) {
				var incidentTypes = [];
				incidentTypeObjs.forEach(function(incidentTypeObj) {
					incidentTypes.push(new IncidentType(incidentTypeObj));
				});
				deferred.resolve(incidentTypes);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}

	/**
	 * Creates an Incident Type in the database
	 *
	 * @returns nothing
	 */
	function _createIncidentType(incidentType) {
		var deferred = $q.defer();

		$http.post('api/incidentTypes', incidentType)
			.success(function(data) {
				var newIncidentType = new IncidentType(data);
				deferred.resolve(newIncidentType);
			})
			.error(function(data, status) {
				deferred.reject(status);
			});

		return deferred.promise;
	}
	
}
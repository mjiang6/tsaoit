'use strict';
angular.module('webappApp')
  .factory('Severity', severity);

function severity() {

  // Constructor
  function Severity(severityData) {
    var model = this;
    if (severityData) { //Init-Constructor
      model._id = severityData._id;
      model.level = severityData.level;
      model.warning_level = severityData.warning_level;

    } else { //Default Constructor
      model._id = null;
      model.level = '';
      model.warning_level = '';
    }
  }

  // Member Functions
  Severity.prototype = {
    // All Business Logic Functions

    someFunction: function() {
      // blah blah
    }


  };
  return Severity;

}
'use strict';
angular.module('webappApp')
  .factory('IncidentType', incidentType);

function incidentType() {

  // Constructor
  function IncidentType(incidentTypeData) {
    var model = this;

    if (incidentTypeData) { //Init-Constructor
      model._id = incidentTypeData._id;
      model.description = incidentTypeData.description;

    } else { //Default Constructor
      model._id = null;
      model.description = '';
    }
  }

  // Member Functions
  IncidentType.prototype = {
    // All Business Logic Functions

    someFunction: function() {
      // blah blah
    }


  };
  return IncidentType;

}
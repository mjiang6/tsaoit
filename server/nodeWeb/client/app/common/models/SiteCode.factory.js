'use strict';
angular.module('webappApp')
  .factory('SiteCode', siteCode);

function siteCode() {

  // Constructor
  function SiteCode(siteCodeData) {
    var model = this;
    if (siteCodeData) { //Init-Constructor
      model._id = siteCodeData._id;
      model.code = siteCodeData.code;

    } else { //Default Constructor
      model._id = null;
      model.code = '';
    }
  }

  // Member Functions
  SiteCode.prototype = {
    // All Business Logic Functions

    someFunction: function() {
      // blah blah
    }


  };
  return SiteCode;

}
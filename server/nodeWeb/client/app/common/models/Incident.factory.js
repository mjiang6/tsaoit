'use strict';
angular.module('webappApp')
  .factory('Incident', incident);

function incident(IncidentType, SiteCode, Comment, Severity) {

  // Constructor
  function Incident(incidentData) {
    var model = this;

    model.comments = [];
    if (incidentData) { //Init-Constructor
      model._id = incidentData._id;
      model.name = incidentData.name;
      model.description = incidentData.description;
      model.reported_time = incidentData.reported_time;
      model.incident_time = incidentData.incident_time;

      model.completed = incidentData.completed;

      // Referenced Models
      model.incident_type = new IncidentType(incidentData.incident_type);
      model.site_code = new SiteCode(incidentData.site_code);
      model.severity = new Severity(incidentData.severity);

      incidentData.comments.forEach(function(comment) {
       model.comments.push(new Comment(comment));
      });

    } else { //Default Constructor
      model._id = null;
      model.name = '';
      model.description = '';
      model.reported_time = null;
      model.incident_time = null;

      model.completed = false;

      // Referenced Models
      model.incident_type = new IncidentType();
      model.site_code = new SiteCode();
      model.severity = new Severity();
    }
  }

  // Member Functions
  Incident.prototype = {
    // All Business Logic Functions

    toPushNotification: function() {
      var model = this;

      return {
        sender: 'dummyAccount@42six.com',
        message: model.name,
        alerts: {
          Alert: true,
          Badge: true,
          Sound: false
        },
        targets: {
          groups: [],
          users: []
        }

      };
    }

  };
  return Incident;

}
'use strict';
angular.module('webappApp')
  .factory('Comment', comment);

function comment() {

  // Constructor
  function Comment(commentData) {
    var model = this;
    if (commentData) { //Init-Constructor
      model._id = commentData._id;
      model.text = commentData.text;
      model.type = commentData.type;
      model.username = commentData.username;
      model.created_time = commentData.created_time;

    } else { //Default Constructor
      model._id = null;
      model.text = '';
      model.type = 'new';
      model.username = 'anonymous';
      model.created_time = null;
    }
  }

  // Member Functions
  Comment.prototype = {
    // All Business Logic Functions

    someFunction: function() {
      // blah blah
    }


  };
  return Comment;

}

'use strict';

angular.module('webappApp')
	.controller('DatePickerCtrl', datePickerCtrl)
	.directive('datePicker', datePickerDirective);

function datePickerCtrl() {
  var viewModel = this; // jshint ignore:line

  /** Directive Variables **/

  /** Directive Functions **/


  /****** Implementation ******/

}

function datePickerDirective() {
	return {
		restrict: 'E',
		scope: {
			incident: '='
		},
		templateUrl: 'app/common/partials/date-picker.partial.html',
		controller: 'DatePickerCtrl',
    controllerAs: 'datePickerVM'
  };
}

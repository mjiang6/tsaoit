'use strict';

angular.module('webappApp')
  .controller('LoginCtrl', loginCtrl);

function loginCtrl($scope, $http, $state, securityManager) {
	/*jshint validthis: true */
	var viewModel = this;


	/** Controller Variables **/

	/** Controller Functions **/
	viewModel.login = _login;


	/****** Implementation ******/

	function _login() {
    securityManager.authenticate(viewModel.username, viewModel.password);
		$state.go('incidents.create');
	}

}

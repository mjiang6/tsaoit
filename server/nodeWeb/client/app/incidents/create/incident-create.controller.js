'use strict';

angular.module('webappApp')
  .controller('IncidentCreateCtrl', incidentCreateCtrl);

function incidentCreateCtrl($scope, $http, $state, $stateParams, Incident, incidentsManager, IncidentType, incidentTypesManager, siteCodesManager, severityManager) {
  	/*jshint validthis: true */
	var viewModel = this;


	/** Controller Variables **/
	viewModel.invalid = {};
	viewModel.incident = null;
	viewModel.incidentTypes = null;
	viewModel.siteCodes = null;
	viewModel.severities = null;
	viewModel.incidentTypeText = null;

	/** Controller Functions **/
	viewModel.updateIncidentType = _updateIncidentType;
	viewModel.reportIncident = _reportIncident;
	viewModel.createIncidentType = _createIncidentType;

	_initController();

  	/****** Implementation ******/


  	function _reportIncident() {
  		if (!_isValid()) {
  			return;
  		}

  		function _continue(incident) {
  			viewModel.incident._id = incident._id;
	  		$state.go('incidents.description', {incident: viewModel.incident, alerts: [{type: 'success', msg: 'You have successfully created a new incident report'}]});
			$scope.$emit('updateIncidentsList', 'update the incidents');
  		}

  		viewModel.incident.reported_time = new Date();
  		incidentsManager.upsertIncident(viewModel.incident).then(_continue);

  		// TODO: Send PNS Notification

  	}

  	function _updateIncidentType(incidentType) {
  		viewModel.incident.incident_type = incidentType;
  	}

  	function _isValid() {
  		viewModel.invalid.name = viewModel.incident.name ? null : true;
  		viewModel.invalid.description = viewModel.incident.description ? null : true;
  		viewModel.invalid.incident_type = (viewModel.incident.incident_type && viewModel.incident.incident_type.description) ? null : true;
  		viewModel.invalid.site_code = (viewModel.incident.site_code && viewModel.incident.site_code.code) ? null : true;
  		viewModel.invalid.severity = ( viewModel.incident.severity && viewModel.incident.severity.level) ? null : true;
  		viewModel.invalid.incident_time = viewModel.incident.incident_time ? null : true;
  		return !_.contains(_.values(viewModel.invalid), true);
  	}

  	function _createIncidentType() {
  		function _updateIncidentTypes(newIncidentType) {
  			viewModel.incidentTypes.push(newIncidentType);
  			viewModel.updateIncidentType(newIncidentType);

  			viewModel.incidentTypeText = '';
  		}

  		if (!viewModel.incidentTypeText || viewModel.incidentTypeText === '') {
  			return;
  		}

  		var newType = new IncidentType({description: viewModel.incidentTypeText});
  		incidentTypesManager.createIncidentType(newType).then(_updateIncidentTypes);

  	}

	function _initController() {

		function _setIncidentTypes(incidentTypes) {
			viewModel.incidentTypes = incidentTypes;
		}

		function _setSiteCodes(siteCodes) {
			viewModel.siteCodes = siteCodes;
			viewModel.incident.site_code = _.find(viewModel.siteCodes, {_id: viewModel.incident.site_code._id});
		}

		function _setSeverities(severities) {
			viewModel.severities = severities;
			viewModel.incident.severity = _.find(viewModel.severities, {_id: viewModel.incident.severity._id});
		}

		viewModel.incident = $stateParams.incident;

		if (!viewModel.incident) {
			viewModel.incident = new Incident();
		}

		incidentTypesManager.getAll().then(_setIncidentTypes);
		siteCodesManager.getAll('Off-Site').then(_setSiteCodes);
		severityManager.getAll().then(_setSeverities);
	}
}

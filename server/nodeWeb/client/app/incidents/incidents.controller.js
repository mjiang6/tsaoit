'use strict';

angular.module('webappApp')
  .controller('IncidentsCtrl', incidentsCtrl);

function incidentsCtrl($scope, $rootScope, $http, $state, securityManager, incidentsManager) {
	/*jshint validthis: true */
	var viewModel = this;


	/** Controller Variables **/
	viewModel.incidents = null;
  viewModel.username = securityManager.getUsername();


	/** Controller Functions **/
	viewModel.openIncident = _openIncident;
	viewModel.goToCreateIncident = _goToCreateIncident;



	_initController();

	/****** Implementation ******/

	/**
   	 * If an incident is added, the controller adding the incident will
   	 * emit an update event, allowing us to fetch a new, updated list of
   	 * incidents.
   	 *
   	 * @returns void
   	 */
   	 // listen for the event in the relevant $scope
	$scope.$on('updateIncidentsList', function () {
  		// Populate Controller Incident Variable from the API
		incidentsManager.getAll().then(_setIncidents);
	});


	function _openIncident(incident) {
		$state.go('incidents.description', {incident: incident, alerts: null});
	}

	function _setIncidents(incidents) {
		viewModel.incidents = incidents;
	}

	function _goToCreateIncident() {
		$state.go('incidents.create');
	}

	function _initController() {
		// Populate Controller Incident Variable from the API
		incidentsManager.getAll().then(_setIncidents);
	}

}

'use strict';

angular.module('webappApp')
  .controller('IncidentDescriptionCtrl', incidentDescriptionCtrl);

function incidentDescriptionCtrl($scope, $http, $state, $stateParams, securityManager, incidentsManager, commentsManager, Comment) {
	/*jshint validthis: true */
	var viewModel = this;


	/** Controller Variables **/
	viewModel.incident = null;
	viewModel.showTextArea = null;
	viewModel.commentText = null;
	viewModel.alerts = [];

	/** Controller Functions **/
	viewModel.editIncident = _editIncident;
	viewModel.toggleCompleted = _toggleCompleted;
	viewModel.openTextArea = _openTextArea;
	viewModel.addComment = _addComment;

	_initController();

  	/****** Implementation ******/

  	function _addComment() {
  		if (!viewModel.commentText) {
  			return;
  		}
  		var comment = new Comment({text: viewModel.commentText });
  		comment.incident_id = viewModel.incident._id;
      comment.username = securityManager.getUsername();

  		commentsManager.createComment(comment);


  		viewModel.commentText = '';
  		viewModel.showTextArea = false;

  		incidentsManager.getById(viewModel.incident._id).then(_updateIncident);
  	}

  	function _toggleCompleted() {
  		viewModel.incident.completed = !viewModel.incident.completed;
  		incidentsManager.upsertIncident(viewModel.incident);
  	}

  	function _editIncident() {
  		if (viewModel.incident.completed) {
  			return;
  		}
  		$state.go('incidents.create', {incident: viewModel.incident});
  	}

  	function _updateIncident(incident) {
		viewModel.incident = incident;
	}

	function _initController() {
		viewModel.incident = $stateParams.incident;
  	    viewModel.alerts = $stateParams.alerts;
		viewModel.showTextArea = false;

		incidentsManager.getById(viewModel.incident._id).then(_updateIncident);
	}

	function _openTextArea() {
		viewModel.showTextArea = true;
	}
}

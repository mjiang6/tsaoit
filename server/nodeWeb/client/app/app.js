'use strict';

angular.module('webappApp', [
  'ngCookies',
  'ngResource',
  'ngMessages',
  'ngSanitize',
  'ngAnimate',
  'ui.router',
  'ui.bootstrap',
  'ui.bootstrap.datetimepicker',
  'angularMoment'
])
  .config(function ($stateProvider, $urlRouterProvider, $locationProvider) {
    $urlRouterProvider
      .otherwise('/login');

    $locationProvider.html5Mode(true);

    $stateProvider
      .state('login', {
        url: '/login',
        templateUrl: 'app/login/login.html',
        controller: 'LoginCtrl',
        controllerAs: 'loginCtrl'
      })

      .state('incidents', {
        url: '/incidents',
        templateUrl: 'app/incidents/incidents.html',
        controller: 'IncidentsCtrl',
        controllerAs: 'incidentsCtrl'
      })

      .state('incidents.create', {
        url: '/incidents/create',
        templateUrl: 'app/incidents/create/incident-create.html',
        controller: 'IncidentCreateCtrl',
        controllerAs: 'incidentCreateCtrl',
        params: {incident: null, alerts: null}
      })

      .state('incidents.description', {
        url: '/incidents/description',
        templateUrl: 'app/incidents/description/incident-description.html',
        controller: 'IncidentDescriptionCtrl',
        controllerAs: 'incidentDescriptionCtrl',
        params: {incident: null, alerts: null}
      });

  });

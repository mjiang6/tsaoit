//
//  RootViewController.h
//
//  Created by Michael Jiang on 9/7/2012.
//  Copyright (c) 2012 CSRA. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SWING_ALT				0
#define SWING_NEXT			1
#define SWING_BACK		 	2
#define CROSS_DISSOLVE 	4

@interface RootViewController : UIViewController

- (UIViewController *)openStory:(NSString *)story;
- (UIViewController *)openStory:(NSString *)story dire:(int)dire;
- (UIViewController *)openScene:(UIViewController *)scene;
- (UIViewController *)openScene:(UIViewController *)scene dire:(int)dire;
- (UIViewController *)openPrevScene;
- (UIViewController *)openPrevScene:(int)dire;

- (void)transitionFrom:(UIViewController *)fromScene  toScene:(UIViewController *)toScene option:(UIViewAnimationOptions)option;

- (void)setContainerView:(UIView *)view;

- (UIViewController *)getSceneInStory:(NSString *)story sceneName:(NSString *)sceneName;
- (UIViewController *)getRootSceneInStory:(NSString *)story;
- (UIViewController *)getCurrentScene;

+ (RootViewController *)singleton;

@end

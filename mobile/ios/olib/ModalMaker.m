//
//  ModalMaker.m
//  hub
//
//  Created by Michael Jiang on 10/12/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "ModalMaker.h"

@implementation ModalMaker

+ (void)makeModalView:(UIView *)view {
  [ModalMaker makeModalView:view border:1.0];
}

+ (void)makeModalView:(UIView *)view border:(CGFloat)width {
  [ModalMaker makeModalView:view style:UIBlurEffectStyleLight border:width];
}

+ (void)makeModalView:(UIView *)view style:(UIBlurEffectStyle)style border:(CGFloat)width {
  if (width > 0) {
    view.layer.cornerRadius = 8.0;
  	view.layer.masksToBounds = YES;
  	view.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:0.5].CGColor;
  	view.layer.borderWidth = width;
  }
  view.backgroundColor = [UIColor clearColor];
  UIBlurEffect *blurEffect = [UIBlurEffect effectWithStyle:style];
  UIVisualEffectView *blurEffectView = [[UIVisualEffectView alloc] initWithEffect:blurEffect];
  blurEffectView.frame = view.bounds;
  blurEffectView.autoresizingMask = UIViewAutoresizingFlexibleWidth | UIViewAutoresizingFlexibleHeight;
  [view addSubview:blurEffectView];
}

@end

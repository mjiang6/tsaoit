//
//  BaseViewController.h
//
//  Created by Michael Jiang on 9/7/2012.
//  Copyright (c) 2012 CSRA. All rights reserved.
//

#import <UIKit/UIKit.h>

#define SWING_ALT				0
#define SWING_NEXT			1
#define SWING_BACK			2
#define CROSS_DISSOLVE 	4

@interface BaseViewController : UIViewController

@property  (strong, nonatomic) NSString *story;

- (UIViewController *)transitionToRoot;
- (UIViewController *)transitionToScene:(NSString *)sceneName;
- (UIViewController *)transitionBackToScene:(NSString *)sceneName;
- (UIViewController *)transitionBack;
- (UIViewController *)transitionBack:(int)dire;
- (UIViewController *)openStory:(NSString *)story;
- (UIViewController *)openStory:(NSString *)story dire:(int)dire;
- (UIViewController *)getScene:(NSString *)sceneName;
//- (UIViewController *)getCurrentScene;
- (UIViewController *)getSceneInStory:(NSString *)story sceneName:(NSString *)sceneName;
- (UIViewController *)getRootSceneInStory:(NSString *)story;
//- (UIViewController *)toScene:(UIViewController *)scene;


-(BOOL)isTransitioning;
-(void) reset;
-(void) refresh;
-(void) closeScene;

@end

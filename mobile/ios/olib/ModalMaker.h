//
//  ModalMaker.h
//  hub
//
//  Created by Michael Jiang on 10/12/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ModalMaker: NSObject

+ (void)makeModalView:(UIView *)view;
+ (void)makeModalView:(UIView *)view border:(CGFloat)width;
+ (void)makeModalView:(UIView *)view style:(UIBlurEffectStyle)style border:(CGFloat)width;

@end

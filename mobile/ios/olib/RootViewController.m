//
//  RootViewController.m
//
//  Created by Michael Jiang on 9/7/2012.
//  Copyright (c) 2012 CSRA. All rights reserved.
//

#import "RootViewController.h"
#import "BaseViewController.h"

#define DURATION 0.5

@interface RootViewController () {
  NSMutableDictionary *sceneMap;
  UIViewController *sceneCur;
  UIViewController *scenePrev;
  UIView *containerView;
  BOOL flip;
}

@end

@implementation RootViewController

// singleton has to be below synthesize
static RootViewController *_singleton = nil;
+ (RootViewController *)singleton {
	return _singleton;
}

- (void)setContainerView:(UIView *)view {
  containerView = view;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  sceneMap = [NSMutableDictionary dictionary];
  sceneCur = nil;
  flip = FALSE;
  _singleton = self;
}

- (void)viewDidUnload {
  [super viewDidUnload];
  sceneMap = nil;
}

- (UIViewController *)openStory:(NSString *)story {
  return [self openScene:[self getRootSceneInStory:story] dire:SWING_ALT];
}

- (UIViewController *)openStory:(NSString *)story dire:(int)dire {
  return [self openScene:[self getRootSceneInStory:story] dire:dire];
}

- (UIViewController *)openPrevScene {
  return [self openPrevScene:SWING_ALT];
}

- (UIViewController *)openPrevScene:(int)dire {
  return [self openScene:scenePrev dire:dire];
}

- (UIViewController *)openScene:(UIViewController *)scene {
  if (scene == nil)
    return nil;
  return [self openScene:scene dire:SWING_ALT];
}

- (UIViewController *)openScene:(UIViewController *)scene dire:(int)dire {
  if (scene == nil)
    return nil;
  UIViewAnimationOptions opt = flip? UIViewAnimationOptionTransitionFlipFromLeft: UIViewAnimationOptionTransitionFlipFromRight;
  if (dire == SWING_NEXT)
    opt = UIViewAnimationOptionTransitionFlipFromLeft;
  else if (dire == SWING_BACK)
    opt = UIViewAnimationOptionTransitionFlipFromRight;
  else if (dire == CROSS_DISSOLVE)
    opt = UIViewAnimationOptionTransitionCrossDissolve;
  else if (dire == 0)
    opt = UIViewAnimationOptionTransitionNone;
  [self transitionFrom:sceneCur toScene:scene option:opt];
  return scene;
}

- (void)transitionFrom:(UIViewController *)fromScene  toScene:(UIViewController *)toScene option:(UIViewAnimationOptions)option {
  if (toScene == nil)
    return;
  toScene.view.hidden = FALSE;

	if ([fromScene isEqual:toScene]) {
    scenePrev = sceneCur;
	  sceneCur = toScene;
		return;
  }
  
  toScene.view.frame = containerView.bounds;
  toScene.view.autoresizingMask = containerView.autoresizingMask;
  if (fromScene == nil) {
    [self addChildViewController:toScene];
    [containerView addSubview:toScene.view];
    [toScene didMoveToParentViewController:self];
    scenePrev = sceneCur;
	  sceneCur = toScene;
	  return;
  }
  
  // notify
  [self addChildViewController:fromScene];
  [self addChildViewController:toScene];
  [fromScene willMoveToParentViewController:nil];
  fromScene.view.hidden = TRUE;
  
  // transition
  [self transitionFromViewController:fromScene
                    toViewController:toScene
                            duration:DURATION
                             options:option		// flip?
																							// UIViewAnimationOptionTransitionFlipFromLeft:
                                    					// UIViewAnimationOptionTransitionFlipFromRight
                          animations:^{ }
                          completion:^(BOOL finished) {
                            [toScene didMoveToParentViewController:self];
                            [fromScene removeFromParentViewController];
                          }];
  scenePrev = sceneCur;
  sceneCur = toScene;
  flip = !flip;
}

- (UIViewController *)getCurrentScene {
  return sceneCur;
}

- (UIViewController *)getSceneInStory:(NSString *)story sceneName:(NSString *)sceneName {
  @try {
    BOOL isRootScene = (sceneName==nil || sceneName.length<=0 || [@"-" isEqual:sceneName]);
    NSString *key = [NSString stringWithFormat:@"%@:%@", story, (isRootScene? @"-": sceneName)];
//    BarItemViewController *scene = (BarItemViewController *)[sceneMap objectForKey:key];
    UIViewController *scene = (UIViewController *)[sceneMap objectForKey:key];
    if (scene != nil)
      return scene;
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:story bundle:nil];
    if (storyboard == nil)
      return nil;
    scene = isRootScene?
      [storyboard instantiateInitialViewController]:
      [storyboard instantiateViewControllerWithIdentifier:sceneName];
    if (scene == nil)
      return nil;
    [sceneMap setObject:scene forKey:key];
    return scene;
  } @catch(NSException* e) {
//  	NSLog(@"Exception=>%@ reason %@", [e name], [e reason]);
  	return nil;
  }
}

- (UIViewController *)getRootSceneInStory:(NSString *)story {
  return [self getSceneInStory:story sceneName:nil];
}

- (BOOL)shouldAutorotate {
  return NO;
}

@end

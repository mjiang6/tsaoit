//
//  NSDate+Helper.h
//  itip
//
//  Created by Michael Jiang on 4/23/2013.
//  Copyright (c) 2013 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDate (Helper)

- (int)secNum;
- (int)minNum;
- (int)hourNum;
- (NSString *)sec;
- (NSString *)min;
- (NSString *)hour;
- (NSString *)time:(NSString *)format;
- (int)dayNum;
- (NSString *)day;
- (int)weekNum;
- (NSString *)week:(NSString *)format;
- (int)monthNum;
- (NSString *)month:(NSString *)format;
- (int)yearNum;
- (NSString *)year;

- (NSDate *)addDays:(int)days;

+ (NSDate* )toDate:(NSString *)tm fmt:(NSString *)fmt;


@end

//
//  BaseViewController.m
//
//  Created by Michael Jiang on 9/7/2012.
//  Copyright (c) 2012 CSRA. All rights reserved.
//

#import "BaseViewController.h"
#import "RootViewController.h"

// #define USE_CURL
// #define USE_FLIP

@interface BaseViewController ()

@end

@implementation BaseViewController
@synthesize story;

- (UIViewController *)transitionToRoot {
  UIViewController *scene = [[RootViewController singleton] getRootSceneInStory:story];
#ifdef USE_CURL
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCurlDown];
#elif USE_FLIP
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionFlipFromTop];
#else
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCrossDissolve];
#endif
  return scene;
}

- (UIViewController *)transitionToScene:(NSString *)sceneName {
  UIViewController *scene = [[RootViewController singleton] getSceneInStory:story sceneName:sceneName];
#ifdef USE_CURL
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCurlUp];
#elif USE_FLIP
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionFlipFromBottom];
#else
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCrossDissolve];
#endif
  return scene;
}
/*
- (UIViewController *)toScene:(UIViewController *)scene
{
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionNone];
  return scene;
}
*/
- (UIViewController *)transitionBackToScene:(NSString *)sceneName {
  UIViewController *scene = [[RootViewController singleton] getSceneInStory:story sceneName:sceneName];
#ifdef USE_CURL
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCurlDown];
#elif USE_FLIP
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionFlipFromTop];
#else
  [[RootViewController singleton] transitionFrom:self toScene:scene option:UIViewAnimationOptionTransitionCrossDissolve];
#endif
  return scene;
}

- (UIViewController *)transitionBack {
  return [self transitionBack:SWING_ALT];
}

- (UIViewController *)transitionBack:(int)dire {
  return [[RootViewController singleton] openPrevScene:dire];
}

- (UIViewController *)openStory:(NSString *)myStory {
  return [[RootViewController singleton] openStory:myStory dire:SWING_BACK];
}

- (UIViewController *)openStory:(NSString *)myStory dire:(int)dire {
  return [[RootViewController singleton] openStory:myStory dire:dire];
}

- (UIViewController *)getScene:(NSString *)sceneName {
  return [[RootViewController singleton] getSceneInStory:story sceneName:sceneName];
}
/*
- (UIViewController *)getCurrentScene
{
  return [[RootViewController singleton] getCurrentScene];
}
*/
- (UIViewController *)getSceneInStory:(NSString *)sto sceneName:(NSString *)sceneName {
  return [[RootViewController singleton] getSceneInStory:sto sceneName:sceneName];
}

- (UIViewController *)getRootSceneInStory:(NSString *)sto {
  return [[RootViewController singleton] getRootSceneInStory:sto];
}

- (void)keepStoryName:(NSString *)storyName {
  self.story = storyName;
}

- (NSString *)findStoryName {
  return self.story;
}

-(void) reset {
}

-(void) refresh {
}

-(void) closeScene {
  
}

-(BOOL)isTransitioning {
  return FALSE;
}

- (void)viewDidUnload {
  [super viewDidUnload];
  [self setStory:nil];
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation {
//  return NO;
//  return (interfaceOrientation == UIInterfaceOrientationPortrait);
  return (interfaceOrientation == UIDeviceOrientationLandscapeRight);
}


@end

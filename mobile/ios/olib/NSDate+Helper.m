//
//  NSDate+Helper.m
//  itip
//
//  Created by Michael Jiang on 4/23/2013.
//  Copyright (c) 2013 CSRA. All rights reserved.
//

#import "NSDate+Helper.h"

@implementation NSDate (Helper)


- (NSString *)sec {
  return [self time:@"ss"];
}

- (NSString *)min {
  return [self time:@"mm"];
}

- (NSString *)hour {
  return [self time:@"hh"];
}

- (int)secNum {
  NSString *ss = [self sec];
  return [ss intValue];
}

- (int)minNum {
  NSString *ss = [self min];
  return [ss intValue];
}

- (int)hourNum {
  NSString *ss = [self hour];
  return [ss intValue];
}

- (NSString *)time:(NSString *)format {
  NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
  [formatter setDateFormat:format];
  return [formatter stringFromDate:self];
}

- (int)dayNum {
  return [[self day] intValue];
}

- (NSString *)day {
  return [self time:@"dd"];
}

- (int)weekNum {
  NSCalendar *gregorian = [[NSCalendar alloc] initWithCalendarIdentifier:NSCalendarIdentifierGregorian];
  NSDateComponents *weekdayComponents =[gregorian components:NSCalendarUnitWeekday fromDate:self];
  NSInteger weekday = [weekdayComponents weekday];
  // weekday 1 = Sunday for Gregorian calendar
  return (int)weekday;
}

- (NSString *)week:(NSString *)format {
  NSString *fmt = @"";
  for (int k=0; k<format.length; k++) {
    char ch = [format characterAtIndex:k];
    if (ch == 'E')
      fmt = [fmt stringByAppendingString:@"E"];
  }
  return [self time:fmt];
}

- (int)monthNum {
  return [[self time:@"MM"] intValue];
}

- (NSString *)month:(NSString *)format {
  NSString *fmt = @"";
  for (int k=0; k<format.length; k++) {
    char ch = [format characterAtIndex:k];
    if (ch == 'M')
      fmt = [fmt stringByAppendingString:@"M"];
  }
  return [self time:fmt];
}

- (int)yearNum {
  return [[self year] intValue];
}

- (NSString *)year {
  return [self time:@"yyyy"];
}

- (NSDate *)addDays:(int)days {
  NSDateComponents *dayComponent = [[NSDateComponents alloc] init];
  dayComponent.day = days;
  NSCalendar *theCalendar = [NSCalendar currentCalendar];
  return [theCalendar dateByAddingComponents:dayComponent toDate:self options:0];
}

+ (NSDate* )toDate:(NSString *)tm fmt:(NSString *)fmt {
  NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
  [dateFormatter setDateFormat:fmt];
  return [dateFormatter dateFromString:tm];
}

@end

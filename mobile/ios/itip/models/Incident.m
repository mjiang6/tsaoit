//
//  Incident.m
//  itip
//
//  Created by Michael Jiang on 12/2/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import "Incident.h"


@interface Incident () {
  Severity _severity;
}

@end


@implementation Incident
@synthesize name;
@synthesize description;
@synthesize type;
@synthesize incidentDate;
@synthesize siteCode;
@synthesize address;
@synthesize longitude;
@synthesize latitude;

- (id)init {
  return [self initWithIncidentName:nil severity:@"Low" siteCode:nil time:[NSDate date]];
}

- (id)initWithIncidentName:(NSString *)nm severity:(NSString *)level siteCode:(NSString *)code time:(NSDate *)time {
  if ((self=[super init]) == nil)
    return nil;
  self.name = nm;
  self.siteCode = code;
  self.incidentDate = time? time: [NSDate date];
  _severity = [Incident toSeverity:level];
  self.address = @"601 12th St S, Arlington, VA 22202";
  self.longitude = @"-77.057148671073492";
  self.latitude = @"38.862748222553535";
  self.type = @"Initial";
  return self;
}

- (Severity)severity {
  if ([@"Initial" isEqualToString:self.type])
    return SeverityUnknown;
  return _severity;
}

+ (id)incident {
  return [[Incident alloc] init];
}

+ (id)incidentWithIncidentName:(NSString *)name severity:(NSString *)level siteCode:(NSString *)code time:(NSDate *)time {
  return [[Incident alloc] initWithIncidentName:name severity:level siteCode:code time:time];
}

+ (Severity)toSeverity:(NSString *)level {
  Severity severity = SeverityUnknown;
  if ([@"High" isEqualToString:level] || [@"S1" isEqualToString:level])
    severity = SeverityHigh;
  else if ([@"Medium" isEqualToString:level] || [@"S2" isEqualToString:level])
    severity = SeverityMedium;
  else if ([@"Low" isEqualToString:level] || [@"S3" isEqualToString:level])
    severity = SeverityLow;
  return severity;
}

@end

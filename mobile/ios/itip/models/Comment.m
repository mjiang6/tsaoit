//
//  Comment.m
//  itip
//
//  Created by Michael Jiang on 12/14/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import "Comment.h"
#import "NSDate+Helper.h"

@implementation Comment
@synthesize commentId;
@synthesize text;
@synthesize type;
@synthesize username;
@synthesize createdTime;

- (id)init {
  return [self initWithJson:nil];
}

- (id)initWithJson:(NSDictionary *)json {
  if ((self=[super init]) == nil)
    return nil;
  self.commentId = [json objectForKey:@"_id"];;
  self.text = [json objectForKey:@"text"];
  self.type = [json objectForKey:@"type"];
  self.username = [json objectForKey:@"username"];
  self.createdTime = [NSDate toDate:[json valueForKey:@"created_time"] fmt:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
  return self;
}

+ (id)comment {
  return [[Comment alloc] init];
}

+ (id)commentWithJson:(NSDictionary *)json {
  return [[Comment alloc] initWithJson:json];
}


@end

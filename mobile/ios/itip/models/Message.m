//
//  Message.m
//  itip
//
//  Created by Michael Jiang on 12/11/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import "Message.h"
#import "NSDate+Helper.h"

@implementation Message
@synthesize message;
@synthesize messageId;
@synthesize incidentTitle;
@synthesize incidentId;
@synthesize date;
@synthesize status;



- (id)init {
  return [self initWithJson:nil];
}

- (id)initWithJson:(NSDictionary *)json {
  if ((self=[super init]) == nil)
    return nil;
  self.message = [json objectForKey:@"message"];
  self.messageId = [json objectForKey:@"messageId"];;
  self.incidentTitle = [json objectForKey:@"incidentTitle"];
  self.incidentId = [json objectForKey:@"incidentId"];
  self.date = [NSDate toDate:[json valueForKey:@"create_time"] fmt:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"];
  return self;
}

+ (id)message {
  return [[Message alloc] init];
}

+ (id)messageWithJson:(NSDictionary *)json {
  return [[Message alloc] initWithJson:json];
}


@end

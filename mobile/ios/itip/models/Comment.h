//
//  Comment.h
//  itip
//
//  Created by Michael Jiang on 12/14/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Comment : NSObject
@property (nonatomic, strong)   NSString *commentId;
@property (nonatomic, strong)   NSString *text;
@property (nonatomic, strong)   NSString *type;
@property (nonatomic, strong)   NSString *username;
@property (nonatomic, strong)   NSDate *createdTime;


- (id)initWithJson:(NSDictionary *)json;

+ (id)comment;
+ (id)commentWithJson:(NSDictionary *)json;


@end

//
//  Message.h
//  itip
//
//  Created by Michael Jiang on 12/11/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Message : NSObject
@property (nonatomic, strong) NSString *message;
@property (nonatomic, strong) NSString *messageId;
@property (nonatomic, strong) NSString *incidentTitle;
@property (nonatomic, strong) NSString *incidentId;
@property (nonatomic, strong) NSDate *date;
@property (nonatomic, strong) NSString *status;

- (id)initWithJson:(NSDictionary *)json;

+ (id)message;
+ (id)messageWithJson:(NSDictionary *)json;

@end


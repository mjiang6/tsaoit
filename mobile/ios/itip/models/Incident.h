//
//  Outage.h
//  itip
//
//  Created by Michael Jiang on 12/2/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>


typedef enum Severity {
  SeverityHigh,
  SeverityMedium,
  SeverityLow,
  SeverityUnknown
} Severity;

@interface Incident : NSObject
@property (nonatomic, strong) NSString *name;
@property (nonatomic, strong) NSString *description;
@property (nonatomic, strong) NSString *type;
@property (nonatomic        ) Severity severity;
@property (nonatomic, strong) NSDate *incidentDate;
@property (nonatomic, strong) NSString *siteCode;
@property (nonatomic, strong) NSString *address;
@property (nonatomic, strong) NSString *longitude;
@property (nonatomic, strong) NSString *latitude;

- (id)initWithIncidentName:(NSString *)name severity:(NSString *)level siteCode:(NSString *)code time:(NSDate *)time;

- (Severity)severity;

+ (id)incident;
+ (id)incidentWithIncidentName:(NSString *)name severity:(NSString *)level siteCode:(NSString *)code time:(NSDate *)time;

+ (Severity)toSeverity:(NSString *)level;

@end

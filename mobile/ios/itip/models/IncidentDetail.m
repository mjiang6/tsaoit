//
//  OutageDetail.m
//  itip
//
//  Created by Michael Jiang on 12/2/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import "IncidentDetail.h"
#import "Incident.h"
#import "NSDate+Helper.h"

@interface IncidentDetail () {
  NSMutableArray *_comments;
}

@end

@implementation IncidentDetail
@synthesize incidentId;
@synthesize incident;
@synthesize reportedDate;
@synthesize completed;
@synthesize showMap;


-(id)init {
  return [self initWithIncident:[Incident incident] incidentId:@"unknown" reportedDate:nil];
}


- (id)initWithIncident:(Incident*)incdnt incidentId:(NSString *)incdntId reportedDate:(NSDate *)repDate {
  if ((self=[super init]) == nil)
    return nil;
  self.incident = incdnt;
  self.incidentId = incdntId;
  self.reportedDate = repDate? repDate: [NSDate date];
  self.completed = NO;
  self.showMap = NO;
  _comments = [NSMutableArray array];
  return self;
}

+ (id)IncidentDetail {
  return [[IncidentDetail alloc] init];
}

+ (id)IncidentDetailWithIncident:(Incident*)incident incidentId:(NSString *)incidentId reportedDate:(NSDate *)repDate {
  return [[IncidentDetail alloc] initWithIncident:incident incidentId:incidentId reportedDate:repDate];
}

- (NSArray *)comments {
  return _comments;
}

- (void)addComment:(Comment *)comment {
  [_comments addObject:comment];
}

@end

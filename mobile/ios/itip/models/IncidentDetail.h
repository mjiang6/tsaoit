//
//  IncidentDetail.h
//  itip
//
//  Created by Michael Jiang on 12/2/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>

@class Incident;
@class Comment;
@interface IncidentDetail : NSObject
@property (nonatomic, strong)   NSString *incidentId;
@property (nonatomic, strong)   Incident *incident;
@property (nonatomic, strong)   NSDate *reportedDate;
@property (nonatomic, readonly) NSArray *comments;
@property (nonatomic)           BOOL completed;
@property (nonatomic)           BOOL showMap;

- (void)addComment:(Comment *)comment;
- (id)initWithIncident:(Incident*)incident incidentId:(NSString *)incidentId reportedDate:(NSDate *)reportedDate;

+ (id)IncidentDetail;
+ (id)IncidentDetailWithIncident:(Incident*)incident incidentId:(NSString *)incidentId reportedDate:(NSDate *)reportedDate;


@end

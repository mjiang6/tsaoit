//
//  AppDelegate.m
//  itip
//
//  Created by Jose Manners on 10/19/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "AppDelegate.h"
#import "MainViewController.h"
#import "Incident.h"
#import "ItipService.h"


#ifndef NDEBUG
	#define USE_LOCAL
// #define USE_LOCAL_IP
#else
//	#define USE_LOCAL_IP
#endif


@interface AppDelegate () <ItipServiceDelegate> {
  NSMutableDictionary *thumbImageMap;
  NSMutableDictionary *fullImageMap;
  ItipService *itipWS;
  NSString *baseUrl;
  BOOL assigned;
}

@end

@implementation AppDelegate
@synthesize alert;
@synthesize deviceToken;
@synthesize deviceUuid;
@synthesize userId;
@synthesize screenRect;
@synthesize alertsIsOn;


- (NSString *)wsBaseUrl {
  if ([baseUrl length] > 0)
    return baseUrl;
  NSString *path = [[NSBundle mainBundle] bundlePath];
  NSString *finalPath = [path stringByAppendingPathComponent:@"Info.plist"];
  NSDictionary *plistDictionary = [NSDictionary dictionaryWithContentsOfFile:finalPath];
  baseUrl = [plistDictionary objectForKey:@"WS_BASE_URL"];

#if defined (USE_LOCAL)
    baseUrl = @"http://localhost:9000/api";
#elif defined (USE_LOCAL_IP)
    baseUrl = @"http://192.168.0.107:9000/api";
#endif

  return baseUrl;
}

- (BOOL)application:(UIApplication *)application willFinishLaunchingWithOptions:(nullable NSDictionary *)launchOptions NS_AVAILABLE_IOS(6_0) {
  self.imgScreen = [AppDelegate screenshot];
  return YES;
}


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
  
  NSLog(@"Tsaoit server URL: %@", self.wsBaseUrl);
  
  thumbImageMap = [NSMutableDictionary dictionary];
  [thumbImageMap setObject:[UIImage imageNamed: @"alert-icon-redx1.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityHigh]];
  [thumbImageMap setObject:[UIImage imageNamed: @"alert-icon-yellowx1.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityMedium]];
  [thumbImageMap setObject:[UIImage imageNamed: @"alert-icon-greenx1.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityLow]];
  [thumbImageMap setObject:[UIImage imageNamed: @"alert-icon-grayx1.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityUnknown]];
  fullImageMap = [NSMutableDictionary dictionary];
  [fullImageMap setObject:[UIImage imageNamed: @"alert-icon-redx2.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityHigh]];
  [fullImageMap setObject:[UIImage imageNamed: @"alert-icon-yellowx2.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityMedium]];
  [fullImageMap setObject:[UIImage imageNamed: @"alert-icon-greenx2.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityLow]];
  [fullImageMap setObject:[UIImage imageNamed: @"alert-icon-grayx2.png"] forKey:[NSString stringWithFormat:@"%d", (int)SeverityUnknown]];
  
  
	itipWS = [[ItipService alloc] initWithBaseUrl:self.wsBaseUrl];
 	itipWS.timeoutInSeconds = 30;
 	itipWS.delegate = self;

  // Let the device know we want to receive push notifications
  UIUserNotificationSettings *settings = [UIUserNotificationSettings
                                          settingsForTypes:UIUserNotificationTypeBadge |
                                          UIUserNotificationTypeSound |
                                          UIUserNotificationTypeAlert
                                          categories:nil];
  [[UIApplication sharedApplication] registerUserNotificationSettings:settings];
  [[UIApplication sharedApplication] registerForRemoteNotifications];
  
  self.alert = nil;
  self.alertsIsOn = NO;
  application.applicationIconBadgeNumber = 0;
  
  if (launchOptions == nil) {
#ifndef NDEBUG
    NSLog(@"[AppDelegate] launchOptions is [nil]...");
#endif
    return YES;
  }
  NSDictionary* notif = [launchOptions objectForKey:UIApplicationLaunchOptionsRemoteNotificationKey];
  if (notif == nil)
  return YES;
#ifndef NDEBUG
  NSLog(@"[AppDelegate] Launched with push notification: %@", notif);
#endif
  NSDictionary *apsInfo = [notif objectForKey:@"aps"];
  self.alert = [apsInfo objectForKey:@"alert"];
#ifndef NDEBUG
  NSLog(@"[AppDelegate] Received Push Alert: %@", self.alert);
#endif
  
    return YES;
}

- (void)application:(UIApplication*)application didRegisterForRemoteNotificationsWithDeviceToken:(NSData*)dvcToken {
  NSMutableString *token = [NSMutableString string];
  // iterate through the bytes and convert to hex
  unsigned char *ptr = (unsigned char *)[dvcToken bytes];
  for (NSInteger i=0; i<[dvcToken length]; i++) {
    [token appendString:[NSString stringWithFormat:@"%02x", ptr[i]]];
  }
  self.deviceToken = [token copy];
#ifndef NDEBUG
  NSLog(@"[AppDelegate token] is: \"%@\"", self.deviceToken);
#endif
  UIDevice *dev = [UIDevice currentDevice];
  self.deviceUuid = dev.identifierForVendor.UUIDString;
#ifndef NDEBUG
  NSLog(@"[AppDelegate deviceUuid] is: \"%@\"", self.deviceUuid);
#endif
#ifndef NDEBUG
  [itipWS registerDevice:dev.name deviceUuid:self.deviceUuid deviceToken:self.deviceToken channel:APNSDevelopment];
#else
  [itipWS registerDevice:dev.name deviceUuid:self.deviceUuid deviceToken:self.deviceToken channel:APNSProduction];
#endif
}

// application:didReceiveRemoteNotification:fetchCompletionHandler:

- (void)application:(UIApplication*)application didReceiveRemoteNotification:(NSDictionary*)userInfo {
#ifndef NDEBUG
  NSLog(@"[AppDelegate] Received notification: %@", [userInfo description]);
#endif
  NSDictionary *apsInfo = [userInfo objectForKey:@"aps"];
  self.alert = [apsInfo objectForKey:@"alert"];
#ifndef NDEBUG
  NSLog(@"Received Push Alert: %@", self.alert);
#endif
  MainViewController *mainView = (MainViewController *)self.window.rootViewController;
  [mainView receivedAlert:self.alert];
}

- (void)application:(UIApplication*)application didFailToRegisterForRemoteNotificationsWithError:(NSError*)error {
  NSLog(@"[AppDelegate] Failed to get token, error: %@", error);
  self.deviceToken = nil;
}

- (UIImage *)thumbImage:(int)severityType {
 	NSString *severity = [NSString stringWithFormat:@"%d", severityType];
  return [thumbImageMap objectForKey:severity];
}

- (UIImage *)fullImage:(int)severityType {
  NSString *severity = [NSString stringWithFormat:@"%d", severityType];
 	return [fullImageMap objectForKey:severity];
}


+ (UIImage*)screenshot {
  // Create a graphics context with the target size
  // On iOS 4 and later, use UIGraphicsBeginImageContextWithOptions to take the scale into consideration
  // On iOS prior to 4, fall back to use UIGraphicsBeginImageContext
  CGSize imageSize = [[UIScreen mainScreen] bounds].size;
  UIGraphicsBeginImageContextWithOptions(imageSize, NO, 0);
  CGContextRef context = UIGraphicsGetCurrentContext();
  
  // Iterate over every window from back to front
  for (UIWindow *window in [[UIApplication sharedApplication] windows]) {
    if (![window respondsToSelector:@selector(screen)] || [window screen] == [UIScreen mainScreen]) {
      // -renderInContext: renders in the coordinate space of the layer,
      // so we must first apply the layer's geometry to the graphics context
      CGContextSaveGState(context);
      // Center the context around the window's anchor point
      CGContextTranslateCTM(context, [window center].x, [window center].y);
      // Apply the window's transform about the anchor point
      CGContextConcatCTM(context, [window transform]);
      // Offset by the portion of the bounds left of and above the anchor point
      CGContextTranslateCTM(context,
                            -[window bounds].size.width * [[window layer] anchorPoint].x,
                            -[window bounds].size.height * [[window layer] anchorPoint].y);
      
      // Render the layer hierarchy to the current context
      [[window layer] renderInContext:context];
      
      // Restore the context
      CGContextRestoreGState(context);
    }
  }
  
  // Retrieve the screenshot image
  UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
  UIGraphicsEndImageContext();
  return image;
}

- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
  MainViewController *mainView = (MainViewController *)self.window.rootViewController;
  [mainView logout];
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    // Saves changes in the application's managed object context before the application terminates.
}

- (NSURL *)applicationDocumentsDirectory {
    // The directory the application uses to store the Core Data store file. This code uses a directory named "csc.itip" in the application's documents directory.
    return [[[NSFileManager defaultManager] URLsForDirectory:NSDocumentDirectory inDomains:NSUserDomainMask] lastObject];
}

#pragma mark -
#pragma mark ItipServiceDelegate delegates

- (void)svc:(ItipService *)svc didRegisterDevice:(NSString *)status {
#ifndef NDEBUG
  NSLog(@"[AppDelegate] didRegisterDevice: %@", status);
#endif
}

- (void)svc:(ItipService *)svc didFailWithError:(NSError *)error method:(NSString *)method{
#ifndef NDEBUG
  NSLog(@"[AppDelegate: [ItipService didFailWithError] %@]: Error: %@", method, [error description]);
#endif
	if ([@"registerDevice" isEqualToString:method]) {
  	self.alert = [[error userInfo] objectForKey:NSLocalizedDescriptionKey];
    MainViewController *mainView = (MainViewController *)self.window.rootViewController;
  	[mainView receivedAlert:self.alert];
  }
}

@end

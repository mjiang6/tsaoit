//
//  ItipService.m
//  itip
//
//  Created by Michael Jiang on 12/1/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import "Incident.h"
#import "IncidentDetail.h"
#import "Comment.h"
#import "NSDate+Helper.h"
#import "ItipService.h"

#ifndef NDEBUG
//  #define USE_TEST
#endif

typedef enum ReqType {
  ReqRegister,
  ReqLogin,
  ReqLogout,
  ReqLoadIncidentList,
  ReqLoadIncident,
  ReqLoadNotificationList,
  ReqUnknown
} ReqType;


@interface ItipService () {
  NSURLConnection *m_conn;
  NSMutableData *m_data;
  NSString *m_code;
  NSString *m_arg;
  NSString *m_userId;
  NSString *m_pwd;
  NSString *m_baseUrl;
  ReqType m_reqType;
  int m_statusCode;
}

@end


@implementation ItipService
@synthesize status;
@synthesize timeoutInSeconds;
@synthesize delegate;

- (id)init {
  return [self initWithBaseUrl:nil];
}

- (id)initWithBaseUrl:(NSString *)baseUrl {
  if ((self=[super init]) == nil)
    return nil;
  self.baseUrl = baseUrl;
  self.timeoutInSeconds = 60;
  self.status = @"INITED";
  m_data = [[NSMutableData alloc] init];
  m_code = nil;
  m_conn = nil;
  m_statusCode = 600;
  m_reqType = ReqUnknown;
  return self;
}

- (NSString *)baseUrl {
  return m_baseUrl;
}

- (void)setBaseUrl:(NSString *)baseUrl {
  if ([baseUrl hasSuffix:@"/"])
    m_baseUrl = [baseUrl substringWithRange:NSMakeRange(0, (baseUrl.length-1))];
  else
    m_baseUrl = [baseUrl copy];
}

- (int)statusCode {
  return m_statusCode;
}

- (void)cancelConnection {
  [m_conn cancel];
  m_conn = nil;
}

- (void)setStatusCode:(int)sttCode {
  m_statusCode = sttCode;
  if (200 <= m_statusCode && m_statusCode < 300)
    self.status = @"RES_SUCCEED";
  else if (m_statusCode == 401)
    self.status = @"NOT_AUTHENTICATED";
  else if (m_statusCode == 403)
    self.status = @"NO_PERMISSION";
  else if (m_statusCode == 404)
    self.status = @"NOT_FOUND";
  else if (m_statusCode == 408 || m_statusCode == 504)
    self.status = @"TIMED_OUT";
  else
    self.status = @"SERVER_ERROR";
}

- (NSString *) toMethod:(ReqType)reqType {
  if (reqType == ReqRegister)
    return @"registerDevice";
  if (reqType == ReqLogin)
    return @"authenticate";
  if (reqType == ReqLogout)
    return @"logout";
  if (reqType == ReqLoadIncidentList)
    return @"loadIncidentList";
  if (reqType == ReqLoadIncident)
    return @"loadIncident";
  if (reqType == ReqLoadNotificationList)
    return @"loadNotificationList";
  return @"unknown";
}


- (void)registerDevice: (NSString *)name deviceUuid:(NSString *)uuid deviceToken:(NSString *)token channel:(APNSChannel)chnl {
  m_reqType = ReqRegister;
  [self cancelConnection];
  [m_data setLength:0];
  m_code = [token copy];
  m_arg = [uuid copy];
  NSString *json = [NSString stringWithFormat:@"{\"name\":\"%@\",\"token\":\"%@\",\"uuid\":\"%@\",\"owner\":\"\",\"channel\":\"%@\"}",
                    name, token, uuid, chnl==APNSDevelopment? @"sandbox": @"production"];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/devices/", self.baseUrl]];
  NSMutableDictionary* headers = [NSMutableDictionary dictionary];
  [headers setValue:@"application/json" forKey:@"Content-Type"];
  [headers setValue:@"no-cache" forKey:@"Cache-Control"];
  [headers setValue:@"no-cache" forKey:@"Pragma"];
  [headers setValue:@"0" forKey:@"Expires"];				// no cache
  NSMutableURLRequest* request = [NSMutableURLRequest
                                  requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:self.timeoutInSeconds];
  [request setHTTPMethod:@"POST"];
  [request setAllHTTPHeaderFields:headers];
  [request setHTTPBody:[json dataUsingEncoding:NSUTF8StringEncoding]];
  m_conn = [NSURLConnection connectionWithRequest:request delegate:self];
#ifndef NDEBUG
  NSLog(@"[ItipService registerDevice]: Registering device[%@] to '%@' submitted via %@ channel...",
        token,
        [url absoluteString],
        chnl==APNSDevelopment? @"Sandbox": @"Production");
#endif
}

- (void)authenticate: (NSString *)userId password:(NSString *)password deviceToken:(NSString *)token {
  m_reqType = ReqLogin;
  m_userId = [userId copy];
  m_code = [token copy];
  m_arg = [userId copy];
  [self cancelConnection];
  [m_data setLength:0];
  NSString *json = [NSString stringWithFormat:@"{\"username\":\"%@\", \"password\":\"%@\", \"token\":\"%@\"}", m_userId, password, m_code];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/auth", self.baseUrl]];
  NSMutableDictionary* headers = [NSMutableDictionary dictionary];
  [headers setValue:@"application/json" forKey:@"Content-Type"];
  [headers setValue:@"no-store, no-cache, must-revalidate, private, max-age=0" forKey:@"Cache-Control"];
  [headers setValue:@"private" forKey:@"Pragma"];
  [headers setValue:@"0" forKey:@"Expires"];
  NSMutableURLRequest* request = [NSMutableURLRequest
                                  requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:self.timeoutInSeconds];
  [request setHTTPMethod:@"POST"];
  [request setAllHTTPHeaderFields:headers];
  [request setHTTPBody:[json dataUsingEncoding:NSUTF8StringEncoding]];
  m_conn = [NSURLConnection connectionWithRequest:request delegate:self];
#ifndef NDEBUG
  NSLog(@"[ItipService authenticate]: Assign device[%@] to '%@'...", m_code, m_userId);
#endif
}

- (void)logout {
  [self.delegate svc:self didLogout:m_userId];
}

- (void)loadIncidentList: (NSString *)filter {
  m_reqType = ReqLoadIncidentList;
  m_code = [filter copy];
#ifdef USE_TEST
  NSURL *url = [[NSBundle mainBundle] URLForResource:@"incidents" withExtension:@"json"];
  NSData *data = [NSData dataWithContentsOfURL:url];
  if (!NSClassFromString(@"NSJSONSerialization")) {
    [self.delegate svc:self didFailWithError:[NSError errorWithDomain:@"Error: NSJSONSerialization not available."
                                                                 code:500
                                                             userInfo:nil] method:@"loadIncidentList"];
    return;
  }
  NSError *err = nil;
  id object = [NSJSONSerialization
               JSONObjectWithData:data
               options:0
               error:&err];
  if (err) {
    [self.delegate svc:self didFailWithError:err method:@"loadIncidentList"];
    return;
  }
  if ([object isKindOfClass:[NSArray class]]) {
    NSArray *incidents = object;
    [self.delegate svc:self didLoadIncidentList:incidents];
  } else {
    [self.delegate svc:self didFailWithError:[NSError errorWithDomain:@"Error: Not JSON type."
                                                                 code:500
                                                             userInfo:nil] method:@"loadIncidentList"];
  }
#else
  [self cancelConnection];
  [m_data setLength:0];
  NSString *param = @"";
  if (filter && ![@"all" isEqualToString:[filter lowercaseString]])
    param = [NSString stringWithFormat:@"?filter=%@", filter];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/incidents%@", self.baseUrl, param]];
  NSMutableDictionary* headers = [NSMutableDictionary dictionary];
  [headers setValue:@"no-store, no-cache, must-revalidate, private, max-age=0" forKey:@"Cache-Control"];
  [headers setValue:@"private" forKey:@"Pragma"];
  [headers setValue:@"0" forKey:@"Expires"];
  NSMutableURLRequest *request = [NSMutableURLRequest
                                  requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:self.timeoutInSeconds];
  [request setHTTPShouldHandleCookies:YES];
  [request setHTTPMethod:@"GET"];
  [request setAllHTTPHeaderFields:headers];
  m_conn = [NSURLConnection connectionWithRequest:request delegate:self];
#endif
}

- (void)loadIncident: (NSString *)incidentId {
  m_reqType = ReqLoadIncident;
  m_code = [incidentId copy];
#ifdef USE_TEST
#else
  [self cancelConnection];
  [m_data setLength:0];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/incidents", self.baseUrl]];
  NSMutableDictionary* headers = [NSMutableDictionary dictionary];
  [headers setValue:@"no-store, no-cache, must-revalidate, private, max-age=0" forKey:@"Cache-Control"];
  [headers setValue:@"private" forKey:@"Pragma"];
  [headers setValue:@"0" forKey:@"Expires"];
  NSMutableURLRequest *request = [NSMutableURLRequest
                                  requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:self.timeoutInSeconds];
  [request setHTTPShouldHandleCookies:YES];
  [request setHTTPMethod:@"GET"];
  [request setAllHTTPHeaderFields:headers];
  m_conn = [NSURLConnection connectionWithRequest:request delegate:self];
#endif
}

- (void)loadNotificationList {
  m_reqType = ReqLoadNotificationList;
  m_code = nil;
  [self cancelConnection];
  [m_data setLength:0];
  NSURL *url = [NSURL URLWithString:[NSString stringWithFormat:@"%@/pns/messages", self.baseUrl]];
  NSMutableDictionary* headers = [NSMutableDictionary dictionary];
  [headers setValue:@"no-store, no-cache, must-revalidate, private, max-age=0" forKey:@"Cache-Control"];
  [headers setValue:@"private" forKey:@"Pragma"];
  [headers setValue:@"0" forKey:@"Expires"];
  NSMutableURLRequest *request = [NSMutableURLRequest
                                  requestWithURL:url
                                  cachePolicy:NSURLRequestUseProtocolCachePolicy
                                  timeoutInterval:self.timeoutInSeconds];
  [request setHTTPShouldHandleCookies:YES];
  [request setHTTPMethod:@"GET"];
  [request setAllHTTPHeaderFields:headers];
  m_conn = [NSURLConnection connectionWithRequest:request delegate:self];
}

+ (IncidentDetail *)jsonToIncidentDetail:(NSDictionary *)incident {
  NSDictionary *severity = [incident valueForKey:@"severity"];
  NSDictionary *site = [incident valueForKey:@"site_code"];
  NSDictionary *itype = [incident valueForKey:@"incident_type"];
  Incident *incdnt = [Incident incidentWithIncidentName:[incident valueForKey:@"name"]
                                               severity:[severity valueForKey:@"level"]
                                               siteCode:[site valueForKey:@"code"]
                                                   time:[NSDate toDate:[incident valueForKey:@"incident_time"] fmt:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]];
  incdnt.description = [incident valueForKey:@"description"];
  incdnt.type = [itype valueForKey:@"description"];
  if ([@"Initial" isEqualToString:incdnt.type])
    incdnt.siteCode = nil;
  incdnt.address = [NSString stringWithFormat:@"%@, %@, %@ %@", [site valueForKey:@"address"], [site valueForKey:@"city"], [site valueForKey:@"state"], [site valueForKey:@"zipCode"]];
  incdnt.longitude = [site valueForKey:@"longitude"];
  incdnt.latitude = [site valueForKey:@"latitude"];
  IncidentDetail *detail = [IncidentDetail IncidentDetailWithIncident:incdnt
                                                           incidentId:[incident valueForKey:@"_id"]
                                                         reportedDate:[NSDate toDate:[incident valueForKey:@"reported_time"] fmt:@"yyyy-MM-dd'T'HH:mm:ss.SSSZ"]];
  detail.completed = ((BOOL)[incident valueForKey:@"completed"] == YES);
  NSArray *comments = [incident valueForKey:@"comments"];
  for (NSDictionary *cmnt in comments)
    [detail addComment:[Comment commentWithJson:cmnt]];

  return detail;
}

#pragma mark -
#pragma mark NSURLConnection delegate methods

- (void)connection:(NSURLConnection *)connection didReceiveResponse:(NSURLResponse *)response {
  self.statusCode = (int)[((NSHTTPURLResponse*)response) statusCode];
  [m_data setLength:0];
  if (m_reqType == ReqLogout) {
    if ([self.delegate respondsToSelector:@selector(svc:didLogout:)])
      [self.delegate svc:self didLogout:m_code];
    return;
  }
  if (m_reqType != ReqLogin)
    return;
  
  if (self.statusCode == 200)
    return;
  
#ifndef NDEBUG
  NSLog(@"[ItipService authenticate] Invalid credentail for user: %@.", m_code);
#endif
  self.statusCode = 401;
  if ([self.delegate respondsToSelector:@selector(svc:didAuthenticate:userId:deviceToken:)])
    [self.delegate svc:self didAuthenticate:NO userId:m_userId deviceToken:m_code];
}

- (void)connection:(NSURLConnection *)connection didReceiveData:(NSData *)data {
  [m_data appendData:data];
}

- (void)connection:(NSURLConnection *)connection didFailWithError:(NSError *)error {
 	[self cancelConnection];
 	[m_data setLength:0];
  if (m_reqType != ReqLogin) {
#ifndef NDEBUG
    NSLog(@"[ItipService didFailWithError] Error: %@", [error description]);
#endif
    [self.delegate svc:self didFailWithError:error method:[self toMethod:(int)m_reqType]];
    return;
  }
#ifndef NDEBUG
  NSLog(@"[ItipService authenticate] didFailWithError(%@): %@", m_code, [error description]);
#endif
  self.statusCode = 401;
  if ([self.delegate respondsToSelector:@selector(svc:didAuthenticate:userId:deviceToken:)])
    [self.delegate svc:self didAuthenticate:NO userId:m_userId deviceToken:m_code];
}

- (BOOL)connection:(NSURLConnection *)connection canAuthenticateAgainstProtectionSpace:(NSURLProtectionSpace *)protectionSpace {
#ifndef NDEBUG
  NSLog(@"canAuthenticateAgainstProtectionSpace");
#endif
  if ([[protectionSpace authenticationMethod] isEqualToString:NSURLAuthenticationMethodServerTrust]) {
    // Note: this is presently only called once per server (or URL?) until
    //       you restart the app
#ifndef NDEBUG
    NSURL *url = [[connection currentRequest] URL];
    NSLog(@"Authentication checking for %@", url);
#endif
    return YES; // Self-signed cert will be accepted
    // Note: it doesn't seem to matter what you return for a proper SSL cert
    //       only self-signed certs
  }
  return NO;
}

- (void)connection:(NSURLConnection *)connection didReceiveAuthenticationChallenge:(NSURLAuthenticationChallenge *)challenge {
#ifndef NDEBUG
  NSLog(@"didReceiveAuthenticationChallenge");
#endif
  if ([challenge.protectionSpace.authenticationMethod isEqualToString:NSURLAuthenticationMethodServerTrust]) {
    [challenge.sender useCredential:[NSURLCredential credentialForTrust:challenge.protectionSpace.serverTrust] forAuthenticationChallenge:challenge];
#ifndef NDEBUG
    NSLog(@"chalenging protection space authentication checking");
#endif
  }
}

- (void)connectionDidFinishLoading:(NSURLConnection *)connection {
 	[self cancelConnection];
#ifndef NDEBUG
  NSLog(@"[ItipService connectionDidFinishLoading] Data(%@) loaded with length=%d for method %@()", m_code, (int)m_data.length, [self toMethod:m_reqType]);
#endif
 	@autoreleasepool {
    if (m_reqType == ReqLogout) {
#ifndef NDEBUG
      NSLog(@"[ItipService connectionDidFinishLoading] logout(%@) ...", m_code);
#endif
      if ([self.delegate respondsToSelector:@selector(svc:didAuthenticate:userId:deviceToken:)])
        [self.delegate svc:self didAuthenticate:NO userId:m_userId deviceToken:m_code];
    } else if (self.statusCode == 401) {
#ifndef NDEBUG
        NSLog(@"[ItipService connectionDidFinishLoading] Authentication(%@) Failed.", m_code);
#endif
      if ([self.delegate respondsToSelector:@selector(svc:didAuthenticate:userId:deviceToken:)])
        [self.delegate svc:self didAuthenticate:NO userId:m_userId deviceToken:m_code];
    } else if (self.statusCode == 404) {
#ifndef NDEBUG
      NSLog(@"[ItipService connectionDidFinishLoading] Error(%@) STATUS_CODE: %d", m_code, self.statusCode);
#endif
      NSError* error = [NSError errorWithDomain:m_code
                                           code:self.statusCode
                                       userInfo:nil];
      [self.delegate svc:self didFailWithError:error method:[self toMethod:m_reqType]];
    } else if (self.statusCode >= 400) {
#ifndef NDEBUG
      NSLog(@"[ItipService connectionDidFinishLoading] Error(%@) STATUS_CODE: %d", m_code, self.statusCode);
#endif
      NSError* error = [NSError errorWithDomain:
                        [[NSString alloc] initWithData:m_data
                                              encoding:NSUTF8StringEncoding]
                                           code:self.statusCode
                                       userInfo:nil];
      [self.delegate svc:self didFailWithError:error method:[self toMethod:m_reqType]];
    } else if (m_reqType == ReqRegister) {
      if ([self.delegate respondsToSelector:@selector(svc:didRegisterDevice:)])
        [self.delegate svc:self didRegisterDevice:@"GOOD"];
    } else if (m_reqType == ReqLogin) {
#ifndef NDEBUG
      NSLog(@"[ItipService connectionDidFinishLoading] Session(%@) authenticated.", m_code);
#endif
      if ([self.delegate respondsToSelector:@selector(svc:didAuthenticate:userId:deviceToken:)])
        [self.delegate svc:self didAuthenticate:YES userId:m_userId deviceToken:m_code];
    } else if (m_reqType == ReqLoadIncidentList) {
      if ([self.delegate respondsToSelector:@selector(svc:didLoadIncidentList:)]) {
        NSError *err = nil;
        id object = [NSJSONSerialization JSONObjectWithData:m_data options:0 error:&err];
        if (err) {
          [self.delegate svc:self didFailWithError:err method:@"loadIncidentList"];
        } else {
          if ([object isKindOfClass:[NSArray class]]) {
            NSArray *incidents = object;
            [self.delegate svc:self didLoadIncidentList:incidents];
          } else {
            [self.delegate svc:self didFailWithError:[NSError errorWithDomain:@"Error: Not JSON type." code:500 userInfo:nil] method:@"loadIncidentList"];
          }
        }
      }
    } else if (m_reqType == ReqLoadIncident) {
      if ([self.delegate respondsToSelector:@selector(svc:didLoadIncident:)]) {
        NSError *err = nil;
        id object = [NSJSONSerialization JSONObjectWithData:m_data options:0 error:&err];
        if (err) {
          [self.delegate svc:self didFailWithError:err method:@"loadIncidentList"];
        } else {
          if ([object isKindOfClass:[NSDictionary class]]) {
            IncidentDetail *detail = [ItipService jsonToIncidentDetail:object];
            [self.delegate svc:self didLoadIncident:detail];
          } else {
            [self.delegate svc:self didFailWithError:[NSError errorWithDomain:@"Error: Not JSON type." code:500 userInfo:nil] method:@"loadIncident"];
          }
        }
      }
    } else if (m_reqType == ReqLoadNotificationList) {
      if ([self.delegate respondsToSelector:@selector(svc:didLoadNotificationList:)]) {
        NSError *err = nil;
        id object = [NSJSONSerialization JSONObjectWithData:m_data options:0 error:&err];
        if (err) {
          [self.delegate svc:self didFailWithError:err method:@"loadNotificationList"];
        } else {
          if ([object isKindOfClass:[NSArray class]]) {
            NSArray *notifications = object;
            [self.delegate svc:self didLoadNotificationList:notifications];
          } else {
            [self.delegate svc:self didFailWithError:[NSError errorWithDomain:@"Error: Not JSON type." code:500 userInfo:nil] method:@"loadNotificationList"];
          }
        }
      }
    } else {
#ifndef NDEBUG
      NSLog(@"[ItipService connectionDidFinishLoading] Unknown RequestType.");
#endif
    }
    [m_data setLength:0];
 	}
}


@end

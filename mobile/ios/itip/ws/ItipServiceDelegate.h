//
//  ItipServiceDelegate.h
//  itip
//
//  Created by Michael Jiang on 12/1/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <UIKit/UIKit.h>

@class IncidentDetail;
@class ItipService;
@protocol ItipServiceDelegate <NSObject>

@optional
- (void)svc:(ItipService *)svc didAuthenticate:(BOOL)loggedIn userId:(NSString *)userId deviceToken:(NSString*)token;
- (void)svc:(ItipService *)svc didLogout:(NSString*)userId;
- (void)svc:(ItipService *)svc didRegisterDevice:(NSString *)status;
- (void)svc:(ItipService *)svc didLoadIncidentList:(NSArray *)incidentList;
- (void)svc:(ItipService *)svc didLoadIncident:(IncidentDetail *)detail;
- (void)svc:(ItipService *)svc didLoadNotificationList:(NSArray *)notificationList;

@required
- (void)svc:(ItipService *)svc didFailWithError:(NSError *)error method:(NSString *)method;

@end

//
//  ItipService.h
//  itip
//
//  Created by Michael Jiang on 12/1/2015.
//  Copyright © 2015 CSRA. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ItipServiceDelegate.h"

typedef enum APNSChannel {
  APNSDevelopment,
  APNSProduction
} APNSChannel;

@class IncidentDetail;

@interface ItipService : NSObject <NSURLConnectionDelegate, NSURLConnectionDataDelegate> {
}

@property (nonatomic        ) int timeoutInSeconds;
@property (nonatomic, strong) NSString *baseUrl;
@property (nonatomic, strong) NSString *status;
@property (nonatomic,   weak) id<ItipServiceDelegate> delegate;

- (id)initWithBaseUrl: (NSString *)baseUrl;
- (int)statusCode;
- (void)registerDevice: (NSString *)name deviceUuid:(NSString *)uuid deviceToken:(NSString *)token channel:(APNSChannel)chnl;
- (void)authenticate: (NSString *)userId password:(NSString *)password deviceToken:(NSString *)token;
- (void)logout;
- (void)loadIncidentList: (NSString *)filter;
- (void)loadIncident: (NSString *)incidentId;
- (void)loadNotificationList;

- (void)cancelConnection;

+ (IncidentDetail *)jsonToIncidentDetail:(NSDictionary *)incident;

@end

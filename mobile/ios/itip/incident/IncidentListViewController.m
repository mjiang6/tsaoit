//
//  IncidentListViewController.m
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "IncidentListViewController.h"
#import "IncidentViewController.h"
#import "IncidentCell.h"
#import "AppDelegate.h"
#import "Incident.h"
#import "IncidentDetail.h"
#import "ItipService.h"
#import "NSDate+Helper.h"
#import "ModalMaker.h"
#import "ChartService.h"

#ifndef NDEBUG
// #define USE_LOCAL
#define USE_LOCAL_IP
#endif

@interface IncidentListViewController () <UITableViewDelegate, UITableViewDataSource, ItipServiceDelegate, ChartViewDelegate> {
  NSMutableArray *incidents;
  ItipService *itipWS;
  ChartService *chartHelper;
  UIImage *imgScreen;
  BOOL collapsed;
}

@end

@implementation IncidentListViewController

- (id)initWithCoder:(NSCoder*)coder {
  if ((self=[super initWithCoder:coder]) == nil)
    return self;
  
  incidents = [NSMutableArray array];
  itipWS = [[ItipService alloc] init];
  itipWS.timeoutInSeconds = 30;
  itipWS.delegate = self;
  chartHelper = [[ChartService alloc] init];
  collapsed = NO;
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  itipWS.baseUrl = app.wsBaseUrl;
  // Initialize Chart

/*
  [ModalMaker makeModalView:self.btnSortOccured border:0];
  [ModalMaker makeModalView:self.btnSortSeverity border:0];
  [ModalMaker makeModalView:self.btnSortSite border:0];
  [ModalMaker makeModalView:self.btnFilterAll border:0];
  [ModalMaker makeModalView:self.btnFilterLow border:0];
  [ModalMaker makeModalView:self.btnFilterMedium border:0];
  [ModalMaker makeModalView:self.btnFilterHigh border:0];
*/
  
	[ChartService initializeChart:self.vwBarChart];
  self.vwBarChart.delegate = self;
  self.txtTimePeriod.text = @"Time Period";
  [self closeCommand];
}

- (void)viewDidAppear:(BOOL)animated {
  [super viewDidAppear:animated];
  [self initialize];
}

- (void)initialize {
  if (collapsed)
    self.vwFilterTop.constant = 0;
  else
    self.vwFilterTop.constant = self.vwChartPanel.frame.size.height;
}

- (void)sort {
  if ([incidents count] <= 0)
    return;
  NSArray *sorted = [incidents sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
    IncidentDetail *detail1 = a; IncidentDetail *detail2 = b;
    if ([@"Date" isEqualToString:self.txtSort.text])
      return [detail2.incident.incidentDate compare:detail1.incident.incidentDate];
    if ([@"Severity" isEqualToString:self.txtSort.text]) {
      if (detail1.incident.severity == detail2.incident.severity)
        return NSOrderedSame;
      if (detail1.incident.severity == SeverityHigh)
        return NSOrderedAscending;
      if (detail1.incident.severity == SeverityLow)
        return NSOrderedDescending;
      return detail2.incident.severity == SeverityLow? NSOrderedAscending: NSOrderedDescending;
    }
    return [detail1.incident.siteCode compare:detail2.incident.siteCode];
  }];
  [incidents removeAllObjects];
  for (IncidentDetail *cd in sorted)
    [incidents addObject:cd];
}

- (void)load {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  [itipWS authenticate:app.userId password:nil deviceToken:app.deviceToken];
}

- (void)closeCommand {
  [self showSorted:NO];
  [self showFilter:NO];
  self.vwCommand.hidden = YES;
}

- (void)showSorted:(BOOL)showIt {
  self.btnSortOccured.hidden = !showIt;
  self.btnSortSeverity.hidden = !showIt;
  self.btnSortSite.hidden = !showIt;
}

- (void)showFilter:(BOOL)showIt {
  self.btnFilterAll.hidden = !showIt;
  self.btnFilterLow.hidden = !showIt;
  self.btnFilterMedium.hidden = !showIt;
  self.btnFilterHigh.hidden = !showIt;
  self.imgFilterLow.hidden = !showIt;
  self.imgFilterMedium.hidden = !showIt;
  self.imgFilterHigh.hidden = !showIt;
}


#pragma mark -
#pragma mark Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:NO];
/*
  UIImage *img = nil;
  if (imgScreen == nil)
  	img = [AppDelegate screenshot];
*/
  IncidentDetail *detail = [incidents objectAtIndex:indexPath.row];
  IncidentViewController *scene = (IncidentViewController *)[self openStory:@"IncidentStoryboard" dire:CROSS_DISSOLVE];
  [scene load:detail imgScreen:nil];
/*
  if (imgScreen == nil) {
    dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_BACKGROUND, 0), ^{
      AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
      double scale = (double)CGImageGetWidth([img CGImage]) / app.screenRect.size.width;
      CGRect rtScr = CGRectMake(scale*app.screenRect.origin.x,
                                scale*app.screenRect.origin.y,
                                scale*app.screenRect.size.width,
                                scale*app.screenRect.size.height);
      CGImageRef imgRef = CGImageCreateWithImageInRect([img CGImage], rtScr);
      imgScreen = [UIImage imageWithCGImage:imgRef];
      CGImageRelease(imgRef);
      dispatch_async(dispatch_get_main_queue(), ^{
        scene.imgBackdrop.image = imgScreen;
      });
    });
  }
*/
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return incidents.count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  IncidentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];
//  [ModalMaker makeModalView:cell];
  IncidentDetail *detail = [incidents objectAtIndex:indexPath.row];
  cell.txtIncident.text = detail.incident.name;
  cell.txtType.text = detail.incident.type;
  cell.imgAlert.image = [app thumbImage:detail.incident.severity];
  cell.txtCel.text = [detail.incident.incidentDate time:@"MM/dd/yyyy"];
  cell.txtSite.text = detail.incident.siteCode;
  return cell;
}

- (IBAction)doSort:(id)sender {
  self.vwCommand.hidden = NO;
  [self showSorted:YES];
  [self showFilter:NO];
}

- (IBAction)doFilter:(id)sender {
  self.vwCommand.hidden = NO;
  [self showFilter:YES];
  [self showSorted:NO];
}

- (IBAction)onLeftClick:(id)sender {
  self.vwBarChart.data = [chartHelper translateIncidentData:incidents forCurrentMonth:NO advanceForward:NO];
  self.txtTimePeriod.text = [chartHelper getCurrentlyDisplayedPeriod];
}

- (IBAction)onRightClick:(id)sender {
  self.vwBarChart.data = [chartHelper translateIncidentData:incidents forCurrentMonth:NO advanceForward:YES];
  self.txtTimePeriod.text = [chartHelper getCurrentlyDisplayedPeriod];
}

- (IBAction)onCollapse:(id)sender {
  if (collapsed) {
    [UIView animateWithDuration:0.5
                     animations:^{
                       self.vwFilterTop.constant = self.vwChartPanel.frame.size.height;
                       self.vwChartPanel.alpha = 1;
                     }
                     completion:^(BOOL finished) {
                       self.imgFlap.image = [UIImage imageNamed:@"collapse.png"];
                     }];
  } else {
    [UIView animateWithDuration:0.5
                     animations:^{
                       self.vwChartPanel.alpha = 0;
                     }
                     completion:^(BOOL finished) {
                       self.vwFilterTop.constant = 0;
                       self.imgFlap.image = [UIImage imageNamed:@"dropdown.png"];
                     }];
  }
  collapsed = !collapsed;
}

- (IBAction)onFilterLow:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"S3";
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (IBAction)onFilterMedium:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"S2";
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (IBAction)onFilterHigh:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"S1";
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (IBAction)onFilterActive:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"Open";
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (IBAction)onFilterInitial:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"Initial";
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (IBAction)onFilterAll:(id)sender {
  [self closeCommand];
  self.txtFilter.text = @"All";
  [itipWS loadIncidentList:nil];
  
/*
  dispatch_time_t popTime = dispatch_time(DISPATCH_TIME_NOW, 0.1 * NSEC_PER_SEC);
  dispatch_after(popTime, dispatch_get_main_queue(), ^(void){
    self.txtFilter.text = @"All";
  });
*/
}

- (IBAction)onCloseCommand:(id)sender {
  [self closeCommand];
}

- (IBAction)onSortOccured:(id)sender {
  [self closeCommand];
  self.txtSort.text = @"Date";
  [self sort];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)onSortSeverity:(id)sender {
  [self closeCommand];
  self.txtSort.text = @"Severity";
  [self sort];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (IBAction)onSortSite:(id)sender {
  [self closeCommand];
  self.txtSort.text = @"Site Code";
  [self sort];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

#pragma mark -
#pragma mark ItipServiceDelegate delegates

- (void)svc:(ItipService *)svc didLoadIncidentList:(NSArray *)incidentList {
  [incidents removeAllObjects];
  for (NSDictionary *obj in incidentList) {
    IncidentDetail *detail = [ItipService jsonToIncidentDetail:obj];
    [incidents addObject:detail];
  }
  self.vwBarChart.data = [chartHelper translateIncidentData:incidents forCurrentMonth:YES advanceForward:NO];
  self.txtTimePeriod.text = [chartHelper getCurrentlyDisplayedPeriod];
  [self sort];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)svc:(ItipService *)svc didAuthenticate:(BOOL)loggedIn userId:(NSString *)userId deviceToken:(NSString *)token {
#ifndef NDEBUG
  NSLog(@"[IncidentListViewController: [ItipService didAuthenticate]]: Username: %@, Token: %@", userId, token);
#endif
  [itipWS loadIncidentList:self.txtFilter.text];
}

- (void)svc:(ItipService *)svc didFailWithError:(NSError *)error method:(NSString *)method{
#ifndef NDEBUG
  NSLog(@"[IncidentListViewController: [ItipService didFailWithError] %@]: Error: %@", method, [error description]);
#endif
}

@end

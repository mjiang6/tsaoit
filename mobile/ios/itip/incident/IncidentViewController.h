//
//  IncidentViewController.h
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "BaseViewController.h"
#import "IncidentDetail.h"

@interface IncidentViewController : BaseViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgBackdrop;
@property (weak, nonatomic) IBOutlet UILabel *blured;
@property (weak, nonatomic) IBOutlet UIImageView *imgAlert;
@property (weak, nonatomic) IBOutlet UILabel *txtIncident;
@property (weak, nonatomic) IBOutlet UILabel *txtType;
@property (weak, nonatomic) IBOutlet UITableView *tblDetails;

- (IBAction)onBack:(id)sender;
- (IBAction)onViewMap:(id)sender;

- (void)load:(IncidentDetail *)detail imgScreen:(UIImage *)imgScreen;

@end

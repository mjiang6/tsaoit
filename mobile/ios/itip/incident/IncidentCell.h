//
//  IncidentCell.h
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface IncidentCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtType;
@property (weak, nonatomic) IBOutlet UIImageView *imgAlert;
@property (weak, nonatomic) IBOutlet UILabel *txtIncident;
@property (weak, nonatomic) IBOutlet UILabel *txtCel;
@property (weak, nonatomic) IBOutlet UILabel *txtSite;

@end

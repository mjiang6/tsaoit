//
//  DetailCell.h
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@interface DetailCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtOcuredDate;
@property (weak, nonatomic) IBOutlet UILabel *txtOcuredTime;
@property (weak, nonatomic) IBOutlet UILabel *txtReportedDate;
@property (weak, nonatomic) IBOutlet UILabel *txtReportedTime;
@property (weak, nonatomic) IBOutlet UILabel *txtSite;
@property (weak, nonatomic) IBOutlet UITextView *txtComment;
@property (weak, nonatomic) IBOutlet MKMapView *vwMap;
@property (weak, nonatomic) IBOutlet UIImageView *imgViewMap;
@property (weak, nonatomic) IBOutlet UIButton *btnViewMap;

@end

//
//  DetailCell.m
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "DetailCell.h"

@implementation DetailCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
  if ((self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) == nil)
    return nil;
  // Initialization code
  return self;
}

/*
- (id)initWithCoder:(NSCoder*)coder {
  if ((self=[super initWithCoder:coder]) == nil)
    return nil;
  UIView *backView = [[UIView alloc] init];
  backView.backgroundColor = [UIColor colorWithRed:0.5 green:0 blue:0.3 alpha:0.6];
  self.selectedBackgroundView = backView;
  return self;
}
*/

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end

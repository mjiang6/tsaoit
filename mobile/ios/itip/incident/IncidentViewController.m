//
//  IncidentViewController.m
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <CoreLocation/CoreLocation.h>
#import "AppDelegate.h"
#import "IncidentViewController.h"
#import "IncidentListViewController.h"
#import "Incident.h"
#import "Comment.h"
#import "DetailCell.h"
#import "MessageCell.h"
#import "NSDate+Helper.h"
#import "ModalMaker.h"

#define METERS_PER_MILES 1609.34

@interface IncidentViewController () <UITableViewDelegate, UITableViewDataSource, CLLocationManagerDelegate, MKMapViewDelegate> {
  CLLocationManager *locationManager;
  IncidentDetail *detail;
  int count;
}

@end

@implementation IncidentViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  count = 0;
  locationManager = [[CLLocationManager alloc] init];
  [locationManager setDelegate:self];
  [locationManager setDistanceFilter:kCLDistanceFilterNone];
  [locationManager setDesiredAccuracy:kCLLocationAccuracyBest];
  [locationManager requestWhenInUseAuthorization];
  [locationManager requestAlwaysAuthorization];
}

- (void)load:(IncidentDetail *)dtl imgScreen:(UIImage *)imgScreen {
  detail = dtl;
  detail.showMap = NO;
  count = (int)([[detail comments] count] + 1);
  [self.tblDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
  if (imgScreen)
	  self.imgBackdrop.image = imgScreen;
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  self.txtIncident.text = detail.incident.name;
  self.txtType.text = detail.incident.type;
  self.imgAlert.image = [app thumbImage:detail.incident.severity];
}

- (IBAction)onBack:(id)sender {
  detail = nil;
  count = 0;
  [self.tblDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
  IncidentListViewController *scene = (IncidentListViewController *)[self openStory:@"IncidentListStoryboard" dire:CROSS_DISSOLVE];
  [scene load];
}

- (IBAction)onViewMap:(id)sender {
  detail.showMap = !detail.showMap;
  count = (int)([[detail comments] count] + 1);
  [self.tblDetails reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationNone];
}

#pragma mark -
#pragma mark Table view delegates

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row == 0)
    return detail.showMap? 600: 380;
  else
    return 140;
}

- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return count;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  if (indexPath.row == 0) {
	  DetailCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell" forIndexPath:indexPath];
    cell.txtOcuredDate.text = [detail.incident.incidentDate time:@"MM/dd/yyyy"];
    cell.txtOcuredTime.text = [detail.incident.incidentDate time:@"HH:mm"];
    cell.txtReportedDate.text = [detail.reportedDate time:@"MM/dd/yyyy"];
    cell.txtReportedTime.text = [detail.reportedDate time:@"HH:mm"];
    cell.txtSite.text = detail.incident.siteCode;
    cell.txtComment.text = detail.incident.description;
    [cell.vwMap removeAnnotations:cell.vwMap.annotations];
    if ([@"Initial" isEqualToString:detail.incident.type]) {
      cell.btnViewMap.hidden = YES;
      cell.imgViewMap.hidden = YES;
      return cell;
    } else {
      cell.btnViewMap.hidden = NO;
      cell.imgViewMap.hidden = NO;
    }
    cell.vwMap.hidden = !detail.showMap;
    if (cell.vwMap.hidden) {
	    cell.imgViewMap.image = [UIImage imageNamed:@"dropdown.png"];
    } else {
      cell.imgViewMap.image = [UIImage imageNamed:@"collapse.png"];
      CLGeocoder *geocoder = [[CLGeocoder alloc] init];
      [geocoder geocodeAddressString:detail.incident.address completionHandler:^(NSArray* placemarks, NSError* error) {
        if ([placemarks count] > 0) {
           CLPlacemark *topResult = [placemarks firstObject];
           MKPlacemark *placemark = [[MKPlacemark alloc] initWithPlacemark:topResult];
           MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
           annotation.coordinate = placemark.coordinate;
           annotation.title = [@"Site Code: " stringByAppendingString:detail.incident.siteCode];
           annotation.subtitle = detail.incident.address;
           [cell.vwMap addAnnotation:annotation];
           MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(placemark.coordinate, 10.0*METERS_PER_MILES, 10.0*METERS_PER_MILES);
           [cell.vwMap setRegion:region animated:YES];
        }
      }];
/*
      CLLocationCoordinate2D location;
      location.latitude = [detail.incident.latitude doubleValue];
      location.longitude = [detail.incident.longitude doubleValue];
      MKPointAnnotation *annotation = [[MKPointAnnotation alloc] init];
      annotation.coordinate = location;
      annotation.title = detail.incident.siteCode;
      annotation.subtitle = @"Incident Site";
      [cell.vwMap addAnnotation:annotation];
      MKCoordinateRegion region = MKCoordinateRegionMakeWithDistance(location, 10.0*METERS_PER_MILES, 10.0*METERS_PER_MILES);
      [cell.vwMap setRegion:region animated:YES];
*/
    }
    return cell;
  }
  Comment *comment = [[detail comments] count]>0? [detail.comments objectAtIndex:(indexPath.row - 1)]: nil;
  MessageCell *cell = [tableView dequeueReusableCellWithIdentifier:@"msgCell" forIndexPath:indexPath];
  cell.txtMonth.text = [comment.createdTime month:@"MMM"];
  cell.txtDay.text = [comment.createdTime time:@"d"];
  cell.txtYear.text = [comment.createdTime year];
  cell.txtTime.text = [comment.createdTime time:@"hh:mm aaa"];
  cell.txtUpdatedBy.text = [@"Updated by: " stringByAppendingString:comment.username? comment.username: @"Unknown"];
  cell.txtTitle.text = [@"update" isEqualToString:comment.type]? @"Comment updated": @"New comment added";
  cell.txtMessage.text = comment.text;
  return cell;
}


@end

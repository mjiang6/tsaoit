//
//  IncidentListViewController.h
//  itip
//
//  Created by Michael Jiang on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "BaseViewController.h"
#import "Charts/Charts-Swift.h"

@interface IncidentListViewController : BaseViewController
@property (weak, nonatomic) IBOutlet BarChartView *vwBarChart;
@property (weak, nonatomic) IBOutlet UITableView *tblList;
@property (weak, nonatomic) IBOutlet UIButton *btnSortBy;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterBy;
@property (weak, nonatomic) IBOutlet UILabel *txtTimePeriod;
@property (weak, nonatomic) IBOutlet UIView *vwChartPanel;
@property (weak, nonatomic) IBOutlet UIImageView *imgFlap;
@property (weak, nonatomic) IBOutlet UIView *vwFilter;
@property (weak, nonatomic) IBOutlet UIView *vwCommand;

@property (weak, nonatomic) IBOutlet UILabel *txtFilter;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterLow;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilterLow;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterMedium;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilterMedium;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterHigh;
@property (weak, nonatomic) IBOutlet UIImageView *imgFilterHigh;
@property (weak, nonatomic) IBOutlet UIButton *btnFilterAll;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *vwFilterTop;
@property (weak, nonatomic) IBOutlet UILabel *txtSort;
@property (weak, nonatomic) IBOutlet UIButton *btnSortOccured;
@property (weak, nonatomic) IBOutlet UIButton *btnSortSeverity;
@property (weak, nonatomic) IBOutlet UIButton *btnSortSite;

- (IBAction)onCollapse:(id)sender;
- (IBAction)onLeftClick:(id)sender;
- (IBAction)onRightClick:(id)sender;

- (IBAction)doSort:(id)sender;
- (IBAction)doFilter:(id)sender;

- (IBAction)onCloseCommand:(id)sender;
- (IBAction)onFilterLow:(id)sender;
- (IBAction)onFilterMedium:(id)sender;
- (IBAction)onFilterHigh:(id)sender;
- (IBAction)onFilterActive:(id)sender;
- (IBAction)onFilterInitial:(id)sender;
- (IBAction)onFilterAll:(id)sender;
- (IBAction)onSortOccured:(id)sender;
- (IBAction)onSortSeverity:(id)sender;
- (IBAction)onSortSite:(id)sender;

- (void)load;
- (void)initialize;

@end

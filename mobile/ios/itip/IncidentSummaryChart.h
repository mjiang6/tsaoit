//
//  IncidentSummaryChart.h
//  itip
//
//  Created by Gabe Harms on 12/8/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <Charts/Charts.h>
#import "Charts/Charts-Swift.h"

@interface IncidentSummaryChart : BarChartView

@property (nonatomic) NSInteger showingYear;


@end

//
//  ChartService.h
//  itip
//
//  Created by Gabe Harms on 12/7/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Charts/Charts-Swift.h"

@interface ChartService : NSObject

+(BarChartView*)initializeChart:(BarChartView*)chart;
-(BarChartData*)translateIncidentData:(NSMutableArray*)incidents forCurrentMonth:(BOOL)forCurrentMonth advanceForward:(BOOL)advanceForward;
-(NSString*)getCurrentlyDisplayedPeriod;

@end

//
//  ChartService.m
//  itip
//
//  Created by Gabe Harms on 12/7/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "ChartService.h"
#import "Charts/Charts-Swift.h"
#import "Incident.h"
#import "IncidentDetail.h"

@implementation ChartService {
    NSArray* months;

    NSInteger currentlySelectedYear;        // Based on what year the chart is showing data for
    BOOL showingFirstHalfOfYear;             // Based on what half of the year the chart is showing data for
}

-(instancetype)init {
    self = [super init];
    months = @[@"Jan", @"Feb", @"Mar", @"Apr", @"May", @"Jun", @"Jul", @"Aug", @"Sep",@"Oct", @"Nov", @"Dec"];
    currentlySelectedYear = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]] year];
    return self;
}


+(BarChartView*)initializeChart:(BarChartView*)chart {
    chart.descriptionText = @"";
    chart.noDataTextDescription = @"You need to provide data for the chart.";
    
    chart.pinchZoomEnabled = NO;
    chart.drawBarShadowEnabled = NO;
    chart.drawGridBackgroundEnabled = NO;
    
    ChartLegend *legend = chart.legend;
    legend.position = ChartLegendPositionBelowChartCenter;
    legend.font = [UIFont fontWithName:@"HelveticaNeue-Light" size:11.f];
    legend.textColor = [UIColor colorWithRed:0.396078f green:0.40f blue:0.41176f alpha:1.0f];
    
    ChartXAxis *xAxis = chart.xAxis;
    xAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    xAxis.labelPosition = XAxisLabelPositionBottom;
    xAxis.labelTextColor = [UIColor colorWithRed:0.396078f green:0.40f blue:0.41176f alpha:1.0f];
    xAxis.drawGridLinesEnabled= NO;
    xAxis.drawAxisLineEnabled = NO;
    
    ChartYAxis *leftAxis = chart.leftAxis;
    leftAxis.labelFont = [UIFont fontWithName:@"HelveticaNeue-Light" size:10.f];
    leftAxis.valueFormatter = [[NSNumberFormatter alloc] init];
    leftAxis.valueFormatter.maximumFractionDigits = 3;
    leftAxis.drawGridLinesEnabled = YES;
    leftAxis.spaceTop = 0.40;
    leftAxis.labelTextColor = [UIColor colorWithRed:0.396078f green:0.40f blue:0.41176f alpha:1.0f];
    
    chart.rightAxis.enabled = NO;
    
    return chart;
}

-(BarChartData*)translateIncidentData:(NSMutableArray*)incidents forCurrentMonth:(BOOL)forCurrentMonth advanceForward:(BOOL)advanceForward {
    
    if (forCurrentMonth) {
        int currentMonth = (int)[[[NSCalendar currentCalendar] components:NSCalendarUnitDay | NSCalendarUnitMonth | NSCalendarUnitYear fromDate:[NSDate date]] month] -1;
        showingFirstHalfOfYear = (currentMonth <= 5);
    }
    else {
        if (advanceForward && !showingFirstHalfOfYear) {
            currentlySelectedYear++;
        }
        else if (!advanceForward && showingFirstHalfOfYear) {
            currentlySelectedYear--;
        }
        showingFirstHalfOfYear = !showingFirstHalfOfYear;
    }
    
    
    // Create array of x values
    NSArray * xVals = [self getXAxisLabels:showingFirstHalfOfYear];
    
    // Create 3 arrays of y data sets
    NSMutableArray * dataSets = [self getYChartData:xVals currentYear:currentlySelectedYear incidents:incidents];

    // Create BarChartData object from our data sets
    BarChartData *data = [[BarChartData alloc] initWithXVals:xVals dataSets:dataSets];
    data.groupSpace = 0.8;
    [data setDrawValues:NO];
    
    // Return BarChartData object
    return data;
}

-(NSMutableArray*)getYChartData:(NSArray*)xAxis currentYear:(BOOL)currentYear incidents:(NSMutableArray*)incidents {
    NSMutableArray *yLowSeverity = [[NSMutableArray alloc] init];
    NSMutableArray *yMediumSeverity = [[NSMutableArray alloc] init];
    NSMutableArray *yHighSeverity = [[NSMutableArray alloc] init];
    
    NSArray* severities = @[@"low", @"medium", @"high"];
    
    
    // Init Count Maps
    //      Structure: incidentCount[month][severityLevel] = The count of incidents that fall in ${month} and are of the ${severityLevel} severity level
    NSMutableDictionary* incidentCounts = [[NSMutableDictionary alloc] init];
    for (NSString* mon in xAxis) {
        [incidentCounts  setObject:[[NSMutableDictionary alloc] init] forKey:mon];
        for (NSString *severity in severities) {
            [[incidentCounts objectForKey:mon] setObject:[NSNumber numberWithInt:0]  forKey:severity];
        }
        
    }
    
    // For each incident, determine if it is within the month range and for the currently seleçted year that we are analyzing
    //  Then, if it is within our month range, determine which severity level within that month to increment
    for (IncidentDetail* incident in incidents) {
	      if ([@"Initial" isEqualToString:incident.incident.type]) // excludes initial incidents
  	      continue;

        NSString* validMonthFoundIn = [self isInMonthRange:incident.incident.incidentDate currentYear:currentlySelectedYear monthRange:xAxis];
        if (validMonthFoundIn != nil) { // If within month range...
            switch(incident.incident.severity) { // For each severity level, increment the count map
                case SeverityHigh: [[incidentCounts objectForKey:validMonthFoundIn] setObject:[NSNumber numberWithInt:[[[incidentCounts objectForKey:validMonthFoundIn] objectForKey:[severities objectAtIndex:2]] intValue]+1] forKey:[severities objectAtIndex:2]];
                    break;
                case SeverityMedium: [[incidentCounts objectForKey:validMonthFoundIn] setObject:[NSNumber numberWithInt:[[[incidentCounts objectForKey:validMonthFoundIn] objectForKey:[severities objectAtIndex:1]] intValue]+1] forKey:[severities objectAtIndex:1]];
                    break;
                case SeverityLow: [[incidentCounts objectForKey:validMonthFoundIn] setObject:[NSNumber numberWithInt:[[[incidentCounts objectForKey:validMonthFoundIn] objectForKey:[severities objectAtIndex:0]] intValue]+1] forKey:[severities objectAtIndex:0]];
                    break;
                default: break;
            }
        }
    }
    
    int i = 0;
    // Add the incident count of each severity level, of each month to the data sets
    for (NSString* mon in xAxis) {
        [yHighSeverity addObject:[[BarChartDataEntry alloc] initWithValue:(double)[[[incidentCounts objectForKey:mon] objectForKey:[severities objectAtIndex:2]] intValue] xIndex:i]];
        [yMediumSeverity addObject:[[BarChartDataEntry alloc] initWithValue:(double)[[[incidentCounts objectForKey:mon] objectForKey:[severities objectAtIndex:1]] intValue] xIndex:i]];
        [yLowSeverity addObject:[[BarChartDataEntry alloc] initWithValue:(double)[[[incidentCounts objectForKey:mon] objectForKey:[severities objectAtIndex:0]] intValue] xIndex:i]];
        i++;
    }

    // Create 3 BarChartDataSets using y value arrays
    BarChartDataSet *set1 = [[BarChartDataSet alloc] initWithYVals:yHighSeverity label:[severities objectAtIndex:2]];
    [set1 setColor:[UIColor colorWithRed:0.98823f green:0.415686f blue:0.33725f alpha:1.f]];
    BarChartDataSet *set2 = [[BarChartDataSet alloc] initWithYVals:yMediumSeverity label:[severities objectAtIndex:1]];
    [set2 setColor:[UIColor colorWithRed:1.0f green:0.7843f blue:0.39217f alpha:1.f]];
    BarChartDataSet *set3 =  [[BarChartDataSet alloc] initWithYVals:yLowSeverity label:[severities objectAtIndex:0]];
    [set3 setColor:[UIColor colorWithRed:0.58039f green:0.666667f blue:0.40784f alpha:1.f]];
    
    // Create Data Set array, and add each BarChartDataSet to it
    NSMutableArray *dataSets = [[NSMutableArray alloc] init];
    [dataSets addObject:set1];
    [dataSets addObject:set2];
    [dataSets addObject:set3];
    
    // Return an array BarChartDataSets objects
    return dataSets;
}

-(NSMutableArray*)getXAxisLabels:(BOOL)showFirstHalfOfYear {
    NSMutableArray *xVals = [[NSMutableArray alloc] init];
    int lowerMonth = (showingFirstHalfOfYear) ? 0 : 6;
    int upperMonth = (showingFirstHalfOfYear) ? 5 : 11;
    
    for (int i = lowerMonth; i <= upperMonth; i++)
    {
        [xVals addObject: months[i]];    // Get month string value
    }
    return xVals;
}

-(NSString*)isInMonthRange:(NSDate*)incidentDate currentYear:(NSInteger)currentYear monthRange:(NSArray*)monthRange {
    NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
    [formatter setDateFormat:@"MMM"];
    NSString* incidentMonth = [formatter stringFromDate:incidentDate];
    [formatter setDateFormat:@"yyyy"];
    NSString* incidentYear = [formatter stringFromDate:incidentDate];
    
    NSUInteger index = (incidentYear.integerValue == currentYear) ? [monthRange indexOfObject:incidentMonth] : NSNotFound; // If in year and month range, a valid integer will be returned
    
    return (index==NSNotFound) ? nil : [monthRange objectAtIndex:index];
}

-(NSString*)getCurrentlyDisplayedPeriod {
    NSMutableString *period = [[NSMutableString alloc] init];
    [period appendString:((showingFirstHalfOfYear) ? @"Jan - Jun " : @"Jul - Dec ")];
    [period appendString:[NSString stringWithFormat:@"%d", (int)currentlySelectedYear]];
    return period;
}


@end

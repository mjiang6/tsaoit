//
//  AppDelegate.h
//  itip
//
//  Created by Jose Manners on 10/19/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <CoreData/CoreData.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) NSString *alert;
@property (strong, nonatomic) NSString *deviceToken;
@property (strong, nonatomic) NSString *deviceUuid;
@property (strong, nonatomic) NSString *userId;
@property (strong, nonatomic) UIImage *imgScreen;
@property (        nonatomic) CGRect screenRect;
@property (        nonatomic) BOOL alertsIsOn;

- (NSString *)wsBaseUrl;
- (UIImage *)thumbImage:(int)severityType;
- (UIImage *)fullImage:(int)severityType;

- (NSURL *)applicationDocumentsDirectory;

+ (UIImage *)screenshot;

@end


//
//  MainViewController.h
//  itip
//
//  Created by Michael Jiang on 12/7/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "RootViewController.h"

@interface MainViewController : RootViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgBackdrop;
@property (weak, nonatomic) IBOutlet UIView *vwTitle;
@property (weak, nonatomic) IBOutlet UIView *vwContainer;
@property (weak, nonatomic) IBOutlet UIView *vwCommand;
@property (weak, nonatomic) IBOutlet UIView *blured;
@property (weak, nonatomic) IBOutlet UIView *vwLogin;
@property (weak, nonatomic) IBOutlet UITextField *txtUsername;
@property (weak, nonatomic) IBOutlet UITextField *txtPassword;
@property (weak, nonatomic) IBOutlet UIView *vwBadge;
@property (weak, nonatomic) IBOutlet UILabel *txtBadge;

- (IBAction)doLogin:(id)sender;
- (IBAction)doForgotPassword:(id)sender;
- (IBAction)doNewUser:(id)sender;
- (IBAction)doNotifications:(id)sender;

- (void)logout;
- (void)login;

- (void)clearAlerts;
- (void)receivedAlert:(NSString *)msg;
- (void)setBadge:(int)count;
- (void)clearBadge;


@end

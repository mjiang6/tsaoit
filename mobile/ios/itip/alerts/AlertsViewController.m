//
//  AlertsViewController.m
//  itip
//
//  Created by Michael Jiang on 12/11/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "AlertsViewController.h"
#import "AppDelegate.h"
#import "ItipService.h"
#import "ModalMaker.h"
#import "Message.h"
#import "AlertCell.h"
#import "NSDate+Helper.h"

@interface AlertsViewController () <UITableViewDelegate, UITableViewDataSource, ItipServiceDelegate> {
  NSMutableArray *msgList;
  ItipService *itipWS;
  NSInteger selectedIndex;
  int cnt;
}

@end

@implementation AlertsViewController

- (id)initWithCoder:(NSCoder*)coder {
  self = [super initWithCoder:coder];
  if (self == nil)
    return nil;
  msgList = [[NSMutableArray alloc] init];
  itipWS = [[ItipService alloc] init];
  itipWS.timeoutInSeconds = 30;
  itipWS.delegate = self;
  return self;
}

- (void)viewDidLoad {
  [super viewDidLoad];
  [ModalMaker makeModalView:self.pnlTable];
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  itipWS.baseUrl = app.wsBaseUrl;
}


- (void) loadWithImage:(UIImage *)image {
  if (image)
	  self.imgBackdrop.image = image;
  [msgList removeAllObjects];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
  [itipWS loadNotificationList];
}


- (IBAction)onClose:(id)sender {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  app.alertsIsOn = NO;
  [self transitionBack];
}


#pragma mark - Table view delegates
- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView {
  return 1;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
  return [msgList count];
}

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
  [tableView deselectRowAtIndexPath:indexPath animated:NO];
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
  AlertCell *cell =[tableView dequeueReusableCellWithIdentifier:@"Cell"];
  Message *msg = [msgList objectAtIndex:indexPath.row];
  cell.txtMessage.text = msg.message;
  cell.txtTime.text = [msg.date time:@"EEE, dd MMM yyyy HH:mm:ss"];
  cell.txtIncident.text = msg.incidentTitle;
  return cell;
}

#pragma mark -
#pragma mark ItipServiceDelegate delegates

- (void)svc:(ItipService *)svc didLoadNotificationList:(NSArray *)notifications {
  [msgList removeAllObjects];
  NSMutableArray *arr = [NSMutableArray array];
  for (NSDictionary *json in notifications) {
    Message *msg = [Message messageWithJson:json];
    [arr addObject:msg];
  }
  NSArray *sorted = [arr sortedArrayUsingComparator:^NSComparisonResult(id a, id b) {
    Message *m1 = a; Message *m2 = b;
    return [m2.date compare:m1.date];
  }];
  for (Message *m in sorted)
    [msgList addObject:m];
  [self.tblList reloadSections:[NSIndexSet indexSetWithIndex:0] withRowAnimation:UITableViewRowAnimationFade];
}

- (void)svc:(ItipService *)svc didFailWithError:(NSError *)error method:(NSString *)method{
#ifndef NDEBUG
  NSLog(@"[AlertsViewController: [ItipService didFailWithError] %@]: Error: %@", method, [error description]);
#endif
}

@end

//
//  AlertCell.h
//  itip
//
//  Created by Michael Jiang on 3/25/13.
//  Copyright (c) 2013 CSC. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AlertCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *txtMessage;
@property (weak, nonatomic) IBOutlet UILabel *txtIncident;
@property (weak, nonatomic) IBOutlet UILabel *txtTime;

@end

//
//  AlertsViewController.h
//  itip
//
//  Created by Michael Jiang on 12/11/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "BaseViewController.h"

@interface AlertsViewController : BaseViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgBackdrop;
@property (weak, nonatomic) IBOutlet UILabel *pnlTable;
@property (weak, nonatomic) IBOutlet UITableView *tblList;

- (IBAction)onClose:(id)sender;

- (void) loadWithImage:(UIImage *)image;

@end

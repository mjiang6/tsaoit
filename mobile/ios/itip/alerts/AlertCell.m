//
//  AlertCell.m
//  itip
//
//  Created by Michael Jiang on 3/25/13.
//  Copyright (c) 2013 CSC. All rights reserved.
//

#import "AlertCell.h"

@implementation AlertCell

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier {
	if ((self=[super initWithStyle:style reuseIdentifier:reuseIdentifier]) == nil)
    return nil;
  // Initialization code
  return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
	[super setSelected:selected animated:animated];
  // Configure the view for the selected state
}

@end

//
//  MainViewController.m
//  itip
//
//  Created by Michael Jiang on 12/7/15.
//  Copyright © 2015 CSC. All rights reserved.
//

#import "MainViewController.h"
#import "IncidentListViewController.h"
#import "AlertsViewController.h"
#import "AppDelegate.h"
#import "ModalMaker.h"

@interface MainViewController () <UITextFieldDelegate> {
  AlertsViewController *alertsScene;
}

@end

@implementation MainViewController

- (void)viewDidLoad {
  [super viewDidLoad];
  
//  self.imgScreen.image = [AppDelegate screenshot];
//  [ModalMaker makeModalView:self.blured style:UIBlurEffectStyleDark];
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  app.alertsIsOn = NO;
  [self setContainerView:self.vwContainer];
  [self clearAlerts];
  alertsScene = nil;
  [self setBadge:6];
}

- (void)viewDidAppear:(BOOL)animated {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  app.screenRect = self.vwContainer.frame;
}


- (IBAction)doLogin:(id)sender {
  [self login];
}

- (IBAction)doForgotPassword:(id)sender {
//  [self login];
}

- (IBAction)doNewUser:(id)sender {
//  [self login];
}

- (IBAction)doNotifications:(id)sender {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  if (app.alertsIsOn) {
    app.alertsIsOn = NO;
    [alertsScene onClose:self];
    alertsScene = nil;
    return;
  }
  app.alertsIsOn = YES;
  UIImage *img = [AppDelegate screenshot];
  CGRect rect = self.vwContainer.frame;
  double scale = (double)CGImageGetWidth([img CGImage]) / rect.size.width;
  CGRect rtScr = CGRectMake(scale*rect.origin.x,
                            scale*rect.origin.y,
                            scale*rect.size.width,
                            scale*rect.size.height);
  CGImageRef imgRef = CGImageCreateWithImageInRect([img CGImage], rtScr);
  UIImage *imgScreen = [UIImage imageWithCGImage:imgRef];
  CGImageRelease(imgRef);
  alertsScene = (AlertsViewController *)[self openStory:@"AlertsStoryboard" dire:CROSS_DISSOLVE];
  [alertsScene loadWithImage:imgScreen];
  [self clearAlerts];
}

- (void)logout {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  app.alertsIsOn = NO;
  self.blured.hidden = YES;
  self.vwLogin.hidden = NO;
  self.imgBackdrop.image = [UIImage imageNamed:@"eagle.png"];
}

- (void)login {
  self.blured.hidden = NO;
  self.vwLogin.hidden = YES;
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  app.userId = self.txtUsername.text;
  IncidentListViewController *scene = (IncidentListViewController *)[self openStory:@"IncidentListStoryboard" dire:CROSS_DISSOLVE];
  [scene load];
  self.imgBackdrop.image = [UIImage imageNamed:@"eagle-blured-more.png"];
}


- (void)receivedAlert:(NSString *)msg {
  AppDelegate *app = (AppDelegate *)[[UIApplication sharedApplication] delegate];
  if ([app.alert length] <= 0)
    return;
  app.alert = nil;
#ifndef NDEBUG
  NSLog(@"[MainViewController: receivedAlert] Alert: %@", app.alert);
#endif
}

- (void)clearAlerts {
  [self clearBadge];
}

- (void)setBadge:(int)count {
  self.txtBadge.text = [NSString stringWithFormat:@"%d", count];
  self.vwBadge.hidden = NO;
}

- (void)clearBadge {
  self.txtBadge.text = nil;
  self.vwBadge.hidden = YES;
}


#pragma mark -
#pragma mark TextField controller delegates
- (BOOL)textFieldShouldReturn:(UITextField *)textField {
  if (textField.returnKeyType == UIReturnKeyNext)
    [[self.view viewWithTag:textField.tag+1] becomeFirstResponder];
  else
    [textField resignFirstResponder];
  return YES;
}

- (void)touchesEnded: (NSSet *)touches withEvent: (UIEvent *)event {
  [self.txtUsername resignFirstResponder];
  [self.txtPassword resignFirstResponder];
}


@end
